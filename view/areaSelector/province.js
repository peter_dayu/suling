/**
 * 省级地址数据
 * 省级数据不会经常变动，可以写死在应用中，减少用户等待时常，减轻服务器压力
 */
// export let province = [{ "id": "320505", "bkName": "虎丘区", "bkGrade": "3", "bkParent": "320500" }, { "id": "320506", "bkName": "吴中区", "bkGrade": "3", "bkParent": "320500" }, { "id": "320507", "bkName": "相城区", "bkGrade": "3", "bkParent": "320500" }, { "id": "320508", "bkName": "姑苏区", "bkGrade": "3", "bkParent": "320500" }, { "id": "320509", "bkName": "吴江区", "bkGrade": "3", "bkParent": "320500" }, { "id": "320510", "bkName": "工业园区", "bkGrade": "3", "bkParent": "320500" }, { "id": "320581", "bkName": "常熟市", "bkGrade": "3", "bkParent": "320500" }, { "id": "320582", "bkName": "张家港市", "bkGrade": "3", "bkParent": "320500" }, { "id": "320583", "bkName": "昆山市", "bkGrade": "3", "bkParent": "320500" }, { "id": "320585", "bkName": "太仓市", "bkGrade": "3", "bkParent": "320500" }];


export let province = [{ "id": "32051001", "bkName": "斜塘", "bkGrade": "4", "bkParent": "320510" }, { "id": "32051004", "bkName": "跨塘", "bkGrade": "4", "bkParent": "320510" }, { "id": "32051005", "bkName": "唯亭", "bkGrade": "4", "bkParent": "320510" }];