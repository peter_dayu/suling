// pages/jrzp/gspeople/gspeople.js
var app = getApp()
var baseUrl = app.globalData.baseUrl;

Page({
    /**
     * 页面的初始数据
     */
    data: {
        resumeWorkList:null,
        usergender:['女','男'],
        schoolList: ['博士', '研究生', '本科', '大专', '中专', '高中', '初中','小学'],
        salary: ['请选择你的资薪', '1000-2000', '2000-3000', '3000-4000', '4000-5000', '5000-6000', '6000-7000', '7000-8000', '8000以上'],
        videoImg: '',
        videoId:''
    },
    onLoad:function(){
        var that = this;
        wx.getSystemInfo({
            success: function (res) {
                that.setData({
                    winWidth: res.windowWidth,
                    winHeight: res.windowHeight
                });
            },
        })
    },
    /**
     * 生命周期函数--监听页面加载
     */
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },
    editBaseInfoTap: function (e) {
        wx.navigateTo({
            url: '/pages/deliver/deliver',
        })
    },
    //跳转到视频播放页面
    play: function(e){

        wx.navigateTo({
            url: '/pages/personalVideo/personalVideo?videoUrl='+e.currentTarget.dataset.url
        })
    },
    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {
      let that = this
        wx.request({
            url: baseUrl + "/getcvbyid?userid=" + app.globalData.userid,
            success: function (res) {
                that.data = res.data.cv;
                that.setData({
                    data : res.data.cv
                })
                if(that.data.videoUrl){
                    that.setData({
                        videoImg: that.data.videoUrl + '?vframe/jpg/offset/0/',
                        videoId: that.data.videoId
                    })
                }
                that.setData({
                    videoId: that.data.videoId
                })
            }
        })
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})
