// components/gspeople/gspeople.js
const app = getApp();
var baseUrl = app.globalData.baseUrl;
Component({
  /**
   * 组件的属性列表
   */
  properties: {

  },

  /**
   * 组件的初始数据
   */
  data: {
    resumeWorkList:null,
    usergender:['女','男'],
    schoolList: ['博士', '研究生', '本科', '大专', '中专', '高中', '初中','小学'],
    salary: ['请选择你的资薪', '1000-2000', '2000-3000', '3000-4000', '4000-5000', '5000-6000', '6000-7000', '7000-8000', '8000以上'],
    videoImg: '',
    videoId:''
  },

  /**
   * 组件的方法列表
   */
  methods: {
    editBaseInfoTap: function (e) {
      wx.navigateTo({
          url: '/pages/deliver/deliver',
      })
  },
  play: function(e){
    wx.navigateTo({
        url: '/pages/personalVideo/personalVideo?videoUrl='+e.currentTarget.dataset.url
    })
},
},
  //跳转到视频播放页面

  /**
   * 生命周期函数--监听页面显示
   */

  lifetimes: {
    attached: function() {
      // 在组件实例进入页面节点树时执行
      var that =this
      wx.request({
        url: baseUrl + "/getcvbyid?userid=" + app.globalData.userid,
        success: function (res) {
            that.data = res.data.cv;
            that.setData({
                data : res.data.cv
            })
            if(that.data.videoUrl){
                that.setData({
                    videoImg: that.data.videoUrl + '?vframe/jpg/offset/0/',
                    videoId: that.data.videoId
                })
            }
            that.setData({
                videoId: that.data.videoId
            })
        }
    })
    },
    detached: function() {
      // 在组件实例被从页面节点树移除时执行
    },
  }
})
