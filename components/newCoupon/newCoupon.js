// components/newCoupon/newCoupon.js
let app = getApp()
var fwzs = app.globalData.fwzsUrl;
var login = require('../../utils/login.js');
Page({

    /**
     * 页面的初始数据
     */
    properties: {
        showFlag: Boolean
    },
    data: {
        showCoupon:true
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {

    },
    close:function(){
        let that = this
        that.setData({
            showCoupon:false
        })
    },
    receive:function(){
        this.setData({
            showCoupon:false
        })
        login.getCache(this.getCoupon)

    },
    getCoupon(adminToken){
        let that = this
        wx.request({
            url: fwzs + '/api/getCoupon99?userid='+app.globalData.userid,
            header:{
                "Content-Type": "application/json",
                "Authorization":"Bearer "+adminToken
            },
            success: function (res) {
                if(res.data.code == '1'){
                    wx.navigateTo({
                        url: '../couponSuccess/couponSuccess'
                    })
                }else{
                    wx.showToast({
                      title: '领取失败'

                    })
                }
            }
        })
    },
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})
