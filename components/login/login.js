// components/login.js
const app = getApp();
var baseUrl = app.globalData.baseUrl;
Component({

  /**
   * 页面的初始数据
   */
  properties: {
    showFlag: Boolean
  },
  data: {
    loginedCallback: null,
    canIUse: typeof wx.canIUse != "undefined" ? wx.canIUse('button.open-type.getUserInfo') : {},
    canIUseGetUserProfile: true,
  },

  /**
   * 组件的方法列表
   */
  methods: {
    giveUp(){
      this.setData({ showFlag: false })
    },
    doLogin(e){

      var isAuth = app.globalData.isAuth;
      if (e.detail) {
        this.setData({
          encryptedData: e.detail.encryptedData,
          iv: e.detail.iv
        })
      }
      wx.login({
        success: (res) => {
          this.getUserProfile();
         // wx.getUserProfile
          this.cancel();
        }
      })
    },
    getUserProfile: function () {
      var that = this;

      wx.getUserProfile({
        desc: '用于完善会员资料',
        lang:'zh_CN',
        success: (res) => {
          var userInfo = res.userInfo
          that.userInfoSetInSQL(userInfo)
        }
      })
      that.cancel()
    },

    getUserInfo: function () {
      var that = this;
      wx.getUserInfo({
        withCredentials: true,
        lang:'zh_CN',
        success: function (res) {
          var userInfo = res.userInfo
         // that.userInfoSetInSQL(userInfo)
        },
        fail: function () {
          // userAccess()
        }
      })
    },
    userInfoSetInSQL: function (userInfo) {
      var that = this;
      wx.getStorage({
        key: 'recomandId',
        success: function (res) {
          that.setData({
            recommandId: res.data
          })
        },
      })
      wx.getStorage({
        key: 'third_Session',
        success: function (res2) {
          var token = res2.data.token
          var openid = res2.data.openid
          wx.request({
            url: baseUrl + '/upduser',
            header: { "token": token },
            method: "POST",
            data: {
              openid: openid,
              nickName: userInfo.nickName,
              avatarUrl: userInfo.avatarUrl,
              gender: userInfo.gender,
              province: userInfo.province,
              city: userInfo.city,
              country: userInfo.country,
              recommandId: that.data.recommandId,
              encryptedData: that.data.encryptedData,
              iv: that.data.iv
            },
            success: function (res3) {
              if(res3.data.auth){
                app.globalData.isAuth = res3.data.auth
                var checkAuth= {
                  checkAuth: true,
                  userInfo:{
                      avatarUrl: userInfo.avatarUrl,
                      nickName: userInfo.nickName
                  }
                };
                that.triggerEvent("checkAuth", checkAuth )
              }
            },
              fail(res){
              console.log(res,'res')
              }

          })
        }
      })
    },
    cancel: function () {
      this.setData({
        showFlag: false
      })
    },
  }

})
