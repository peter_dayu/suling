const app = getApp();
var baseUrl = app.globalData.baseUrl;
export function getNextAreaList(areaId) {
    return new Promise((resolve, reject) => {
        wx.request({
            url: baseUrl+'/area/getAddressData?bkParent=' + areaId,//你的 接口地址
            // header: {
            //     "content-type": "application/json;charset=UTF-8",
            //     "Authorization": app.globalData.token,
            //     // "Authorization": 'Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJoZW5hbiIsImV4cCI6MTYwMDM2MjU4NiwiaWF0IjoxNjAwMjE4NTg2fQ.Hi6uJdJz-8SqF_L9PwLDx7Bn9m6-HLCJP49_7AU23BUFEgg8uijWWhqsP1t3M6kXFdOi3buYa4tg_KwAoVSP7Q',
            // },

            success(res) {

                let list = []
                for (let item of res.data) {
                    list.push({
                        id: item.id,//id对应地区ID
                        bkName: item.bkName//name对应地区名称
                    })
                }
                //成功回调 要确保数组中的对象有id和name字段
                resolve(list);

            },
            fail(err) {
                //失败回调
                reject(err)

            }

        })
    })
}
