var promise = require('./promiseMethod.js');

const app = getApp();
var baseUrl = app.globalData.baseUrl

function setCache(adminToken) {
    wx.setStorageSync('adminToken', adminToken);
    wx.setStorageSync('expireTime', getExpireTime())

}

function getCache(callBack) {
    let expireTime = wx.getStorageSync('expireTime');
    let nowTime = Date.now();
    if (expireTime && nowTime < expireTime) {
        let adminToken=wx.getStorageSync('adminToken')
        if(adminToken ){
            callBack(adminToken)
            return adminToken;
        }else {
            app.globalData.expireTime=0;
            setCache("")
        }

    } else {
        let adminToken = ''
        let url= baseUrl + '/admin/getToken'
        promise.getRequest(url, null).then(res => {
            adminToken = res.data.adminToken
            app.globalData.expireTime=res.data.expireTime*1000
            callBack(adminToken)
            setCache(adminToken)
        })
    }

}

function judgeTime() { //判断缓存是否过期

    let nowTime = Date.now();
    let oldTime = wx.getStorageSync('oldTime');
    if (oldTime && nowTime < oldTime) {
        return false;
    }
    return true;
}

function getExpireTime() { //返回以现在为准的3天后的时间
    return Date.now() + app.globalData.expireTime; //毫秒(72小时)
}



function login() {

    wx.login({
        success: res => {
            // 发送 res.code 到后台换取 openId, sessionKey, unionId
            var url = app.globalData.baseUrl + '/wxlogin'
            var params = {
                code: res.code
            }
            promise.getRequest(url, params).then(res1 => {
                app.globalData.openid = res1.data.openid;
                app.globalData.token = res1.data.token;
                app.globalData.accessToken = res1.data.accessToken;
                app.globalData.adminToken = res1.data.adminToken;
                app.globalData.session_key = res1.data.session_key;
                app.globalData.userid = res1.data.user.userid;
                app.globalData.phone = res1.data.user.phone;
                app.globalData.isRefuse = res1.data.isRefuse;
                app.globalData.isAuth = res1.data.user.auth;
                app.globalData.visible= res1.data.visible;
                app.globalData.isagent = res1.data.user.isagent;
                app.globalData.userInfo = {
                    isagent: res1.data.user.isagent,
                    avatarUrl: res1.data.user.avatarUrl,
                    nickName: res1.data.user.nickName
                }
                if (this.userIdCallback) {
                    this.userIdCallback(res.userid);
                }
                wx.setStorage({
                    key: 'third_Session',
                    data: res1.data
                })
            })
        }
    })

}

function doWxPay(param) {
    //小程序发起微信支付
    wx.requestPayment({
        appid: 'wxb6f03d40f36ac99f',
        timeStamp: param.timeStamp, //记住，这边的timeStamp一定要是字符串类型的，不然会报错
        nonceStr: param.nonceStr,
        package: param.packageValue,
        signType: 'MD5',
        paySign: param.paySign,
        success: function (event) {
            wx.showToast({
                title: '支付成功',
                icon: 'success',
                duration: 800
            });
            setTimeout(() => {
                wx.navigateTo({
                    url: '/pages/agentRecruits/agentRecruits?userid=' + app.globalData.userid,
                })
            }, 800);

        },
        fail: function (error) {
            wx.showToast({
                title: '支付失败',
                icon: 'error',
                duration: 500
            });
        },

        complete: function () {
            // complete
            console.log("pay complete")
        }

    });

}
module.exports = {
    login: login,
    doWxPay: doWxPay,
    getCache:getCache
}
