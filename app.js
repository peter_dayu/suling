var promise = require('./utils/promiseMethod.js');
//app.js
App({
    onLaunch: function () {
        // 展示本地存储能力
        wx.cloud.init()
        var logs = wx.getStorageSync('logs') || []
        logs.unshift(Date.now())
        wx.setStorageSync('logs', logs)
        // var baseUrl=app.getGolobleData.baseUrl;
        // 登录
        var that = this;
        // 获取用户信息
        wx.getSetting({
            success: res => {
                if (res.authSetting['scope.userInfo']) {
                    // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
                    wx.getUserInfo({
                        success: res => {
                            // 可以将 res 发送给后台解码出 unionId
                            this.globalData.userInfo = res.userInfo
                            this.globalData.isAuth = true;
                            // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
                            //x 所以此处加入 callback 以防止这种情况
                            if (this.userInfoReadyCallback) {
                                this.userInfoReadyCallback(res)
                            }
                        },
                        fail: res => {
                            wx.navigateTo({
                                url: '../../pages/login/login',
                            })
                        }
                    })
                }
            }
        })
        if (wx.getUpdateManager) {
            const updateManager = wx.getUpdateManager();
            updateManager.onCheckForUpdate(function(res) {
                // 请求完新版本信息的回调
                console.log(res.hasUpdate);
            });
            updateManager.onUpdateReady(function() {
                wx.showModal({
                    title: "更新提示",
                    content: "新版本已经准备好，是否重启应用？",
                    success: function(res) {
                        if (res.confirm) {
                            // 新的版本已经下载好，调用 applyUpdate 应用新版本并重启
                            updateManager.applyUpdate();
                        }
                    }
                });
            });
            updateManager.onUpdateFailed(function() {
                // 新的版本下载失败
                wx.showModal({
                    title: "已经有新版本了哟~",
                    content: "新版本已经上线啦~，请您删除当前小程序，重新搜索打开哟~"
                });
            });
        }
    },
    onShow: function (options) {
        let option = JSON.stringify(options);
        var resultScene = this.sceneInfo(options.scene);
    },
    //场景值判断
    sceneInfo: function (s) {
        var scene = [];
        switch (s) {
            case 1001:
                scene.push(s, "发现栏小程序主入口");
                break;
            case 1005:
                scene.push(s, "顶部搜索框的搜索结果页");
                break;
            case 1006:
                scene.push(s, "发现栏小程序主入口搜索框的搜索结果页");
                break;
            case 1007:
                scene.push(s, "单人聊天会话中的小程序消息卡片");
                break;
            case 1008:
                scene.push(s, "群聊会话中的小程序消息卡片");
                break;
            case 1011:
                scene.push(s, "扫描二维码");
                break;
            case 1012:
                scene.push(s, "长按图片识别二维码");
                break;
            case 1014:
                scene.push(s, "手机相册选取二维码");
                break;
            case 1017:
                scene.push(s, "前往体验版的入口页");
                break;
            case 1019:
                scene.push(s, "微信钱包");
                break;
            case 1020:
                scene.push(s, "公众号profile页相关小程序列表");
                break;
            case 1022:
                scene.push(s, "聊天顶部置顶小程序入口");
                break;
            case 1023:
                scene.push(s, "安卓系统桌面图标");
                break;
            case 1024:
                scene.push(s, "小程序profile页");
                break;
            case 1025:
                scene.push(s, "扫描一维码");
                break;
            case 1026:
                scene.push(s, "附近小程序列表");
                break;
            case 1027:
                scene.push(s, "顶部搜索框搜索结果页“使用过的小程序”列表");
                break;
            case 1028:
                scene.push(s, "我的卡包");
                break;
            case 1029:
                scene.push(s, "卡券详情页");
                break;
            case 1031:
                scene.push(s, "长按图片识别一维码");
                break;
            case 1032:
                scene.push(s, "手机相册选取一维码");
                break;
            case 1034:
                scene.push(s, "微信支付完成页");
                break;
            case 1035:
                scene.push(s, "公众号自定义菜单");
                break;
            case 1036:
                scene.push(s, "App分享消息卡片");
                break;
            case 1037:
                scene.push(s, "小程序打开小程序");
                break;
            case 1038:
                scene.push(s, "从另一个小程序返回");
                break;
            case 1039:
                scene.push(s, "摇电视");
                break;
            case 1042:
                scene.push(s, "添加好友搜索框的搜索结果页");
                break;
            case 1044:
                scene.push(s, "带shareTicket的小程序消息卡片");
                break;
            case 1047:
                scene.push(s, "扫描小程序码");
                break;
            case 1048:
                scene.push(s, "长按图片识别小程序码");
                break;
            case 1049:
                scene.push(s, "手机相册选取小程序码");
                break;
            case 1052:
                scene.push(s, "卡券的适用门店列表");
                break;
            case 1053:
                scene.push(s, "搜一搜的结果页");
                break;
            case 1054:
                scene.push(s, "顶部搜索框小程序快捷入口");
                break;
            case 1056:
                scene.push(s, "音乐播放器菜单");
                break;
            case 1058:
                scene.push(s, "公众号文章");
                break;
            case 1059:
                scene.push(s, "体验版小程序绑定邀请页");
                break;
            case 1064:
                scene.push(s, "微信连Wifi状态栏");
                break;
            case 1067:
                scene.push(s, "公众号文章广告");
                break;
            case 1068:
                scene.push(s, "附近小程序列表广告");
                break;
            case 1072:
                scene.push(s, "二维码收款页面");
                break;
            case 1073:
                scene.push(s, "客服消息列表下发的小程序消息卡片");
                break;
            case 1074:
                scene.push(s, "公众号会话下发的小程序消息卡片");
                break;
            case 1089:
                scene.push(s, "微信聊天主界面下拉");
                break;
            case 1090:
                scene.push(s, "长按小程序右上角菜单唤出最近使用历史");
                break;
            case 1092:
                scene.push(s, "城市服务入口");
                break;
            default:
                scene.push("未知入口");
                break;
        }
        return scene;
    },
    globalData: {
        userInfo: {},
        isAuth: true,//是否受过权利
        auth:true,//是否给与小程序权限
        isRefuse:'',
        useData: [],
        token: '',
        expireTime:0,
        visible:true,
        accessToken: '',
        adminToken:'',
        openid: '',
        userid: '',
        phone:'',
        session_key: '',
        lastId:'',
        lastId2:'',
        isagent:'',
        baseUrl: "https://api.sulinghr.com",
        wssUrl: "wss://api.sulinghr.com",
        fwzsUrl: 'https://job.sulinghr.com',
        notifyUrl:"https://job.sulinghr.com",
        qiniuTokenUrl: 'https://job.sulinghr.com/api/qiNiuToken',
        // baseUrl: "http://192.168.0.108:9000",
        // wssUrl: "ws://192.168.0.108:9000",
        // fwzsUrl:"http://192.168.0.108:8000",
        // notifyUrl:"http://192.168.0.108:8000",
        // qiniuTokenUrl: 'http://192.168.0.108:8000/api/qiNiuToken',
        // baseUrl: "http://192.168.0.164:9000",
        // wssUrl: "ws://192.168.0.164:9000",
        // fwzsUrl:"http://192.168.0.164:8000",
        // notifyUrl:"http://192.168.0.164:8000",
        // qiniuTokenUrl: 'http://192.168.0.164:8000/api/qiNiuToken'

    },
})
