// pages/myAssets/myAssets.js
let app = getApp()
var baseUrl = app.globalData.fwzsUrl;
var login = require('../../utils/login.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
      accountInfo:{}
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },
  getAssets:function(){
    login.getCache(this.getAssets1)
  },
  getAssets1:function(adminToken){
      let that = this
      var token = app.globalData.token;
      wx.request({
          url: baseUrl + '/api/account',
          data: { "userid": app.globalData.userid },
          method: 'GET',
          header:{
              "Content-Type":"application/json",
              "Authorization":"Bearer "+adminToken
          },
          success: function (res) {
              that.setData({
                  accountInfo:res.data
              })
          }
      })
  },
    goWithdraw:function(){
      wx.navigateTo({
        url: '../withdraw/withdraw'
      })
    },
    goTransactionDetails:function(){
        wx.navigateTo({
            url: '../transactionDetails/transactionDetails'
        })
    },
        /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },
  goMyIntegral: function () {
    wx.navigateTo({
        url: '../myIntegral/myIntegral',
    })
},
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getAssets()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})
