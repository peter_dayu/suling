// pages/videoInfo/videoInfo.js
const app = getApp();
var baseUrl = app.globalData.baseUrl;
var fwzsUrl = app.globalData.fwzsUrl;
var login = require('../../utils/login.js');
Page({

    /**
     * 页面的初始数据
     */
    data: {
        fullVideoInfo:{},
        showModalStatus:false,
        commentContents:[],
        isChildShow:false,
        videoId:'',
        commentContent:'',
        moreCommentContent:[],
        pageCommentNo:0,
        userid:'',
        type:0,
        fromPath:''
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        var that=this
        if( app.globalData.userid == ''){
            login.login()
        }
        if(options.type){
            that.setData({
                type :options.type
            })
        }
        let id = options.id;
        that.setData({
            videoId:id,
            userid:app.globalData.userid,
            fromPath:options.fromPath
        })
        that.getAreaList()
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },
    getAreaList(){
        let that = this;
        wx.request({
            url: baseUrl + "/video/queryVideoById?id="+that.data.videoId+"&userid="+app.globalData.userid+"&type="+that.data.type,
            success: function (res) {
                that.setData({
                    fullVideoInfo: res.data,
                })
            }
        })
    },
    popComments(e){
        let id = e.currentTarget.dataset.id
        let userid = e.currentTarget.dataset.userid
        let that = this
        that.setData({
            showModalStatus:true
        })
        wx.request({
            url: baseUrl + "/video/queryCommentByVid?vid="+id+"&userid="+app.globalData.userid+"&type="+that.data.type,
            success: function (res) {
                that.setData({
                    commentContents: res.data.pageData,
                    isChildShow:res.data.isChildShow
                })
            }
        })
    },
    closeModal(){
        let that = this
        that.setData({
            showModalStatus:false
        })
    },
    showInput(e){
        let id= e.currentTarget.dataset.id
        this.setData({
            isFocus:true,
            commentId: id
        })
    },
    showLoginModal(){
        if (app.globalData.isAuth == 0||!app.globalData.auth) {
            wx.showModal({
                cancelColor: 'cancelColor',
                title: '提示',
                content: '请先登录',
                success:(res) =>{
                    if (res.confirm) {
                        console.log('用户点击确定')
                        this.setData({
                            showFlag: true
                        })
                    } else if (res.cancel) {
                        console.log('用户点击取消')
                    }

                }
            })
            return false
        }else{
            return true
        }
    },
    //获取单个视频信息
    getSingleVideo(id){
        let that = this
        wx.request({
            url: baseUrl + "/video/queryVideoById?id="+id+"&userid="+app.globalData.userid+"&type="+that.data.type,
            success:function (res) {
                that.setData({
                    fullVideoInfo:res.data
                })
            }
        })
    },
    // 关注
    toFollow(e){
        let that = this
        if(!that.showLoginModal()){
            return false
        }
        let userid = e.currentTarget.dataset.userid
        let isfollow = e.currentTarget.dataset.isfollow
        let id = e.currentTarget.dataset.id
        if(isfollow == '0'){
            let urlUser=baseUrl + "/video/follow?userid="+app.globalData.userid+"&followId="+userid
            let urlRecruit=baseUrl + "/video/followHr?userid="+app.globalData.userid+"&deptId="+userid
            wx.request({
                url: (that.data.type==0 ||that.data.type==3 )?urlUser:urlRecruit,
                success: function (res) {
                    that.getSingleVideo(id)
                }
            })
        }else {
            wx.request({
                url: baseUrl + "/video/unFollow?userid="+app.globalData.userid+"&followId="+userid,
                success: function (res) {
                    that.getSingleVideo(id)
                }
            })
        }

    },
    thumbs_up(e){

        let that = this
        if(!that.showLoginModal()){
            return false
        }
        let id = e.currentTarget.dataset.id
        // if(that.data.fullVideoInfo.stars == 0){
        wx.request({
            url: baseUrl + "/video/star",
            data: {
                vid: id,
                userid: app.globalData.userid,
                isStar: !that.data.fullVideoInfo.stars,
            },
            method:"Post",
            success: function (res) {
                that.getAreaList()
            }
        })
        // }
    },
    getComments(e){
        this.setData({
            commentContent:e.detail.value
        })
    },
    getMoreComments(e){
        let that = this
        let id = e.currentTarget.dataset.id
        let vid = e.currentTarget.dataset.vid
        that.data.pageCommentNo++
        wx.request({
            url: baseUrl + "/video/queryCommentByCommentId?vid="+vid+"&commentId="+id+"&userid="+app.globalData.userid+"&pageNo="+that.data.pageCommentNo+"&pageSize=10",
            success: function (res) {
                that.setData({
                    moreCommentContent:[...that.data.moreCommentContent,...res.data],
                })
            }
        })
    },
    toComments(e){
        let that= this
        let vid = e.currentTarget.dataset.vid
        if(that.data.commentContent ==""){
            wx.showModal({
                title: "提示",
                content: "请输入评论内容哦～"
            })
            return false
        }
        wx.request({
            url: baseUrl + "/video/addComment",
            data: {
                commentId:that.data.commentId,
                vid: vid,
                userid: app.globalData.userid,
                content: that.data.commentContent,
            },
            method:"Post",
            success: function (res) {
                // that.popComments()
                if(res.data == '1'){
                    wx.showToast({
                        title: '评论成功',
                        icon: 'success',
                        duration: 2000
                    })
                    that.getAreaList()
                    that.setData({
                        showModalStatus:false,
                        commentId:''
                    })
                }
            }
        })
    },
    goCommentsLike(e){
        let that = this
        let id = e.currentTarget.dataset.id
        let vid = e.currentTarget.dataset.vid
        let stars = e.currentTarget.dataset.stars
        wx.request({
            url: baseUrl + "/video/star",
            data: {
                commentId:id,
                vid: vid,
                userid: app.globalData.userid,
                isStar: !stars,
            },
            method:"Post",
            success: function (res) {
                wx.request({
                    url: baseUrl + "/video/queryCommentByVid?vid="+vid+"&userid="+app.globalData.userid,
                    success: function (res) {
                        that.setData({
                            commentContents: res.data.pageData
                        })
                    }
                })
            }
        })
    },
    //进入推荐企业
    toAgentRecruits:function(e){
        let userid = e.currentTarget.dataset.userid
        wx.navigateTo({
            url:"../agentRecruits/agentRecruits?userid="+userid
        })
    },
    toPersonalInfo:function(e){

        wx.navigateTo({
            url: '../personalInfo/personalInfo?userid='+e.currentTarget.dataset.userid+"&type="+this.data.type
        })

    },
    delete:function(){
        var that=this
        if(that.data.fullVideoInfo.type=='4'){
            wx.showModal({
              title: '确认删除此视频?',
              content:'此视频关联了招聘职位，删除后将停止此招聘。',
              success (res) {
                if (res.confirm) {
                    login.getCache(that.delete1)
                } else if (res.cancel) {
                  return false
                }
              }
            })
        }else{
            wx.showModal({
                title: '确认删除此视频?',
                success (res) {
                    if (res.confirm) {
                        login.getCache(that.delete1)
                        that.setData({
                            showFlag:true
                        })
                    } else if (res.cancel) {
                        return false
                    }
                }
            })
        }


    },
    delete1:function(adminToken){
        let that = this
        wx.request({
            url: fwzsUrl + "/api/video/delete/"+that.data.videoId,
            method:"delete",
            header:{
                "Content-Type":"application/json",
                "Authorization":"Bearer "+adminToken
            },
            success: function (res) {
                if(res.data == 'success'){
                    wx.showToast({
                        title: '删除成功',
                        icon: 'success',
                        duration: 1500
                    })
                    setTimeout(function () {
                        wx.navigateBack()
                    },1500)
                }else{
                    wx.showToast({
                        title: '删除失败',
                        icon: 'error',
                        duration: 1500
                    })
                }
            }
        })
    },
    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function (res) {
        let id = res.target.dataset.id
        if (res.from === 'button') {
        }
        return {
            title: '给你分享一个视频',
            path: 'pages/videoInfo/videoInfo?id='+id,
            success: function (res) {

            }
        }
    },
    onShareTimeline:function(res){
        let id =this.data.videoId
        let title = this.data.fullVideoInfo.title
       return{
           title:title,
           imageUrl:this.data.fullVideoInfo.imgCover,
           query:'id='+id
       }
    },
})
