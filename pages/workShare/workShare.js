// pages/workShare/workShare.js
const qiniuUploader = require("../../utils/qiniuUploader");
const app = getApp();
var fwzsUrl = app.globalData.fwzsUrl;
var baseUrl = app.globalData.baseUrl;
var QQMapWX = require('../../utils/qqmap-wx-jssdk.min.js');
var login=require('../../utils/login.js')
var qqmapsdk = new QQMapWX({
    key: 'IFCBZ-FF53U-ILBVP-BOAOD-SA4E7-GWFPK'//申请的开发者秘钥key
});
function initQiniu(){
    login.getCache(initQiniu1)
}
function initQiniu1(adminToken) {
    app.globalData.adminToken=adminToken
    var options = {
        // bucket所在区域，这里是华北区。ECN, SCN, NCN, NA, ASG，分别对应七牛云的：华东，华南，华北，北美，新加坡 5 个区域
        region: 'ECN',
        adminToken: adminToken,
        // 获取uptoken方法三选一即可，执行优先级为：uptoken > uptokenURL > uptokenFunc。三选一，剩下两个置空。推荐使用uptokenURL，详情请见 README.md
        // 由其他程序生成七牛云uptoken，然后直接写入uptoken
        uptoken: '',
        key:null,
        // 从指定 url 通过 HTTP GET 获取 uptoken，返回的格式必须是 json 且包含 uptoken 字段，例如： {"uptoken": "0MLvWPnyy..."}
        uptokenURL: app.globalData.qiniuTokenUrl,
        // uptokenFunc 这个属性的值可以是一个用来生成uptoken的函数，详情请见 README.md
        uptokenFunc: function () {
            // do something
            return qiniuUploadToken;
        },

        // bucket 外链域名，下载资源时用到。如果设置，会在 success callback 的 res 参数加上可以直接使用的 fileURL 字段。否则需要自己拼接
        domain: 'http://[yourBucketId].bkt.clouddn.com',
        // qiniuShouldUseQiniuFileName 如果是 true，则文件的 key 由 qiniu 服务器分配（全局去重）。如果是 false，则文件的 key 使用微信自动生成的 filename。出于初代sdk用户升级后兼容问题的考虑，默认是 false。
        // 微信自动生成的 filename较长，导致fileURL较长。推荐使用{qiniuShouldUseQiniuFileName: true} + "通过fileURL下载文件时，自定义下载名" 的组合方式。
        // 自定义上传key 需要两个条件：1. 此处shouldUseQiniuFileName值为false。 2. 通过修改qiniuUploader.upload方法传入的options参数，可以进行自定义key。（请不要直接在sdk中修改options参数，修改方法请见demo的index.js）
        // 通过fileURL下载文件时，文件自定义下载名，请参考：七牛云“对象存储 > 产品手册 > 下载资源 > 下载设置 > 自定义资源下载名”（https://developer.qiniu.com/kodo/manual/1659/download-setting）。本sdk在README.md的"常见问题"板块中，有"通过fileURL下载文件时，自定义下载名"使用样例。
        shouldUseQiniuFileName: true
    };
    // 将七牛云相关配置初始化进本sdk
    qiniuUploader.init(options);
}
Page({

    /**
     * 页面的初始数据
     */
    data: {
        // 文件上传（从客户端会话）返回对象。上传完成后，此属性被赋值
        messageFileObject: {},
        // 文件上传（从客户端会话）进度对象。开始上传后，此属性被赋值
        messageFileProgress: {},
        videoUrl: '',
        videoImage: '',
        current: 1,
        qiniuId:"",
        imgId:'',
        title:'',
        ass_enterprise:'',
        locationId:'',

    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        this.getLocation()
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },
    editCover(){
        let that = this
        wx.chooseImage({
            count: 1,
            sizeType: ['original', 'compressed'],
            sourceType: ['album', 'camera'],
            success(res) {
                const tempFilePaths = res.tempFilePaths
                wx.uploadFile({
                  url: app.globalData.fwzsUrl+'/api/qiNiuContent',
                  header:{
                    "Authorization":"Bearer "+app.globalData.adminToken
                  },
                  filePath: tempFilePaths[0],
                  name:'file',
                  success:function(res1){
                      let data =JSON.parse(res1.data)
                      const id = data.id
                      const imgcover = data.data[0]
                      that.setData({
                        videoImage:imgcover,
                        imgId:id
                    })

                  },
                  fail(res) {
                        wx.showToast({
                            title: res.data.message,
                            duration: 2000
                        })
                    }
                })
            }
        })
    },

    uploadVideo(){
        let that = this
        // 初始化七牛云相关参数
        initQiniu();
        // 置空messageFileObject和messageFileProgress，否则在第二次上传过程中，wxml界面会存留上次上传的信息
        that.setData({
            'messageFileObject': {},
            'messageFileProgress': {}
        });
        wx.chooseVideo({
            // 最多可以选择的文件个数。目前本sdk只支持单文件上传，若选择多文件，只会上传第一个文件
            count: 1,
            // type: 所选的文件的类型
            // type的值: {all: 从所有文件选择}, {video: 只能选择视频文件}, {image: 只能选择图片文件}, {file: 只能选择除了图片和视频之外的其它的文件}
            type: 'video',
            success: function (res) {
                var filePath = res.tempFilePath;
                var fileName = new Date().getTime(); // 向七牛云上传
                var size = res.size;
                if(res.size>100*1024*1024){
                    wx.showToast({
                      title: '文件最大100M',
                      icon:'none'
                    })
                    return false
                }
                // if(res.duration>120){
                //     wx.showToast({
                //       title: '视频长度最长2分钟',
                //       icon:'none'
                //     })
                //     return false
                // }
                // 向七牛云上传
                qiniuUploader.upload(filePath, (res) => {
                        res.fileName = fileName;
                        that.setData({
                            'messageFileObject': res,
                            videoUrl: 'https://video.sulinghr.com/' + res.key,
                            videoImage: 'https://video.sulinghr.com/' + res.key + '?vframe/jpg/offset/0/'
                        });
                        if(res.key){
                            wx.request({
                                url: fwzsUrl + "/api/qiNiuContent/saveVideoUpload?size="+size+"&key="+res.key,
                                header:{
                                    "Authorization":"Bearer "+app.globalData.adminToken
                                },
                                success: function (res) {
                                    that.setData({
                                        qiniuId: res.data
                                    })
                                }
                            })
                        }
                    }, (error) => {
                        console.error('error: ' + JSON.stringify(error));
                    },
                    // 此项为qiniuUploader.upload的第四个参数options。若想在单个方法中变更七牛云相关配置，可以使用上述参数。如果不需要在单个方法中变更七牛云相关配置，则可使用 null 作为参数占位符。推荐填写initQiniu()中的七牛云相关参数，然后此处使用null做占位符。
                    // 若想自定义上传key，请把自定义key写入此处options的key值。如果在使用自定义key后，其它七牛云配置参数想维持全局配置，请把此处options除key以外的属性值置空。
                    // 启用options参数请记得删除null占位符
                    // {
                    //   region: 'NCN', // 华北区
                    //   uptokenURL: 'https://[yourserver.com]/api/uptoken',
                    //   domain: 'http://[yourBucketId].bkt.clouddn.com',
                    //   shouldUseQiniuFileName: false,
                    //   key: 'testKeyNameLSAKDKASJDHKAS',
                    //   uptokenURL: 'myServer.com/api/uptoken'
                    // },
                    null,
                    (progress) => {
                        that.setData({
                            'messageFileProgress': progress
                        });
                        if(progress.progress < 100){
                            wx.showToast({
                                title: '上传进度'+progress.progress+'%',
                                icon:'none'

                            })
                        }
                    }, cancelTask => that.setData({ cancelTask })
                );
            }
        })
    },
    getTitle(e){
        let title = e.detail.value;
        this.setData({
            title: title
        })
    },
    getEnterprise(e){
        let ass_enterprise = e.detail.value;
        this.setData({
            ass_enterprise: ass_enterprise
        })
    },
    chooseLabel(e){
      let current = e.currentTarget.dataset.current;
      this.setData({
          current: current
      })
    },
    getLocation() {
        let that = this
        wx.getLocation({
            success: function (res) {
                // 调用sdk接口
                qqmapsdk.reverseGeocoder({
                    location: {
                        latitude: res.latitude,
                        longitude: res.longitude
                    },
                    success: function (res) {
                        //获取当前地址成功
                        var city = res.result.address_component.city;
                        var district = res.result.address_component.district
                        wx.request({
                            url: baseUrl + "/area/queryLocationIdByName?city=" + city+"&district="+district,
                            success: function (res) {
                                that.setData({
                                    locationId:res.data
                                })
                            }
                        })
                    },
                    fail: function (res) {
                        console.log('获取当前地址失败');
                    }
                });
            },
            fail(res) {

                console.log(res, '获取地址失败')
            }
        })
    },
    publish(){
        let that= this
        if(!that.data.locationId){
            wx.showModal({
                title: "提示",
                content: "请先去设置中开启定位才可发布哦～"
            })
            return false
        }
        if(!that.data.qiniuId){
            wx.showModal({
                title: "提示",
                content: "请选择视频"
            })
            return false
        }
        if(!that.data.title){
            wx.showModal({
                title: "提示",
                content: "请输入标题"
            })
            return false
        }
        wx.request({
            url: baseUrl + "/video/upload",
            data: {
                title: that.data.title,
                userid: app.globalData.userid,
                url:that.data.videoUrl,
                imgCover:that.data.videoImage,
                imgId:that.data.imgId,
                qiniuId: that.data.qiniuId,
                locationId:that.data.locationId,
                relateCompany:that.data.ass_enterprise,
                tag:that.data.current
            },
            method:"Post",
            success: function (res) {
                if(res.data > 0){
                    setTimeout(function () {
                        wx.navigateBack()
                    },1500)
                    wx.showToast({
                        title: '发布成功',
                        icon: 'success',
                        duration: 1500
                    })

                }
            }
        })
    },
    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {
        clearInterval()

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})
