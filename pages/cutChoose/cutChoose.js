// pages/cutChoose/cutChoose.js
const app = getApp();
var fwzsUrl = app.globalData.fwzsUrl;
var baseUrl = app.globalData.baseUrl;
var login = require('../../utils/login.js');
Page({

    /**
     * 页面的初始数据
     */
    data: {
        current:0,
        priceList:[]
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {

    },
    chooseNav:function(e){
        let that = this
        let current = e.currentTarget.dataset.current
        that.setData({
            current:current
        })
    },
    cut:function(){
        let that = this
        that.goBargain()
        wx.redirectTo({
          url: '../invite/invite?current='+that.data.current
        })

    },
    query:function(){
        login.getCache(this.queryPrice)
    },
    queryPrice:function (adminToken){
        let that = this
        var current=that.data.current
        wx.request({
            url: fwzsUrl + "/api/vip/queryPrice?type=agent&level=1",
            header: {
                'content-type': 'application/json', // 默认值
                'Authorization':'Bearer '+adminToken
            },
            success: function (res) {
                that.setData({
                    priceList:res.data,
                })
            }
        })
    },
    goBargain:function(){
        login.getCache(this.bargain)
    },
    bargain:function(adminToken){
        let that = this
        let type = ''
        if(that.data.current == '0'){
            type='knight1'
        }else if(that.data.current == '1'){
            type='knight2'
        }else{
            type='knight3'
        }
        wx.request({
            url:fwzsUrl+"/api/bargain",
            header: {
                'content-type': 'application/json', // 默认值
                'Authorization':'Bearer '+adminToken
            },
            method:'Post',
            data:{
                type:type,
                userid:app.globalData.userid
            },
            success:function (res) {
            }
        })
    },
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {
        this.query()
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})
