// pages/edit-resume-base/edit-resume-base.js
var app = getApp();
var baseUrl = app.globalData.baseUrl;
var login = require('../../utils/login.js');
Page({
    data: {
        edulevellist: ['博士', '研究生', '本科', '大专', '中专', '高中', '初中', '小学'],//学历
        genderlist: ['女', '男'],//性别
        user: "输入您的姓名",    // 姓名placeholder
        userphone: "输入您的电话",
        useremail: "输入紧急联系人的电话",
        houseuser: '输入居住地址',
        usera: '',
        userb: '请填写您的籍贯',
        userid: '',
        openid: '',
        info: '',
        userf: '请填写紧急联系人姓名',
        /**
         * 页面的初始数据
         */
        name: '',//姓名
        gender: 0,//性别index
        height: '',
        education: 3,
        household: '',//默认北京
        phone: '',//联系电话
        birthyear: '1995-01',//出生日期
        habihould: '',
        // 工作经历
        content_id: '',
        salary: ['请选择你的资薪', '1000-2000', '2000-3000', '3000-4000', '4000-5000', '5000-6000', '6000-7000', '7000-8000', '8000以上'],
        hiredate: '2015-01',//入职时间w
        firedate: '2015-01',//离职时间
        endDate: '2000-01',//离职时间,
        leaveDate: "",       // 选择离职的时间
        workContentLen: 0,
        salaryGrade: 0,
        company: "",    // 公司名称
        workContent: "",     // 工作内容
        orporatename: "输入公司名称",
        position: "如：市场专员",
        jobcontent: "如：拓展合作资源，策划线上及线下推广活动",
        videoSrc: [],
        videoUrl: '',
        videoId: '',
        videoImg: '',
        showFlag: false,
    },

    /*
     生命周期函数--监听页面加载
     */
    onLoad: function () {

    },
    onShow:function(){
        let that = this
        that.getcvbyid()

    },
    getcvbyid:function(){
        let that = this
        wx.request({
            url: baseUrl + "/getcvbyid?userid=" + app.globalData.userid,
            success: function (res) {
                if (res.data.cv == null) {
                    that.data.info = true
                    that.setData({
                        info: true
                    })
                } else {
                    that.data.name = res.data.cv.name,//姓名
                        that.data.gender = res.data.cv.gender,//性别index
                        that.data.height = res.data.cv.height,
                        that.data.education = res.data.cv.education,//默认本科
                        that.data.household = res.data.cv.household,
                        that.data.phone = res.data.cv.phone,//联系电话
                        that.data.birthyear = res.data.cv.birthyear,
                        that.data.habitation = res.data.cv.habitation,
                        that.data.company = res.data.cv.company,
                        that.data.salaryGrade = res.data.cv.salaryGrade,
                        that.data.hiredate = res.data.cv.hiredate,
                        that.data.firedate = res.data.cv.firedate,
                        that.data.description = res.data.cv.description,
                        that.data.videoSrc[0] = res.data.cv.videoUrl,
                        that.data.videoId = res.data.cv.videoId,
                        that.data.videoImg = res.data.cv.videoUrl + '?vframe/jpg/offset/0/',
                        that.data.info = false,
                        that.setData({
                            info: false,
                            name: res.data.cv.name,//姓名
                            gender: res.data.cv.gender,//性别index
                            height: res.data.cv.height,
                            education: res.data.cv.education,//默认本科
                            household: res.data.cv.household,
                            phone: res.data.cv.phone,//联系电话
                            birthyear: res.data.cv.birthyear,//出生日期
                            habitation: res.data.cv.habitation,
                            company: that.data.company,
                            salaryGrade: that.data.salaryGrade,
                            hiredate: that.data.hiredate,
                            firedate: that.data.firedate,
                            description: that.data.description,
                            videoSrc: that.data.videoSrc,
                            videoImg:that.data.videoImg,
                            videoId: that.data.videoId
                        })
                }
            }
        })
    },
    namefocus: function (e) {
        this.setData({
            user: ""
        })
    },
    //姓名失去焦点
    blurfocus: function (e) {
        this.setData({
            user: "输入您的姓名",
            name: e.detail.value
        })
    },
    //身高
    heightfocus: function (e) {
        this.setData({
            height: e.detail.value
        })
    },

    tionfocus: function (e) {
        this.setData({
            habitation: e.detail.value
        })
    },

    housefocus: function (e) {
        this.setData({
            household: e.detail.value
        })
    },
    //性别
    bindPickerChangeSex: function (e) {
        this.setData({
            gender: e.detail.value
        })
    },
    //学历
    bindPickerChangeEduLevel: function (e) {
        this.setData({
            education: e.detail.value
        })
    },
    //出生日期
    bindDateChangeBirthday: function (e) {
        this.setData({
            birthyear: e.detail.value
        })
    },
    //城市
    bindPickerChangeCity: function (e) {
        this.setData({
            household: e.detail.value
        })
    },
    getPhoneNumber (e) {
        var that = this;
        var openid = app.globalData.openid;
        var session_key = app.globalData.session_key;
        if (e.detail.errMsg == "getPhoneNumber:ok") {
            wx.request({
                url: baseUrl + '/decodePhone',
                data: {
                    encryptedData: e.detail.encryptedData,
                    iv: e.detail.iv,
                    sessionKey: session_key,
                    uid: openid,
                },
                header: {
                    "Content-Type": "application/x-www-form-urlencoded"
                },
                method: "Post",
                success: function (res) {
                    that.setData({
                        phone: res.data
                    })
                    wx.request({
                        url: baseUrl + '/upduser',
                        method: "POST",
                        data: {
                            openid: openid,
                            phone: res.data
                        },
                        success: function (res2) {

                        }
                    })
                }
            })
        }
    },

    //电话失去焦点
    phoneblur: function (e) {
        this.setData({
            phone: e.detail.value
        });
    },
    //公司名称失焦
    orporatenameblur: function (e) {
        this.setData({
            company: e.detail.value
        });
    },
    //入职时间
    bindDateChangeJoin: function (e) {
        this.setData({
            hiredate: e.detail.value,
            endDate: e.detail.value
        })
    },
    //离职时间
    bindDateChangeLeave: function (e) {
        this.setData({
            firedate: e.detail.value
        })
    },
    //薪水情况
    bindPickersalary: function (e) {
        this.setData({
            salaryGrade: e.detail.value
        })
    },
    //工作内容
    WorkContentTap: function (e) {
        var eValueLen = e.detail.value.length,
            eValue = e.detail.value;
        this.setData({
            workContentLen: eValueLen,
            description: eValue
        })
    },
    //上传视频
    uploadVideo(){
        login.getCache(this.uploadVideo1)
    },
    uploadVideo1: function(adminToken){
        let that = this
        wx.chooseVideo({
            sourceType: ['album','camera'],
            maxDuration: 60,
            camera: 'back',
            success(res) {
                wx.uploadFile({
                    url: app.globalData.fwzsUrl+'/api/videoUpload',
                    header:{
                        "Authorization":"Bearer "+adminToken
                    },
                    filePath: res.tempFilePath,
                    name:'file',
                    success:function(res1){
                        let data =JSON.parse(res1.data)
                        that.setData({
                            videoSrc: data.data,
                            videoUrl:data.data[0],
                            videoId:data.id,
                            videoImg:data.data[0] +'?vframe/jpg/offset/0/'
                        })
                    },
                    fail(res) {
                        wx.showToast({
                            title: res.data.message,
                            duration: 2000
                        })
                    }
                })
            }
        })
    },
    //删除视频
    deleteVideo: function(e) {
        let that = this
        wx.request({
            url: baseUrl + '/video/delVideo/?id='+ e.currentTarget.dataset.id+'&userid='+app.globalData.userid,
            method: "GET",
            success: function (res2) {
                that.setData({
                    videoId: '',
                    videoImg:''
                })
            }
        })
    },
    //跳转到视频播放页面
    play: function(e){

        wx.navigateTo({
            url: '/pages/personalVideo/personalVideo?videoUrl='+e.currentTarget.dataset.url
        })
    },
    //保存
    submitResumeBaseTap: function () {
        if ((this.data.name == "")) {
            wx.showModal({
                title: "和谐马提示您",
                content: "请填写姓名"
            });
            return;
        } else if(this.data.phone == ""){
            wx.showModal({
                title: "和谐马提示您",
                content: "请填写电话号码"
            });
            return;
        } else if (!(/^1(3|4|5|6|7|8|9)\d{9}$/.test(this.data.phone))) {
            wx.showModal({
                title: "和谐马提示您",
                content: "手机号码格式不对！"
            });
            return;
        } else {
            var that = this;
            if (that.data.info == true) {
                wx.request({
                    url: baseUrl + "/addcv?userid=" + app.globalData.userid,
                    method: 'POST',
                    data: {
                        userid: app.globalData.userid,
                        name: that.data.name,
                        gender: that.data.gender,
                        height: that.data.height,
                        education: that.data.education,
                        household: that.data.household,
                        phone: that.data.phone,
                        birthyear: that.data.birthyear,
                        habitation: that.data.habitation,
                        company: that.data.company,
                        salaryGrade: that.data.salaryGrade,
                        hiredate: that.data.hiredate,
                        firedate: that.data.firedate,
                        description: that.data.description,
                        videoUrl: that.data.videoUrl,
                        videoId: that.data.videoId,
                    },
                    success: function (res) {
                        if(res.data.code == '1'){
                            wx.showToast({
                                title: '保存成功！',
                                icon: 'success',
                                duration: 500
                            })
                            //返回上一个页面
                            var pages = getCurrentPages();
                            var curPage = pages[pages.length - 2];
                            curPage.setData({
                                data: that.data,
                                num:1
                            });
                            //返回上一个页面
                            setTimeout(function () {
                                wx.navigateBack({
                                })
                            }, 800);
                        }else {
                            wx.showModal({
                                title: res.data.message,
                                icon: 'error',
                                duration: 500
                            })
                        }
                    }
                })
            } else {
                wx.request({
                    url: baseUrl + "/modifycv?userid=" + app.globalData.userid,
                    method: 'POST',
                    data: {
                        userid: app.globalData.userid,
                        name: that.data.name,
                        gender: that.data.gender,
                        height: that.data.height,
                        education: that.data.education,
                        household: that.data.household,
                        phone: that.data.phone,
                        birthyear: that.data.birthyear,
                        habitation: that.data.habitation,
                        company: that.data.company,
                        salaryGrade: that.data.salaryGrade,
                        hiredate: that.data.hiredate,
                        firedate: that.data.firedate,
                        description: that.data.description,
                        videoUrl: that.data.videoUrl,
                        videoId: that.data.videoId,
                    },
                    success: function (res) {
                        if(res.data.code == '1'){
                            wx.showToast({
                                title: '保存成功！',
                                icon: 'success',
                                duration: 500
                            })
                            //返回上一个页面
                            var pages = getCurrentPages();
                            var curPage = pages[pages.length - 2];
                            curPage.setData({
                                data: that.data
                            });
                            //返回上一个页面
                            setTimeout(function () {
                                wx.navigateBack({
                                })
                            }, 800);
                        }else {
                            wx.showModal({
                                title: res.data.message,
                                icon: 'error',
                                duration: 500
                            })
                        }
                    }
                })
            }
        }
    },
})
