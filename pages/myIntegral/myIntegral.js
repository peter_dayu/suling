// pages/myIntegral/myIntegral.js
let app = getApp()
var baseUrl = app.globalData.baseUrl;
Page({

    /**
     * 页面的初始数据
     */
    data: {
        current:0,
        integralList:[],
        type:'',
        points:0
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {

    },
    chooseNav:function(e){
        let that = this
        let current = e.currentTarget.dataset.current
        this.setData({
            current: current
        })
        if(current == '1'){
            that.setData({
                type:1
            })
        }else if(current == '2'){
            that.setData({
                type:2
            })
        }else{
            that.setData({
                type:''
            })
        }
        that.queryPointsDetail()
    },
    queryPoints:function(){
        let that = this
        wx.request({
            url: baseUrl + '/center/queryPoints?userid='+app.globalData.userid,
            success: function (res) {
                that.setData({
                    points:res.data
                })
            }
        })
    },
    queryPointsDetail:function(){
        let that = this
        wx.request({
            url: baseUrl + '/center/queryPointsDetail?userid='+app.globalData.userid+'&type='+that.data.type,
            success: function (res) {
                that.setData({
                    integralList:res.data
                })
            }
        })
    },
    goBonusPoints:function(){
        wx.navigateTo({
            url: '../bonusPoints/bonusPoints'
        })
    },
    goIntegralInfos:function(){
        wx.navigateTo({
            url: '../integral/integral'
        })
    },
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {
        this.queryPointsDetail()
        this.queryPoints()
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})
