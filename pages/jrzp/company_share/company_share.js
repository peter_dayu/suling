function getImageInfo(url) {
    return new Promise((resolve, reject) => {
        wx.getImageInfo({
            src: url,
            success: resolve,
            fail: reject,
        })
    })
}

function createRpx2px() {
    const { windowWidth } = wx.getSystemInfoSync()

    return function (rpx) {
        return windowWidth / 750 * rpx
    }
}

const rpx2px = createRpx2px()

function canvasToTempFilePath(option, context) {
    return new Promise((resolve, reject) => {
        wx.canvasToTempFilePath({
            ...option,
            success: resolve,
            fail: reject,
        }, context)
    })
}

function saveImageToPhotosAlbum(option) {
    return new Promise((resolve, reject) => {
        wx.saveImageToPhotosAlbum({
            ...option,
            success: resolve,
            fail: reject,
        })
    })
}

Component({
    properties: {
        visible: {
            type: Boolean,
            value: false,
            observer(visible) {
                if (visible) {
                    this.draw()
                    // this.beginDraw = true
                }
            }
        },
        resultImage: {
            type: String,
            value: false
        },
        recruit: {
            type: Object,
            value: false
        },
        salary: {
            type: String,
            value: false
        }
    },

    data: {
        isDraw: false,
        canvasWidth: 920,
        canvasHeight: 1500,
        imageFile: '',
        responsiveScale: 1,
        tagNames:'',
        bannerUrl:''
    },

    lifetimes: {
        ready() {


            const designWidth = 375
            const designHeight = 603 // 这是在顶部位置定义，底部无tabbar情况下的设计稿高度

            // 以iphone6为设计稿，计算相应的缩放比例
            const { windowWidth, windowHeight } = wx.getSystemInfoSync()
            const responsiveScale =
                windowHeight / ((windowWidth / designWidth) * designHeight)
            if (responsiveScale < 1) {
                this.setData({
                    responsiveScale,
                })
            }
        },
    },

    methods: {
        handleClose() {
            this.triggerEvent('close')
        },
        handleSave() {
            const { imageFile } = this.data

            if (imageFile) {
                saveImageToPhotosAlbum({
                    filePath: imageFile,
                }).then(() => {
                    wx.showToast({
                        icon: 'none',
                        title: '分享图片已保存至相册',
                        duration: 2100,
                    })
                })
            }
        },
        draw() {
            wx.showLoading()
            const { canvasWidth, canvasHeight, resultImage, recruit, salary } = this.data
            this.data.tagNames = ""
            if(recruit.company.bannerDtos.length == 0){
                this.data.bannerUrl = 'https://www.juyuange.net/20200525140832.png'
            }else{
                this.data.bannerUrl = recruit.company.bannerDtos[0].bannerUrl
            }
            const bannerPromise = getImageInfo  (this.data.bannerUrl)
            const resultImagePromise = getImageInfo(resultImage)
            const backgroundPromise = getImageInfo('https://www.juyuange.net/20200525110041.png')
            for(let i = 0;i<recruit.reTags.length;i++){
                this.data.tagNames += recruit.reTags[i].tagName + ' '
            }
            Promise.all([bannerPromise, resultImagePromise, backgroundPromise])
                .then(([banner, rImage, background]) => {
                    const ctx = wx.createCanvasContext('share', this)

                    const canvasW = rpx2px(canvasWidth * 2)
                    const canvasH = rpx2px(canvasHeight * 2)
                    // 绘制背景
                    ctx.drawImage(
                        background.path,
                        0,
                        0,
                        canvasW,
                        canvasH
                    )

                    // 绘制公司图片
                    const y = rpx2px(216)
                    ctx.drawImage(
                        banner.path,
                        (canvasW - 700)/ 2,
                        y + 160,
                        700,
                        400,
                    )

                    // 绘制公司名称
                    ctx.setFontSize(45)
                    ctx.setTextAlign('center')
                    ctx.setFillStyle('#0A0A0A')
                    ctx.fillText(
                        recruit.company.shortName,
                        canvasW / 2,
                        y + rpx2px(640 * 2),
                    )
                    //绘制工资
                    ctx.setFontSize(45)
                    ctx.setTextAlign('center')
                    ctx.setFillStyle('#E14057')
                    ctx.fillText(
                        salary,
                        canvasW / 2,
                        y + rpx2px(720 * 2),
                    )
                    //绘制工作标签
                    ctx.setFontSize(40)
                    ctx.setTextAlign('center')
                    ctx.setFillStyle('#333')
                    ctx.fillText(
                        this.data.tagNames,
                        canvasW / 2,
                        y + rpx2px(800 * 2),
                    )

                    ctx.drawImage(
                        rImage.path,
                        (canvasW - 340)/ 2,
                        y + rpx2px(830 * 2),
                        340,
                        340,
                    )
                    //码上报名
                    ctx.setFontSize(45)
                    ctx.setTextAlign('center')
                    ctx.setFillStyle('#333')
                    ctx.fillText(
                        '码上报名',
                        canvasW / 2,
                        y + rpx2px(1240 * 2),
                    )
                    ctx.stroke()

                    ctx.draw(false, () => {
                        canvasToTempFilePath({
                            canvasId: 'share',
                        }, this).then(({ tempFilePath }) => this.setData({ imageFile: tempFilePath }))
                    })
                    wx.hideLoading()
                    this.setData({ isDraw: true })
                })
                .catch(() => {
                    this.setData({ beginDraw: false })
                    wx.hideLoading()
                })
        }
    }
})
