function getImageInfo(url) {
    return new Promise((resolve, reject) => {
        wx.getImageInfo({
            src: url,
            success: resolve,
            fail: reject,
        })
    })
}

function createRpx2px() {
    const { windowWidth } = wx.getSystemInfoSync()

    return function (rpx) {
        return windowWidth / 750 * rpx
    }
}

const rpx2px = createRpx2px()

function canvasToTempFilePath(option, context) {
    return new Promise((resolve, reject) => {
        wx.canvasToTempFilePath({
            ...option,
            success: resolve,
            fail: reject,
        }, context)
    })
}

function saveImageToPhotosAlbum(option) {
    return new Promise((resolve, reject) => {
        wx.saveImageToPhotosAlbum({
            ...option,
            success: resolve,
            fail: reject,
        })
    })
}

Component({
    properties: {
        visible: {
            type: Boolean,
            value: false,
            observer(visible) {
                if (visible) {
                    this.draw()
                    // this.beginDraw = true
                }
            }
        },
        resultImage: {
            type: String,
            value: false
        },
        enterpriseInfo: {
            type: Object,
            value: false
        }
    },

    data: {
        canvasWidth: 920,
        canvasHeight: 1500,
        imageFile: '',
        responsiveScale: 1,
    },

    lifetimes: {
        ready() {


            const designWidth = 375
            const designHeight = 603 // 这是在顶部位置定义，底部无tabbar情况下的设计稿高度

            // 以iphone6为设计稿，计算相应的缩放比例
            const { windowWidth, windowHeight } = wx.getSystemInfoSync()
            const responsiveScale =
                windowHeight / ((windowWidth / designWidth) * designHeight)
            if (responsiveScale < 1) {
                this.setData({
                    responsiveScale,
                })
            }
        },
    },

    methods: {
        handleClose() {
            // this.triggerEvent('close')
            this.setData({
                visible:false
            })
        },
        handleSave() {
            const { imageFile } = this.data
            if (imageFile) {
                wx.saveImageToPhotosAlbum({
                    filePath: imageFile,
                    success:function (data) {
                        wx.showToast({
                            icon: 'none',
                            title: '分享图片已保存至相册',
                            duration: 2100,
                        })
                    },
                    fail: function (err) {
                        if (err.errMsg === "saveImageToPhotosAlbum:fail:auth denied" || err.errMsg === "saveImageToPhotosAlbum:fail auth deny" || err.errMsg === "saveImageToPhotosAlbum:fail authorize no response") {
                            // 这边微信做过调整，必须要在按钮中触发，因此需要在弹框回调中进行调用
                            wx.showModal({
                                title: '提示',
                                content: '需要您授权保存相册',
                                showCancel: false,
                                success: modalSuccess => {
                                    wx.openSetting({
                                        success(settingdata) {
                                            if (settingdata.authSetting['scope.writePhotosAlbum']) {
                                                wx.showModal({
                                                    title: '提示',
                                                    content: '获取权限成功,再次点击图片即可保存',
                                                    showCancel: false,
                                                })
                                            } else {
                                                wx.showModal({
                                                    title: '提示',
                                                    content: '获取权限失败，将无法保存到相册哦~',
                                                    showCancel: false,
                                                })
                                            }
                                        },
                                        fail(failData) {
                                            console.log("failData", failData)
                                        },
                                        complete(finishData) {
                                            console.log("finishData", finishData)
                                        }
                                    })
                                }
                            })
                        }
                    }
                })
            }
        },
        draw() {
            wx.showLoading()
            const { canvasWidth, canvasHeight, resultImage, enterpriseInfo } = this.data
            const iconurlPromise = getImageInfo(this.data.enterpriseInfo.iconurl)
            const resultImagePromise = getImageInfo(resultImage)
            const backgroundPromise = getImageInfo('https://www.juyuange.net/qiyefenxiangtubeijing.png')
            Promise.all([iconurlPromise, resultImagePromise, backgroundPromise])
                .then(([iconurl, rImage, background]) => {
                    const ctx = wx.createCanvasContext('share', this)
                    const canvasW = rpx2px(canvasWidth * 2)
                    const canvasH = rpx2px(canvasHeight * 2)
                    // 绘制背景
                    ctx.drawImage(
                        background.path,
                        0,
                        0,
                        canvasW,
                        canvasH
                    )

                    //绘制公司图片
                    ctx.drawImage(
                        iconurl.path,
                        (canvasW - 750)/2,
                        200,
                        750,
                        500
                    )
                    // 绘制公司名称
                    ctx.setFontSize(45)
                    ctx.setTextAlign('center')
                    ctx.setFillStyle('#0A0A0A')
                    ctx.fillText(
                        '我分享的'+enterpriseInfo.recruitName,
                        canvasW / 2,
                        800,
                    )
                    ctx.stroke()

                    // 绘制公司名称
                    ctx.setFontSize(45)
                    ctx.setTextAlign('center')
                    ctx.setFillStyle('#0A0A0A')
                    ctx.fillText(
                        enterpriseInfo.type,
                        canvasW / 2,
                        880,
                    )
                    ctx.stroke()

                    ctx.drawImage(
                        rImage.path,
                        (canvasW - 340)/ 2,
                        910,
                        340,
                        340,
                    )
                    //码上报名
                    ctx.setFontSize(45)
                    ctx.setTextAlign('center')
                    ctx.setFillStyle('#333')
                    ctx.fillText(
                        '码上报名',
                        canvasW / 2,
                        1300
                    )
                    ctx.draw(false, () => {
                        canvasToTempFilePath({
                            canvasId: 'share',
                        }, this).then(({ tempFilePath }) => this.setData({ imageFile: tempFilePath }))
                    })
                    wx.hideLoading()
                })
                .catch(() => {
                    wx.hideLoading()
                })
        }
    }
})
