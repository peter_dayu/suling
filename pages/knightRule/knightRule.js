// pages/knightRule/knightRule.js
const app = getApp();
var baseUrl = app.globalData.baseUrl;
var fwzsUrl = app.globalData.fwzsUrl;
var login = require('../../utils/login.js');
Page({

    /**
     * 页面的初始数据
     */
    data: {
        showCoupon:false,
        isShow:false,
        isNewUser:false,
        coupons:[],
        visible:app.globalData.visible,
        personalInfo:{}
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        this.setData({
            visible:app.globalData.visible
        })
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {
        // this.getNewUser()
        this.queryMyCoupons()
        this.getInfo()
    },
    join:function(){
        // if(this.data.isNewUser){
        //     wx.navigateTo({
        //         url: '../upgradeAgent/upgradeAgent'
        //     })
        // }else{
            wx.navigateTo({
                url: '../upgradeAgent/upgradeAgent?level='+(this.data.coupons[0]?this.data.coupons[0].level:null)+'&coupon='+(this.data.coupons[0]?1:'')
            })
        // }

    },
    //判断是否为骑士
    getInfo: function () {
        let that = this
        wx.request({
            url: baseUrl + '/info',
            data: {
                "userid": app.globalData.userid
            },
            method: 'GET',
            success: function (res) {
                that.setData({
                    personalInfo: res.data
                })
                // if(res.data.user.isagent == 0){
                //     that.setData({
                //         showCoupon:true,
                //         isNewUser:true
                //     })
                // }
                if (res.data.user.isagent == '0') {
                    that.isgetCoupon()
                } else {
                    that.setData({
                        showCoupon: false
                    })
                }
            }
        })
    },
    // 判断是否领取红包
    isgetCoupon(){
        let that = this
        wx.request({
            url:baseUrl+"/center/hasCoupon99?userid="+app.globalData.userid,
            success:function (res) {
                if(res.data == '1'){
                    that.setData({
                        showCoupon: true
                    })
                }else{
                    that.setData({
                        showCoupon: false
                    })
                }

            }
        })
    },
    // 查询是否是新用户
    // getNewUser(){
    //     let that = this
    //     wx.request({
    //         url: baseUrl + "/center/isNewUser?userid="+app.globalData.userid,
    //         success: function (res) {
    //             if(res.data == '1'){
    //                 that.setData({
    //                     showCoupon:true,
    //                     isNewUser:true
    //                 })
    //             }else{
    //
    //             }
    //         }
    //     })
    // },
    toBuy:function(){
        wx.navigateTo({
            url: '../upgradeAgentAgain/upgradeAgentAgain'
        })
    },
    queryMyCoupons:function(){
        login.getCache(this.queryMyCoupons1)
    },
    queryMyCoupons1(adminToken){
        let that = this
        wx.request({
            url: fwzsUrl + '/api/myCoupons',
            data: { "userid":app.globalData.userid,"type": 'knight'},
            method: 'GET',
            header:{
                "Content-Type":"application/json",
                "Authorization":"Bearer "+adminToken
            },
            success: function (res) {
                if(res.data){
                    that.setData({
                        coupons:res.data
                    })
                }

            }
        })
    },
    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})
