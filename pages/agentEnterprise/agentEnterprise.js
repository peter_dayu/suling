// pages/recommendInfo/recommendInfo.js
let app = getApp()
var baseUrl = app.globalData.baseUrl;
var login = require('../../utils/login.js');
Page({

    /**
     * 页面的初始数据
     */
    data: {
        current:0,
        date:'',
        index:'',
        interviewList:['合格','不合格'],
        statusIndex:'',
        sysCompanyList:[],
        createTime:null,
        reportStatus:0,
        reid:'',
        status:'',
        candidateCurrent:0,
        agentCurrent:0,
        teamCurrent:0,
        agentRecruitsList:[],
        groupIndex:0,
        teamList:[],
        enrollIndex:'',
        showFlag:false,
        recommendId:'',
        enrollCurrent:0,
        level:1,
        images:{},
        totalRcommendBonusExp:0,
        totalRcommendBonusAct:0,
        enrollStatus:[
            {
                name:'全部',
                id:''
            },
            {
                name:'已报名',
                id:'0'
            },
            {
                name:'已入职',
                id:'8'
            }],
        statusId:'',
        isShow:false,
        personalInfo:{}
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        if(options.recommendId){
            login.login()
            this.setData({
                recommendId:options.recommendId
            })

        }
        if(options && options.current){
            this.setData({
                current:options.current
            })
        }
        this.getAgentRecruits()
        this.getSysRecommendRecruitList()
        this.isShowGuide()

    },
    //判断是否为骑士
    getInfo: function () {
        let that = this
        wx.request({
            url: baseUrl + '/info',
            data: {
                "userid": app.globalData.userid
            },
            method: 'GET',
            success: function (res) {
                that.setData({
                    personalInfo: res.data
                })
            }
        })
    },
    // 判断指引是否显示
    isShowGuide:function(){
        let that = this
        wx.request({
            url:baseUrl+"/center/isFirstEnterKnight?userid="+app.globalData.userid,
            success:function (res) {
                if(res.data == '1'){
                    that.setData({
                        isShow:true
                    })
                }else{
                    that.setData({
                        isShow:false
                    })
                }
            }
        })
    },
    //成为骑士
    toKnight:function(){
        wx.navigateTo({
          url: '../knightRule/knightRule'
        })
    },
    updateRecommedUser:function(recommendId){
        if(recommendId){
            wx.request({
                url: baseUrl + '/updRecommendId?userid='+app.globalData.userid +'&recommendId='+recommendId,
                success: function (res) {
                }
            })
        }

    },
    //关闭指引
    closePop:function(){
        this.setData({
            isShow:false
        })
    },

    getSysRecommendRecruitList:function(){
        let that = this
        wx.request({
            url: baseUrl + '/entprz/sysRecommendRecruitList?userid='+app.globalData.userid,
            success: function (res) {
                that.setData({
                    sysCompanyList:res.data
                })
            }
        })
    },
    checkAuth: function(e) {
        app.globalData.isAuth = e.detail.checkAuth
        app.globalData.userInfo = e.detail.userInfo
    },

    getAgentRecruits:function(){
        let that = this
        wx.request({
            url: baseUrl + '/agentRecruits?userid='+app.globalData.userid,
            success: function (res) {

                if(res.data.recruits){
                    var totalRcommendBonusExp=0
                    var totalRcommendBonusAct=0
                    res.data.recruits.forEach(function(item ){
                        totalRcommendBonusExp=totalRcommendBonusExp+item.totalRcommendBonusExp
                        totalRcommendBonusAct=totalRcommendBonusAct+item.totalRcommendBonusAct
                    }
                     )
                }
                that.setData({
                    agentRecruitsList:res.data.recruits,
                    totalRcommendBonusExp,
                    totalRcommendBonusAct
                })
            }
        })
    },

    chooseAgentNav:function(e){
        let current = e.currentTarget.dataset.current
        this.setData({
            agentCurrent: current
        })
    },


    toRecruit:function(e){
        var vid =e.currentTarget.dataset.vid
       wx.navigateTo({
         url: '../enterpriseInfo/enterpriseInfo?id='+vid,
       })
    },
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {
        let that = this
        login.userIdCallback = userid => {
            if(!app.globalData.user.isagent){
                that.updateRecommedUser(that.data.recommendId)
            }

            if(!app.globalData.isAuth){
                that.setData({
                    showFlag:true
                })
            }
        }
        that.getInfo()
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },
    /**
     * 用户点击右上角分享
     */

    onShareAppMessage: function (res) {

        let vid = res.target.dataset.vid
        if (res.from === 'button') {

        }
        return {
            title: '欢迎加入',
            path:  'pages/enterpriseInfo/enterpriseInfo?id=' + vid + '&userid=' + app.globalData.userid + '&current=0',
            success: function (res) {

                wx.showModal({
                  cancelColor: 'cancelColor',
                  content:res
                })
            }
        }
    },

})
