var app = getApp();
var baseUrl = app.globalData.baseUrl;
Page({
  data: {
    num: 0,
    list: [],
    userid: "",
    openid: "",

    indicatorDots: false,
    autoplay: false,
    interval: 3000,
    duration: 800,
  },
  onLoad: function () {
    var that = this;
    var that = this;
    wx.getSystemInfo({
      success: function (res) {
        that.setData({
          winWidth: res.windowWidth,
          winHeight: res.windowHeight
        });
      }
    });
    let postId = this.data.postId;
    try {
      var value = wx.getStorageSync('third_Session')
      if (value) {
        that.setData({
          openid: value.openid
        })
      }
    } catch (e) {

    };
      wx.request({
        url: baseUrl + '/center/getCommodityList',//请求地址
        method: 'GET',
        success: function (res) {
          var list = that.data.list;
          list = res.data;
          that.setData({
            list: list
          })
        },
      })
  },
  onShow(){
    this.points();
  },

  //获取积分
  points: function () {
    let that = this;
    wx.request({
      url: baseUrl + '/queryuser?openid=' + that.data.openid,
      success: res => {
        that.setData({
          userid: res.data.userid
        })
        wx.request({
          url: baseUrl + '/center/queryPoints?userid=' + that.data.userid,//请求地址
          method: 'GET',
          success: function (res) {
            var num = that.data.num;
            num = res.data
            that.setData({
              num: num
            })
          },
        })
      }
    })
  }
})
