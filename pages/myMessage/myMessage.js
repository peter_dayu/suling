// pages/myMessage/myMessage.js
const app = getApp();
var baseUrl = app.globalData.baseUrl;
Page({

  /**
   * 页面的初始数据
   */
  data: {
      chat:[],
      message:{}
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {


  },
    toMyFans:function(){
      wx.navigateTo({
        url: '../myFans/myFans?unread='+this.data.message.unReadFans
      })
    },
    toInteractionMessage:function(){
        wx.navigateTo({
            url: '../interactionMessage/interactionMessage?unread='+this.data.message.unReadHudong
        })
    },
    toNoticeMessage:function(){
        wx.navigateTo({
            url: '../noticeMessage/noticeMessage?unread='+this.data.message.unReadSys
        })
    },
    toChat:function(e){
      wx.navigateTo({
          url: '../chat/chat?to='+e.currentTarget.dataset.userid
      })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
      let that = this
      wx.request({
          url: baseUrl + "/message?userid=" + app.globalData.userid,
          success: function (res) {
              that.setData({
                  chat:res.data.chat,
                  message:res.data.message
              })
          }
      })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})
