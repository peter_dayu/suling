// pages/enterpriseInfo/enterpriseInfo.js
const app = getApp();
var baseUrl = app.globalData.baseUrl;
var fwzs = app.globalData.fwzsUrl;
var myVar;
var login = require('../../utils/login.js');
Page({

    /**
     * 页面的初始数据
     */
    data: {
        current:2,
        pageNo:1,
        videoId:'',
        inter:'',
        videoList:[],
        videoInfo:{},
        showModalStatus: false,
        commentContents:[],
        isChildShow:false,
        enterpriseInfo:{},
        money: ['1000-2000', '2000-3000', '3000-4000', '4000-5000', '5000-6000', '6000-7000', '7000-8000', '8000以上'],
        isHaveSalary:false,
        commentContent:'',
        num:'',
        rightsNum:0,
        fromSet:0,
        moreCommentContent:[],
        pageCommentNo:0,
        recommendId:'',
        groupList:[],
        enterGroupTitle:'加入组团',
        showFlag: false,
        workId:'',
        isfollowed:false,
        isAgent:false,
        visible:false,
        visibleAuth:app.globalData.visible
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        let that = this

        if(options.id){
            this.setData({
                videoId:options.id,
            })
        }
        if(options.current){
            this.setData({
                current:options.current,
            })
        }
        if(options.workId){
            this.setData({
                workId:options.workId,
            })
        }
        if(options.userid){
            this.setData({
                recommendId:options.userid,
            })
        }
        if(options.createid){
            this.setData({
                createId:options.createid,
            })
        }

        if(options && options.scene){
            let scene = decodeURIComponent(options.scene)
            let obj = scene.split("&")
            let userid = obj[0].split("=")[1]
            let current = obj[1].split("=")[1]
            let id = obj[2].split("=")[1]
            that.setData({
                recommendId:userid,
                current:current,
                videoId:id
            })
        }
        if(!app.globalData.userid){
            login.login()
        }

    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {
        console.log(app.globalData.userid,'userid')
        let that = this
        if(app.globalData.userid){
            that.getResume()
            that.getRelateVideo()
            that.getVideo()
            that.getEnterpriseInfo()
        }else{
            login.userIdCallback = userid => {
                that.getResume()
                that.getRelateVideo()
                that.getVideo()
                that.getEnterpriseInfo()
            }
        }

    },
    checkAuth: function(e) {
        app.globalData.isAuth = e.detail.checkAuth
        app.globalData.userInfo = e.detail.userInfo
    },
    //查询是否已经代理 某企业
    isHasAgent:function(){
        let that = this
        wx.request({
            url:baseUrl+"/queryIsAgentRecruit?userid="+app.globalData.userid+"&reid="+that.data.enterpriseInfo.reId,
            success:function (res) {
                if(res.data == '1'){

                    that.generatePic()
                    that.setData({
                        isAgent:true
                    })
                }else if(res.data == '0'){
                    wx.showModal({
                        title: '提示',
                        content: '您已申请过该公司职位分享权限，正在审核中！',
                        success (res) {
                            if (res.confirm) {
                                console.log('用户点击确定')
                            } else if (res.cancel) {
                                console.log('用户点击取消')
                            }
                        }
                    })
                }else{
                    that.getMyAgentRights()
                    that.setData({
                        isAgent:false
                    })
                }

            }
        })
    },
    showLoginModal(){
        if (app.globalData.isAuth == 0||!app.globalData.auth) {
            wx.showModal({
                cancelColor: 'cancelColor',
                title: '提示',
                content: '请先登录',
                success:(res) =>{
                    if (res.confirm) {
                        console.log('用户点击确定')
                        this.setData({
                            showFlag: true
                        })
                    } else if (res.cancel) {
                        console.log('用户点击取消')
                    }

                }
            })
            return false
        }else{
            return true
        }
    },
    getEnterpriseInfo(){
        let that = this
        wx.request({
            url: baseUrl + "/entprz/getRecruitByVid?vid="+that.data.videoId+"&userid="+app.globalData.userid+(that.data.workId?'&type='+that.data.workId:''),
            success: function (res) {
                if(res.data.recruit.customSalary){
                    that.checkNumber(res.data.recruit.customSalary)
                }
                var inter
                if(res.data.recruit.interviewTime){
                    inter = res.data.recruit.interviewTime.substring(0, 10)
                }
                that.setData({
                    visibleAuth:app.globalData.visible,
                    enterpriseInfo: res.data.recruit,
                    isfollowed:res.data.recruit.isFollow==1?true:false,
                    inter
                })

            }
        })
    },
    //查询是否关注企业
    getFollowInfo:function(){
        let that = this
        wx.request({
            url: baseUrl + '/queryFollowRecruit?userid='+app.globalData.userid+'&deptId='+ that.data.enterpriseInfo.deptId,
            success: function (res) {
                if(res.data == '1'){
                    that.setData({
                        isfollowed:true
                    })
                }else{
                    that.setData({
                        isfollowed:false
                    })
                }
            }
        })
    },

    //验证字符串是否是数字
    checkNumber(theObj) {
        var reg = /^[0-9]+.?[0-9]*$/;
        if (reg.test(theObj)) {
            this.setData({
                isHaveSalary:true
            })
            return true;
        }
        return false;
    },
    getVideo(){
        let that = this
        wx.request({
            url: baseUrl + "/video/queryVideoById?userid=" + app.globalData.userid +"&id="+that.data.videoId,
            success: function (res) {
                that.setData({
                    videoInfo: res.data,
                })
            }
        })
    },
    // 判断是否已授权简历
    getResume(){
        let that = this
        wx.request({
            url: baseUrl + '/getcvbyid?userid=' + app.globalData.userid,
            success: function (e) {
                var num = that.data.num
                if (e.data.cv == null) {
                    num = 0
                } else {
                    num = 1
                }
                that.setData({
                    num: num
                })
            }
        });
    },
    chooseNav(e){
        let that = this
        this.setData({
            current: e.currentTarget.dataset.current
        })
        if(this.data.current == 0){
            that.getRelateVideo()
        }
    },
    recommendPlay(e){
        var that =this
        if(that.data.updateVid!=that.data.videoId){
            myVar = setTimeout(()=>{
                login.getCache(that.recommendPlay1)
            }, 10000)
        }
    },
    recommendPlay1(adminToken){
        wx.request({
            url: fwzs + "/api/sendViews/"+this.data.videoId,
            header:{
                "Content-Type": "application/json",
                "Authorization":"Bearer "+adminToken
            },
            success: function (res) {
                console.log(res)
            }
        })
    },


    // 获取相关视频列表
    getRelateVideo(){
        let that =this
        wx.request({
            url: baseUrl + "/video/queryRelateVideo?userid=" + app.globalData.userid +"&id="+that.data.videoId+"&pageNo="+that.data.pageNo+"&pageSize=10",
            success: function (res) {
                that.setData({
                    videoList: res.data,
                })
            }
        })
    },
    // 窗口放大
    goWindowInfo(e){
        this.setData({
            videoId: e.currentTarget.dataset.id
        })
        this.getVideo()
    },
    //评论弹框弹出
    popComments(){
        let that = this
        that.getVideoComments();
    },
    getVideoComments(){
        let that = this
        that.setData({
            showModalStatus:true
        })
        wx.request({
            url: baseUrl + "/video/queryCommentByVid?vid="+that.data.videoId+"&userid="+app.globalData.userid,
            success: function (res) {
                that.setData({
                    commentContents: res.data.pageData,
                    isChildShow:res.data.isChildShow
                })
            }
        })
    },
    getComments(e){
        this.setData({
            commentContent:e.detail.value
        })
    },
    getMoreComments(e){
        let that = this
        let id = e.currentTarget.dataset.id
        let vid = e.currentTarget.dataset.vid
        that.data.pageCommentNo++
        wx.request({
            url: baseUrl + "/video/queryCommentByCommentId?vid="+vid+"&commentId="+id+"&userid="+app.globalData.userid+"&pageNo="+that.data.pageCommentNo+"&pageSize=10",
            success: function (res) {
                that.setData({
                    moreCommentContent:[...that.data.moreCommentContent,...res.data],
                })
            }
        })
    },
    getAgentRecruits:function(){
        let that = this
        wx.request({
            url: baseUrl + '/agentRecruits?userid='+app.globalData.userid,
            success: function (res) {
                that.setData({
                    agentRecruitsList:res.data.recruits
                })
            }
        })
    },
    getMyAgentRights:function(){
        let that = this
        wx.request({
            url: baseUrl + '/myAgentRights?userid='+app.globalData.userid,
            success: function (res) {
                let rightsNum =res.data.total-res.data.used
                that.setData({
                    rightsNum,
                    fromSet:res.data.fromSet
                })
                if(rightsNum > 0){
                    wx.navigateTo({
                        url: '../recommendShare/recommendShare?reId=' + that.data.enterpriseInfo.reId
                    })
                }else{
                    if(that.data.fromSet == '0'){
                        wx.navigateTo({
                            url: '../upgradeAgent/upgradeAgent'
                        })
                    }else{
                        wx.navigateTo({
                            url: '../upgradeAgentAgain/upgradeAgentAgain'
                        })
                    }
                }
            }
        })
    },
    //去评论
    toComments(e){
        let that= this
        if(!that.showLoginModal()){
            return false
        }
        let vid = e.currentTarget.dataset.vid
        if(that.data.commentContent ==""){
            wx.showModal({
                title: "提示",
                content: "请输入评论内容哦～"
            })
            return false
        }
        wx.request({
            url: baseUrl + "/video/addComment",
            data: {
                commentId:that.data.commentId,
                vid: vid,
                userid: app.globalData.userid,
                content: that.data.commentContent,
            },
            method:"Post",
            success: function (res) {
                // that.popComments()
                if(res.data == '1'){
                    wx.showToast({
                        title: '评论成功',
                        icon: 'success',
                        duration: 2000
                    })
                    that.getVideo()
                    that.setData({
                        showModalStatus:false,
                        commentId:''
                    })
                }
            }
        })
    },
    //点赞主视频
    thumbs_up(e){
        let id = e.currentTarget.dataset.id
        let that = this
        if(!that.showLoginModal()){
            return false
        }
        wx.request({
            url: baseUrl + "/video/star",
            data: {
                vid: id,
                userid: app.globalData.userid,
                isStar: !that.data.videoInfo.stars,
            },
            method:"Post",
            success: function (res) {
                that.getVideo()
            }
        })
    },
    //点赞评论
    goCommentsLike(e){
        let that = this
        let id = e.currentTarget.dataset.id
        let vid = e.currentTarget.dataset.vid
        let stars = e.currentTarget.dataset.stars
        wx.request({
            url: baseUrl + "/video/star",
            data: {
                commentId:id,
                vid: vid,
                userid: app.globalData.userid,
                isStar: !stars,
            },
            method:"Post",
            success: function (res) {
                wx.request({
                    url: baseUrl + "/video/queryCommentByVid?vid="+vid+"&userid="+app.globalData.userid,
                    success: function (res) {
                        that.setData({
                            commentContents: res.data.pageData
                        })
                    }
                })
            }
        })
    },
    //关闭评论弹框
    closeModal(){
        let that = this
        that.setData({
            showModalStatus:false
        })
    },
    // 点击回复弹出的评论弹框
    showInput(e){
        let id= e.currentTarget.dataset.id
        this.setData({
            isFocus:true,
            commentId: id
        })
    },
    // 拨打联系电话
    makePhone(e){
        wx.makePhoneCall({
            phoneNumber: e.currentTarget.dataset.phone,
            success: function () {
            }
        })
    },
    //报名
    goEnroll(){
        let that = this
        if(that.data.enterpriseInfo.report == '0'){
            if(!app.globalData.isAuth){
                that.setData({
                    showFlag:true
                })
                return false
            }
            if (that.data.num == 0) {
                wx.showModal({
                    title: '请先填写简历',
                    content: '暂无简历',
                    success: function (res) {
                        if (res.confirm) {
                            wx.navigateTo({
                                url: '../deliver/deliver',
                            })
                        }
                    }
                })
            } else {
                var recommendType = that.data.recommendId ? 3:1
                var recommendId = that.data.recommendId ? that.data.recommendId : that.data.videoInfo.userid
                wx.request({
                    url: baseUrl + "/sendingApplications?reId=" + that.data.enterpriseInfo.reId + "&userid=" + app.globalData.userid + '&interviewTime=' + that.data.enterpriseInfo.interviewTime +'&recommendId='+ recommendId +'&recommendType='+recommendType,
                    success: function (res) {
                        if(res.data.result.code == '1'){
                            wx.showToast({
                                title: '报名成功',
                            })
                            that.getEnterpriseInfo()
                        }else {
                            wx.showModal({
                                content: res.data.result.message,
                            })
                        }
                    }
                })
            }
        }
    },
    cancelEnroll: function (e) {
        var that = this;
        wx.showModal({
            title: '是否取消面试',
            cancelText:"取消",
            content: '提示',
            success: function (res) {
                if (res.confirm) {
                    wx.request({
                        url: baseUrl + "/cancelApplications?reId=" +  that.data.enterpriseInfo.reId + "&userid=" + app.globalData.userid + "&isapplication=0" + '&interviewTime=' +  that.data.enterpriseInfo.interviewTime,
                        success: function (res) {

                            if (res.data.result=='success') {
                                wx.showToast({
                                    title: '取消成功',
                                })
                                let result = "enterpriseInfo.report"
                                that.setData({
                                    [result]: 0,
                                })
                            }
                        }
                    })

                }else{
                    return false
                }
            }
        })
    },
    goGroupShare(){
        let that = this
        wx.navigateTo({
            url: '../groupShare/groupShare?reId=' + that.data.enterpriseInfo.reId
        })
    },
    toRecommendShare(){

        let that = this
        that.isHasAgent()


    },
    //生成企业分享图
    generatePic:function(){
        let that = this
        wx.cloud.callFunction({
            name: 'generateQR',
            data: {
                page: 'pages/enterpriseInfo/enterpriseInfo',
                scene: "user=" + app.globalData.userid +'&cur='+that.data.current+'&id='+that.data.enterpriseInfo.vid,
                width: 300
            },
            success(res) {
                that.setData({
                    resultImage: res.result,
                    enterpriseInfo: that.data.enterpriseInfo,
                    visible: true,
                })
            }
        })

    },

    //关注企业
    follow:function(){
        let that = this
        wx.request({
            url:baseUrl+'/video/followHr?userid='+app.globalData.userid+"&deptId="+that.data.enterpriseInfo.deptId,
            success:function (res) {
                if(res.data == "1"){
                    wx.showToast({
                        title: '关注成功',
                        icon: 'success',
                        duration: 1000
                    })
                    that.getFollowInfo()
                }else{
                    wx.showToast({
                        title: '关注失败',
                        icon: 'error',
                        duration: 1000
                    })
                }
            }
        })
    },
    //取消关注企业
    unfollow:function(){
        let that = this
        wx.request({
            url:baseUrl+'/unFollowRecruit?userid='+app.globalData.userid+"&deptId="+that.data.enterpriseInfo.deptId,
            success:function (res) {
                if(res.data.status == "1"){
                    wx.showToast({
                        title: '取消关注成功',
                        icon: 'success',
                        duration: 1000
                    })
                    that.getFollowInfo()
                }else{
                    wx.showToast({
                        title: '取消关注失败',
                        icon: 'error',
                        duration: 1000
                    })
                }
            }
        })
    },
    // 去领取红包
    goHb:function(){
        wx.navigateTo({
            url: '../redEnvelope/redEnvelope',
        })
    },
    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {
        clearTimeout(myVar);
    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {


    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function (res) {
        if (res.from === 'button') {

        }
        return {
            title: '职位详情',
            path: 'pages/enterpriseInfo/enterpriseInfo?userid='+app.globalData.userid+'&id='+this.data.videoId,
            success: function (res) {
                wx.showModal({
                    cancelColor: 'cancelColor',
                    content:res
                })
            }
        }
    },
    /**
     * 企业详情分享朋友圈
     * @param {*} res
     */
    onShareTimeline:function(res){
        let id =this.data.videoId
        let title = this.data.videoInfo.title
        let image = this.data.videoInfo.imgCover
        return{
            title:title,
            imageUrl:image,
            query:'id='+id
        }
    },
})
