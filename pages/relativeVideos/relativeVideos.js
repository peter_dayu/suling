// pages/relativeVideos/relativeVideos.js
const app = getApp();
var fwzsUrl = app.globalData.fwzsUrl;
var baseUrl = app.globalData.baseUrl;
Page({

    /**
     * 页面的初始数据
     */
    data: {
        videoList:[],
        pageNo:1,
        id :''
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
       if(options.id){
           this.setData({
              id:options.id
           })
        }
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {
        let that = this
        that.setData({
            videoList:[]
        })
        this.getRelateVideo()
    },
    getRelateVideo:function(){
        let that = this
        wx.request({
            url:baseUrl+"/video/queryAgentRecruitRelateVideo?pageNo="+1+'&pageSize=10&id='+that.data.id,
            success:function (res) {
                //  videoList:[...that.data.videoList,...res.data]
                if(res.data[0]!=null){
                    that.setData({
                        videoList:res.data
                    })
                }
            }
        })
    },
    goRecommendVideoInfos:function(e){
        wx.navigateTo({
            url: '../enterpriseInfo/enterpriseInfo?id='+e.currentTarget.dataset.id
        })
    },
    getTitle:function(e){
        var name1 = e.detail.value;
        var that = this;
        if(name1){
            wx.request({
                url:baseUrl+"/video/queryAgentRecruitRelateVideo?pageNo="+1+'&pageSize=10&id='+that.data.id+"&title="+name1,
                success:function (res) {
                    //  videoList:[...that.data.videoList,...res.data]
                    if(res.data[0]!=null){
                        that.setData({
                            videoList:res.data
                        })
                    }
                }
            })
        }
    },
    relate:function(e){
        const pages = getCurrentPages()
        const prevPage = pages[pages.length-2] // 上一页// 调用上一个页面的setData 方法，将数据存储
        prevPage.setData({
            vid:e.currentTarget.dataset.id,
            related:1
        })
        wx.navigateBack({
            delta: 1
        })
        // wx.navigateBack({

        // })
    },
    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})
