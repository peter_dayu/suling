// pages/myGroup/myGroup.js
let app = getApp()
var baseUrl = app.globalData.baseUrl;
Page({

  /**
   * 页面的初始数据
   */
  data: {
      groupList:[],
      date:'',
      reid:'',
      companyList:[],
      index:'',
      status:['组团失败','组团成功'],
      statusIndex:'',

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getGroupInfo()
    this.getCompanyList()
  },
    getGroupInfo:function(){
        let that = this
        wx.request({
            url: that.data.date?baseUrl + '/myGroupInfo?userid='+app.globalData.userid+'&createTime='+that.data.date+ ' 00:00:00'+'&reid='+that.data.reid+'&finish='+that.data.statusIndex:baseUrl + '/myGroupInfo?userid='+app.globalData.userid+'&reid='+that.data.reid+'&finish='+that.data.statusIndex,
            method: 'GET',
            success: function (res) {
                that.setData({
                    groupList:res.data
                })
            }
        })
    },
    getCompanyList:function(){
        let that = this
        wx.request({
            url: baseUrl + '/entprz/myGroupRecruitList',
            data: { "userid": app.globalData.userid },
            method: 'GET',
            success: function (res) {
                that.setData({
                    companyList:res.data
                })
            }
        })
    },
    goMemberInfo:function(e){
        let that = this
        let id = e.currentTarget.dataset.id
        wx.navigateTo({
            url: '../groupMemberInfo/groupMemberInfo?id='+id,
        })
    },
    bindPickerChange:function(e){
        let data = e.detail.value
        this.setData({
            date:data
        })
        this.getGroupInfo()
    },
    bindCompanyChange:function(e){
        let that = this
        let index = e.detail.value
        that.setData({
            index:index,
            reid:that.data.companyList[index].reId
        })
        that.getGroupInfo()
    },
    bindStatusChange:function(e){
        let index = e.detail.value
        this.setData({
            statusIndex:index
        })
        this.getGroupInfo()
    },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})
