// pages/invite/invite.js
const app = getApp();
var fwzsUrl = app.globalData.fwzsUrl;
var baseUrl = app.globalData.baseUrl;
var login = require('../../utils/login.js');
Page({

    /**
     * 页面的初始数据
     */
    data: {
        showRule:false,
        showInvite:false,
        showHelp:false,
        showCut:false,
        showHb:false,
        showError:false,
        RecommendUserId:'',
        isNewUser:false,
        showFlag:false,
        type:'',
        bargainList:{},
        priceList:[],
        quota:1,
        price:0,
        current:0,
        showBargined:false,
        barHeight:0,
        cutInfo:{},
        visible:false,
        resultImage:{},
        difference:'',
        timer:''
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        let that = this
        if(options && options.id){
            that.setData({
                RecommendUserId:options.id
            })
        }
        if(options && options.current){
           var current = options.current
        }
        if(options && options.scene){
            let scene = decodeURIComponent(options.scene)
            let obj = scene.split("&")
            let id = obj[0].split("=")[1]
            var current = obj[1].split("=")[1]
            that.setData({
                RecommendUserId:id
            })
        }
        if(current){
            that.setData({
                current:current
            })
            if(current == '0'){
                that.setData({
                    type:'knight1',
                })
            }else if(current == '1'){
                that.setData({
                    type:'knight2',
                })
            }else {
                that.setData({
                    type:'knight3',
                })
            }
        }
        let menuButton=wx.getMenuButtonBoundingClientRect()
        that.setData({
            barHeight:menuButton.top
        })
        if(!app.globalData.isAuth){
            that.setData({
                showFlag:true
            })
        }
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },
    toRule:function(){
        this.setData({
            showRule:true
        })
    },
    close:function(){
        this.setData({
            showRule:false
        })
    },
    back:function(){
        let that = this
        if(that.data.RecommendUserId){
            wx.switchTab({
              url: '../index/index'
            })
        }else{
            wx.navigateBack({
                detal:1
            })
        }
    },
    invite:function(){
        this.setData({
            showInvite:true
        })
    },
    cancel:function(){
        this.setData({
            showInvite:false
        })
    },
    closeCut:function(){
        this.setData({
            showCut:false,
            showHb:true
        })
    },
    closeHb:function(){
        this.setData({
            showHb:false
        })
    },
    closeHelp:function(){
        this.setData({
            showHelp:false
        })
    },
    closeError:function(){
        this.setData({
            showError:false
        })
        wx.switchTab({
            url:'../index/index'
        })
    },
    closeMyError:function(){
        this.setData({
            showMyError:false
        })
    },
    closeBargined:function(){
        this.setData({
            showBargined:false
        })
    },
    receive:function(){
        login.getCache(this.getCoupon)

    },
    getCoupon(adminToken){
        let that = this
        wx.request({
            url: fwzsUrl + '/api/getCoupon99?userid='+app.globalData.userid,
            header:{
                "Content-Type": "application/json",
                "Authorization":"Bearer "+adminToken
            },
            success: function (res) {
                if(res.data.code == '1'){
                    wx.navigateTo({
                        url: '../couponSuccess/couponSuccess'
                    })
                }else{
                    wx.showToast({
                        title: '领取失败'

                    })
                }
            }
        })
    },
    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

        let that = this
        that.query()
        that.getNewUser()
        if(that.data.RecommendUserId){
            that.setData({
                showHelp:true
            })
        }
        that.data.timer = setInterval(function () {
            that.handleTime()
        },1000)
    },
    checkAuth: function(e) {
        app.globalData.isAuth = e.detail.checkAuth
        app.globalData.userInfo = e.detail.userInfo
    },
    //查询价格
    query:function(){
        login.getCache(this.queryPrice)
    },
    queryPrice:function (adminToken){
        let that = this
        var current=that.data.current
        wx.request({
            url: fwzsUrl + "/api/vip/queryPrice?type=agent&level=1",
            header: {
                'content-type': 'application/json', // 默认值
                'Authorization':'Bearer '+adminToken
            },
            success: function (res) {

                that.setData({
                    priceList:res.data,
                })
                if(that.data.current == '0'){
                    that.setData({
                        price:that.data.priceList[0].totalPrice,
                        quota:1
                    })
                }else if(that.data.current == '1'){
                    that.setData({
                        price:that.data.priceList[1].totalPrice,
                        quota:3
                    })
                }else{
                    that.setData({
                        price:that.data.priceList[2].totalPrice,
                        quota:5
                    })
                }
                that.queryBargain()
            }
        })
    },
    // 查询是否是新用户
    getNewUser(){
        let that = this
        wx.request({
            url: baseUrl + "/center/isBargainUser?userid="+app.globalData.userid,
            success: function (res) {
                if(res.data == '1'){
                    that.setData({
                        isNewUser:true
                    })
                }else{
                    that.setData({
                        isNewUser:false
                    })
                }
            }
        })
    },
    goBargain:function(){
        login.getCache(this.bargain)
    },
    bargain:function(adminToken){
        let that = this
        wx.request({
            url:fwzsUrl+"/api/bargain",
            header: {
                'content-type': 'application/json', // 默认值
                'Authorization':'Bearer '+adminToken
            },
            method:'Post',
            data:{
                type:that.data.type,
                newUserid:app.globalData.userid,
                userid:that.data.RecommendUserId
            },
            success:function (res) {
                if(res.data.status == '-1'){
                    wx.showToast({
                        title: res.data.message,
                        icon: 'success',
                        duration: 2000
                    })
                }else if(res.data.status == '1'){

                    that.setData({
                        showCut:true,
                        cutInfo:res.data
                    })
                    that.queryBargain()
                }
            }
        })
    },

    //助力
    help(){
        let that = this
        that.setData({
            showHelp:false
        })

        if(that.data.isNewUser){
            that.goBargain()
        }else{
            if(that.data.RecommendUserId == app.globalData.userid){
                that.setData({
                    showMyError:true
                })
            }else{
                that.setData({
                    showError:true
                })
            }
        }
    },
    //查询砍价结果列表
    queryBargain:function(){
        login.getCache(this.queryBargainList)
    },
    queryBargainList:function(adminToken){
        let that = this
        wx.request({
            url:fwzsUrl+"/api/bargainList",
            header: {
                'content-type': 'application/json', // 默认值
                'Authorization':'Bearer '+adminToken
            },
            method:'Post',
            data:{
                // userid:that.data.RecommendUserId?that.data.RecommendUserId:app.globalData.userid,
                userid:app.globalData.userid,
            },
            success:function (res) {
                let difference = (that.data.price - res.data.totalAmount).toFixed(2)


                that.setData({
                    bargainList:res.data,
                    difference:difference
                })
            }
        })
    },
    //加endtime
    handleTime:function(){
        let that = this
        if(that.data.bargainList.remainingTime != '-1'){
            let time = that.showEndTime(that.data.bargainList.remainingTime)
            that.data.bargainList.endTime = time
            that.setData({
                bargainList:that.data.bargainList
            })

        }
    },
    //时间转化
    showEndTime(t){
        let nowTime = new Date().getTime();
        let endTime = t - nowTime;
        let days = parseInt(endTime / (1000 * 60 * 60 * 24));
        let hours = parseInt((endTime % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)) < 10? "0"+parseInt((endTime % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)):parseInt((endTime % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        let minutes = parseInt((endTime % (1000 * 60 * 60)) / (1000 * 60)) <10 ? "0"+parseInt((endTime % (1000 * 60 * 60)) / (1000 * 60)):parseInt((endTime % (1000 * 60 * 60)) / (1000 * 60));
        let seconds = parseInt((endTime % (1000 * 60)) / 1000) <10 ? "0"+parseInt((endTime % (1000 * 60)) / 1000):parseInt((endTime % (1000 * 60)) / 1000);
        endTime=hours+'时'+minutes+'分'+seconds+'秒'
        return endTime
    },
    //生成面对面分享图
    generatePic:function(){
        let that = this
        wx.cloud.callFunction({
            name: 'generateQR',
            data: {
                page: 'pages/invite/invite',
                scene: "id=" + app.globalData.userid +'&current='+that.data.current,
                width: 300
            },
            success(res) {
                that.setData({
                    resultImage: res.result,
                    userInfo: app.globalData.userInfo,
                    visible: true,
                    showInvite:false
                })
            }
        })

    },
    // 直接购买
    buy:function(){
        wx.navigateTo({
            url: '../upgradeAgent/upgradeAgent?bargain=1'
        })
    },
    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {
        let that = this
        clearInterval(that.data.timer);

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {
        let that = this
        clearInterval(that.data.timer);
    },


    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function (res) {
        let that = this
        that.setData({
            showInvite:false
        })
        if (res.from === 'button') {
        }
        return {
            title: '快帮我砍一刀',
            path: 'pages/invite/invite?id='+app.globalData.userid+'&current='+that.data.current,
            success: function (res) {

            }
        }
    },
    onShareTimeline: function(res){
        return {
            title: '快帮我砍一刀',
            path: '../invite/invite?id='+app.globalData.userid+'&current='+that.data.current,
        }

    }
})
