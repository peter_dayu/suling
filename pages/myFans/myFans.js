// pages/myFans/myFans.js
const app = getApp();
var baseUrl = app.globalData.baseUrl;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    fansList:[],
      userid:''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
      this.setData({
          userid:options.userid
      })
      if(options.unread&&options.unread>0){
        this.clearUnreadFansCache()
      }

  },
  clearUnreadFansCache:function(){
    wx.request({
      url: baseUrl+'/cache/clearUnreadFansCache?userid='+app.globalData.userid,
      success:function(res){

      }
    })
  },
  getMyFans:function(){
    let that = this
      let userid = that.data.userid?that.data.userid:app.globalData.userid
      wx.request({
          url: baseUrl + "/video/queryMyFans?userid="+userid,
          success: function (res) {
              that.setData({
                  fansList:res.data
              })
          }
      })
  },
  cancelFollow:function(e){
      let that = this
      let followId = e.currentTarget.dataset.followid
      wx.showModal({
          title: '提示',
          content: '确认取消关注',
          success (res) {
              if (res.confirm) {
                  wx.request({
                      url: baseUrl + "/video/unFollow?userid="+app.globalData.userid+"&followId="+followId,
                      success: function (res) {
                          that.getMyFans()
                      }
                  })
              } else if (res.cancel) {
              }
          }
      })


  },
  follow:function(e){
      let that = this
      let followId = e.currentTarget.dataset.followid
      wx.request({
          url: baseUrl + "/video/follow?userid="+app.globalData.userid+"&followId="+followId,
          success: function (res) {
            if(res.data == '1'){
                wx.showToast({
                    title: '关注成功',
                    icon: 'success',
                    duration: 2000
                })
                that.getMyFans()
            }
          }
      })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
      this.getMyFans()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})
