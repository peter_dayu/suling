// pages/personalInfo/personalInfo.js
let app = getApp()
var baseUrl = app.globalData.baseUrl;
Page({

  /**
   * 页面的初始数据
   */
  data: {
      avatarUrl:'',
      nickName:'',
      personalInfo:{},
      current:0,
      userid:'',
      videoPage:1,
      followPage:1,
      starPage:1,
      myFollowVideo:[],
      myStarVideo:[],
      myVideo:[],
      recruits:[],
      videoTotalCount:0,
      followTotalCount:0,
      starTotalCount:0,
      type:0,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
      let userid = options.userid
      let type = options.type
      this.setData({
          userid:userid,
          type:type,
          avatarUrl:app.globalData.userInfo.avatarUrl,
          nickName:app.globalData.userInfo.nickName
      })
      if(type==1){
        this.getEnterpriseUserInfo(userid)
      }else {
        this.getInfo(userid)
      }
      this.getMyVideo()
  },
    getInfo:function(userid){
        let that = this
        wx.request({
            url: baseUrl + '/info',
            data: {
                "userid": userid,
                "userid1":app.globalData.userid
            },
            method: 'GET',
            success: function (res) {
                that.setData({
                    personalInfo:res.data
                })
            }
        })
    },
    getEnterpriseUserInfo:function(userid){
        let that = this
        wx.request({
            url: baseUrl + '/queryEnterpriseUserInfo',
            data: {
                "deptId": userid,
                "userid1":app.globalData.userid
            },
            method: 'GET',
            success: function (res) {
                that.setData({
                    personalInfo:res.data
                })
            }
        })
    },
    getRecruits: function () {
        let that = this
        wx.request({
            url: baseUrl + "/agentRecruits?userid=" + that.data.userid,
            success: function (res) {
                that.setData({
                    userInfo: res.data.user,
                    recruits: res.data.recruits
                })
            }
        })
    },
    getRecruitsByDeptId:function(){
        let that = this
        wx.request({
            url: baseUrl + "/video/queryAll?deptId="+that.data.userid+"&userid="+app.globalData.userid+"&type=1"  ,
            success: function (res) {
                that.setData({
                    recruits: res.data
                })
            }
        })
    },
    goEnterpriseInfo(e) {
        let id = e.currentTarget.dataset.id
        let userid = e.currentTarget.dataset.userid
        wx.navigateTo({
            url: '../enterpriseInfo/enterpriseInfo?id=' + id + '&userid=' + userid + '&current=0'
        })
    },
    chooseNav:function(e){
        let that =this
        let current = e.currentTarget.dataset.current
        this.setData({
            current: current
        })
        // if(current == '1'){
        //     that.setData({
        //         myFollowVideo:[],
        //         followPage:1,
        //     })
        //     that.getMyFollowVideo()

        // }
         if(current == '1'){
            that.setData({
                recruits:[]
            })
            if(that.data.type==1){
                that.getRecruitsByDeptId()
            }else{
                that.getRecruits()
            }

        }
        else{
            that.setData({
                myVideo:[],
                videoPage:1,
            })
            that.getMyVideo()
        }
    },
    getMyVideo:function(){
        let that = this
        wx.request({
            url: baseUrl + '/video/myVideo?userid0='+that.data.userid+'&pageNo='+that.data.videoPage+'&pageSize=12',
            success: function (res) {
                that.setData({
                    myVideo:[...that.data.myVideo,...res.data.pageData],
                    videoTotalCount:res.data.totalCount
                })
            }
        })
    },
    getMyFollowVideo:function(){
        let that = this
        wx.request({
            url: baseUrl + '/video/myFollowVideo?userid0='+that.data.userid+'&pageNo='+that.data.followPage+'&pageSize=12',
            success: function (res) {
                that.setData({
                    myFollowVideo:[...that.data.myFollowVideo,...res.data.pageData],
                    // myFollowVideo:res.data.pageData,
                    followTotalCount:res.data.totalCount
                })
            }
        })
    },
    getMyStarVideo:function(){
        let that = this
        wx.request({
            url: baseUrl + '/video/myStarVideo?userid0='+that.data.userid+'&pageNo='+that.data.starPage+'&pageSize=12',
            success: function (res) {
                that.setData({
                    myStarVideo:[...that.data.myStarVideo,...res.data.pageData],
                    // myStarVideo:res.data.pageData,
                    starTotalCount:res.data.totalCount
                })
            }
        })
    },
    toFollow:function(){
        let that = this
        let urlUser=baseUrl + "/video/follow?userid="+app.globalData.userid+"&followId="+that.data.userid
        let urlRecruit=baseUrl + "/video/followHr?userid="+app.globalData.userid+"&deptId="+that.data.userid
        wx.request({
            url:(that.data.type==0||that.data.type==4||that.data.type==3)?urlUser:urlRecruit,
            success: function (res) {
                wx.showToast({
                    title: '关注成功',
                    icon: 'success',
                    duration: 2000
                })
                if(that.data.type==1){
                    that.getEnterpriseUserInfo(that.data.userid)
                }else {
                    that.getInfo(that.data.userid)
                }
            }
        })
    },
    toUnfollow:function(){
        let that = this
        let urlUser=baseUrl + "/video/unFollow?userid="+app.globalData.userid+"&followId="+that.data.userid
        let urlRecruit=baseUrl + "/unFollowRecruit?userid="+app.globalData.userid+"&deptId="+that.data.userid
        wx.request({
            url:(that.data.type==0||that.data.type==4||that.data.type==3)?urlUser:urlRecruit,
            success: function (res) {
                wx.showToast({
                    title: '取消关注成功',
                    icon: 'success',
                    duration: 2000
                })
                if(that.data.type==1){
                    that.getEnterpriseUserInfo(that.data.userid)
                }else {
                    that.getInfo(that.data.userid)
                }
            }
        })
    },
    toChat:function(e){
        wx.navigateTo({
            url: '../chat/chat?to='+e.currentTarget.dataset.userid
        })
    },
    goVideoInfo:function(e){
        let id = e.currentTarget.dataset.id
        let type = e.currentTarget.dataset.type
        wx.navigateTo({
            url: '../videoInfo/videoInfo?id='+id+'&type='+type,
        })
    },
    queryFans:function(e){
        let userid = e.currentTarget.dataset.userid
        wx.navigateTo({
            url: '../myFans/myFans?userid='+userid,
        })
    },
    toFollow1:function(e){
        let userid = e.currentTarget.dataset.userid
        wx.navigateTo({
            url: '../follow/follow?userid='+userid,
        })
    },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
      let that = this
      if(that.data.current == '0'){
          if(that.data.videoPage * 12 < that.data.videoTotalCount){
              that.data.videoPage++
              that.getMyVideo()
          }
      }else if(that.data.current == '1'){
          if(that.data.followPage * 12 < that.data.followTotalCount){
              that.data.followPage++
              that.getMyFollowVideo()
          }
      }else{
          if(that.data.starPage * 12 < that.data.starTotalCount){
              that.data.starPage++
              that.getMyStarVideo()
          }
      }
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})
