// pages/bonusPoints/bonusPoints.js
const app = getApp();
var baseUrl = app.globalData.baseUrl;
Page({

    /**
     * 页面的初始数据
     */
    data: {
        register:'',
        reported:''
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        let that = this
        wx.request({
            url: baseUrl + "/center/queryPointTaskDetail",
            data: {"userid": app.globalData.userid},
            method: 'GET',
            success: function (res) {
                that.setData({
                    register:res.data.register,
                    reported:res.data.reported
                })
            }
        })
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },
    toResume:function(){
        wx.switchTab({
            url:"../me/me"
        })
    },
    goApplyInterview:function(){
        wx.showModal({
            title: '提示',
            content: '快去参加面试吧',
            success: function (res) {
            }
        })
    },
    toJobList:function(){
        wx.showModal({
            title: '提示',
            content: '快去首页报名吧',
            success: function (res) {
            }
        })
    },
    toInvite:function(){
        wx.switchTab({
            url:"../me/me"
        })
    },
    toEntryList:function(){
        wx.navigateTo({
          url: '../friendEntry/friendEntry'
        })
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})
