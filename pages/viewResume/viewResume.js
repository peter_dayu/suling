var app = getApp()
var baseUrl = app.globalData.baseUrl;
Page({
    data: {
        /**
         * 页面配置
         */
        winWidth:'',
        current:0,
        hasCv:false,
        currentTab: 0,
        list: [],
        list1: [],
        list2: [],
        list3: [],
        a: '',
        b: '',
        c: '',
    },

    onLoad: function () {
        var that = this;
        that.getUserJob()
        that.getcvbyid()
    },
    getUserJob:function(current){
        var that = this
        wx.request({
            url: baseUrl + "/entprz/listuserjob?userid=" + app.globalData.userid + '&isapplication=1',
            success: function (res) {
                var list1 = []
                var list2 = []
                var list3 = []
                that.list = res.data
                for (var i = 0; i < that.list.length; i++) {
                    if (that.list[i].status == 0 || that.list[i].status == 1 || that.list[i].status == 2) {
                        list1.push(that.list[i])
                    } else if (that.list[i].status == 3 || that.list[i].status == 4 || that.list[i].status == 7) {
                        list2.push(that.list[i])
                    } else if (that.list[i].status == 8 || that.list[i].status == 9 || that.list[i].status == 10|| that.list[i].status == 11) {
                        list3.push(that.list[i])
                    }
                }
                var a = that.data.a;
                var b = that.data.b;
                var c = that.data.c;


                if (list3.length == 0) {
                    c = 1;
                } else {
                    c = 0
                }
                if (list2.length == 0) {
                    b = 1
                } else {
                    b = 0
                }
                if (list1.length == 0) {
                    a = 1
                } else {
                    a = 0
                }
                that.setData({
                    a: a,
                    b: b,
                    c: c,
                    list1: list1,
                    list2: list2,
                    list3: list3
                })
                if(!current){
                    if(that.data.a == '0'){
                        that.setData({
                            currentTab:0
                        })
                    }else if(that.data.b == '0'){
                        that.setData({
                            currentTab:1
                        })
                    }else if(that.data.c == '0'){
                        that.setData({
                            currentTab:2
                        })
                    }else{
                        that.setData({
                            currentTab:0
                        })
                    }
                }
            }
        })
    },
    getcvbyid:function(){
        let that = this
        wx.request({
            url: baseUrl + "/getcvbyid?userid=" + app.globalData.userid,
            success: function (res) {
                if (res.data.cv == null) {
                    wx.showModal({
                        title: '请先填写简历',
                        content: '暂无简历',
                        success: function (res) {
                            if (res.confirm) {
                                wx.navigateTo({
                                    url: '../deliver/deliver',
                                })
                            }
                        }
                    })
                } else {
                    that.setData({
                        hasCv:true
                    })
                }
            },
        })
    },
    makePhone: function (e) {
        wx.makePhoneCall({
            phoneNumber: e.currentTarget.dataset.phone,
            success: function () {
            }
        })
    },
    /**
     * 点击tab切换
     */
    swichNav: function (e) {
        var that = this;

        if (this.data.currentTab === e.target.dataset.current) {
            return false;
        } else {
            that.setData({
                currentTab: e.target.dataset.current,
            })
            that.getUserJob(e.target.dataset.current);
        }
    },
    remove: function (e) {
        var that = this;
        var index = e.currentTarget.dataset.index;
        var list = that.data.list1;
        let reId = list[index].reId
        let interviewTime = list[index].interviewTime
        if (!that.pageLoading) {
            that.pageLoading = !0
            wx.showModal({
                title: '是否取消面试',
                content: '提示',
                success: function (res) {
                    if (res.confirm) {
                        wx.request({
                            url: baseUrl + "/cancelApplications?reId=" + reId + "&userid=" + app.globalData.userid + "&isapplication=0" + '&interviewTime=' + interviewTime,
                            success: function (res) {
                                if (index > -1) {
                                    list.splice(index, 1)
                                    wx.showToast({
                                        title: '取消成功',
                                    })
                                }
                                that.setData({
                                    list1: list
                                })
                                if(that.data.list1.length == 0){
                                    that.setData({
                                        a: 1
                                    })
                                }
                            }
                        })
                        that.pageLoading = !1;
                    }else{
                        that.pageLoading = !1;
                    }
                }

            })
        }
    },
    chooseNav:function(e){
        let current = e.currentTarget.dataset.current
        this.setData({
            current: current
        })

        if(this.data.current==1){
            if(!this.data.hasCv){
                wx.showModal({
                    title: '请先填写简历',
                    content: '暂无简历',
                    success: function (res) {
                        if (res.confirm) {
                            wx.navigateTo({
                                url: '../deliver/deliver',
                            })
                        }
                    }
                })
            }
        }
    },
    removea: function (e) {
        var that = this;
        var index = e.currentTarget.dataset.index;
        var list = that.data.list2;
        let reId = list[index].reId
        let interviewTime = list[index].interviewTime
        if (!that.pageLoading) {
            that.pageLoading = !0
            wx.showModal({
                title: '是否拒绝入职',
                content: '提示',
                success: function (res) {
                    if (res.confirm) {
                        wx.request({
                            url: baseUrl + "/refuseEntry?reId=" + reId + "&userid=" + app.globalData.userid + "&isapplication=0" + '&interviewTime=' + interviewTime,
                            success: function (res) {
                                if (res.data.success == '-3') {
                                    wx.showToast({
                                        title: '面试进行中',
                                    })
                                } else {
                                    if (index > -1) {
                                        list.splice(index, 1)
                                        wx.showToast({
                                            title: '拒绝成功',
                                        })
                                    }
                                    that.setData({
                                        list2: list
                                    })
                                    if(that.data.list2.length == 0){
                                        that.setData({
                                            b: 1
                                        })
                                    }
                                }
                            }
                        })
                        that.pageLoading = !1;
                    } else {
                        that.pageLoading = !1;
                    }
                }

            })
        }
    },
    jumpup: function (e) {
        if (!this.pageLoading){
            this.pageLoading = !0
            var id = e.currentTarget.dataset.vid;
            wx.navigateTo({
                url: '../enterpriseInfo/enterpriseInfo?id='+id+'&current=1',
            })
        }
    },
    onShow: function () {
        this.pageLoading = !1;
    },

    payDeposit:function(e){
        let reId = e.currentTarget.dataset.reid
        wx.navigateTo({
            url: '../pay/pay?reId='+reId,
        })
    },

    stopTouchMove: function () {
        return false;
    }
})
