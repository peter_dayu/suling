var QQMapWX = require('../../utils/qqmap-wx-jssdk.min.js');
var login = require('../../utils/login.js');
var qqmapsdk = new QQMapWX({
    key: 'IFCBZ-FF53U-ILBVP-BOAOD-SA4E7-GWFPK' //申请的开发者秘钥key
});
const app = getApp();
var baseUrl = app.globalData.baseUrl;
var fwzs = app.globalData.fwzsUrl;
Page({
    data: {
        city: '',
        vid: '',
        district: '',
        current: 0,
        locationId: '',
        videoList: [],
        fullVideoInfo: {},
        showModalStatus: false,
        commentContents: [],
        isChildShow: false,
        commentContent: '',
        showInput: false,
        isFocus: false,
        commentId: '',
        pageRecommendNo: 1,
        pageAreaNo: 1,
        recommendIndex: 0,
        recommendVideoList: [],
        showPause: false,
        indexCurrent: 0,
        indexCurrentZhaopin: 0,
        firstLoadRecommend: true,
        moreCommentContent: [],
        pageCommentNo: 1,
        hasNext: false,
        showContent: true,
        barHeight: 0,
        menuHeight: 0,
        showFlag: false,
        showCoupon: false,
        up: false,
        down: false,
        autoplay: true,
        auth: true,
        visible: app.globalData.visible,
        timer: '',
        userid0: '',
        recommendId:''

    },

    onLoad: function (options) {
        let that = this
        console.log(options,'options')
        if (app.globalData.userid == '') {
            login.login()
        }
        if (options.current) {
            that.setData({
                current: options.current
            })
        }
        if (options.id) {
            that.setData({
                vid: options.id,
            })
        }
        if (options.userid) {
            that.setData({
                recommendId: options.userid,
            })
        }

        if (options.userid0) {
            that.setData({
                userid0: options.userid0,
            })
        }

        let menuButton = wx.getMenuButtonBoundingClientRect()
        that.setData({
            barHeight: menuButton.top,
            menuHeight: menuButton.height
        })

    },
    onShow: function () {

        var that = this;
        if (app.globalData.userid && ((that.data.current == 0 && that.data.recommendVideoList.length == 0) ||
                that.data.current == 1 && that.data.videoList.length == 0)) {
            that.getLocation();
        }
        if (that.data.fullVideoInfo && that.data.fullVideoInfo.id) { //从别的页面返回更新视频信息
            that.getSingleVideo(that.data.fullVideoInfo.id)
        }
        login.userIdCallback = userid => {
            that.setData({
                visible: app.globalData.visible
            })
            that.getInfo()
            wx.getSetting({
                success(res) {
                    wx.getSetting({
                        success: res => {
                            // 已授权
                            if (res.authSetting['scope.userLocation']) {
                                // console.log('已授权定位')
                                that.getLocation()

                            } else if (res.authSetting['scope.userLocation'] === undefined) {
                                // console.log('初次授权定位')
                                // 尚未授权
                                that.getLocation()
                            } else if (res.authSetting['scope.userLocation'] === false) {
                                app.globalData.auth = false
                                that.getAreaList()

                                // console.log("拒绝授权定位",app.globalData.userid)
                            }
                        }
                    })
                }
            })
            if (!app.globalData.isAuth) {
                that.setData({
                    showFlag: true
                })
            }
        }
    },
    playerror: function (params) {
        wx.showToast({
            title: '视频不见了',
        })
    },
    findShop(to) { //拿到商家的地理位置，用到了腾讯地图的api
        // 实例化API核心类
        var _that = this
        var demo = new QQMapWX({
            key: 'IFCBZ-FF53U-ILBVP-BOAOD-SA4E7-GWFPK' // 必填
        });
        // 调用接口
        demo.calculateDistance({
            to,
            success: function (res) {
                var arr = res.result.elements
                var min = arr[0].distance;
                for (var i = 1; i < arr.length; i++) {
                    var cur = arr[i].distance
                    cur < min ? min = cur : null
                }
                var num;
                for (var j = 0; j < arr.length; j++) {
                    if (arr[j].distance == min) {
                        num = j
                    }
                }
                _that.data.locationName = to[num].locationName
                _that.data.locationList = to[num].locationId
                _that.setData({
                    locationList: to[num].locationId,
                    locationName: to[num].locationName
                })
            },
            fail: function (res) {
                _that.setData({
                    locationList: to[4].locationId,
                    locationName: to[4].locationName
                })
            }
        });
    },
    getAreaList() {
        let that = this;
        if (that.data.current == '1') {
            wx.request({
                url: baseUrl + "/video/queryKnightVideo?userid=" + app.globalData.userid + "&locationId=" + that.data.locationId + "&pageNo=" + that.data.pageAreaNo + "&pageSize=5&id=" + that.data.vid + "&userid0=" + that.data.userid0,
                success: function (res) {
                    if (res.data.pageData.length > 0) {
                        that.setData({
                            videoList: [...that.data.videoList, ...res.data.pageData],
                            fullVideoInfo: res.data.pageData[0],
                            // nextFullVideoInfoZhaopin: videoList.length == 0 ? res.data[1] : res.data[0]
                        })

                    } else {
                        wx.showToast({
                            title: '没有更多数据了',
                            icon: "none"
                        })
                    }
                }
            })
        } else  {
            wx.request({
                url: baseUrl + "/video/queryRec?userid=" + app.globalData.userid + "&pageNo=" + that.data.pageRecommendNo + "&pageSize=5&isagent=&orders=1&id=" + that.data.vid,

                success: function (res) {
                    that.setData({
                        recommendVideoList: [...that.data.recommendVideoList, ...res.data],
                        fullVideoInfo: res.data[0],
                        // nextFullVideoInfo: re.length == 0 ? res.data[1] : res.data[0]
                    })
                }
            })

        }
    },
    queryKnightVideo() {
        var that = this
        wx.request({
            url: baseUrl + "/video/queryKnightVideo?userid=" + app.globalData.userid + "&pageNo=" + that.data.pageAreaNo + "&pageSize=5",
            success: function (res) {
                if (res.data.pageData.length > 0) {
                    if(that.data.videoList.length==0){
                        that.setData({
                            fullVideoInfo:res.data.pageData[0]
                        })
                    }
                    that.setData({
                        videoList: [...that.data.videoList, ...res.data.pageData],
                        // nextFullVideoInfoZhaopin: videoList.length == 0 ? res.data[1] : res.data[0]
                    })
                } else {
                    wx.showToast({
                        title: '没有更多数据了',
                        icon: "none"
                    })
                }
            }
        })
    },
    getLocation() {
        let that = this
        wx.getLocation({
            success: function (res) {
                // 调用sdk接口
                qqmapsdk.reverseGeocoder({
                    location: {
                        latitude: res.latitude,
                        longitude: res.longitude
                    },
                    success: function (res) {
                        //获取当前地址成功
                        var city = res.result.address_component.city;
                        var district = res.result.address_component.district
                        // that.setData({
                        //     city: city,
                        //     district: district,
                        // })
                        wx.request({
                            url: baseUrl + "/area/queryLocationIdByName?city=" + city + "&district=" + district,
                            success: function (res) {
                                that.setData({
                                    locationId: 320506
                                })
                                that.getAreaList();
                            }
                        })
                    },
                    fail: function (res) {
                        console.log('获取当前地址失败');
                    }
                });
            },
            fail(res) {
                that.getAreaList()
                console.log(res, '获取地址失败')
            }
        })
    },
    chooseNav(e) {
        let that = this
        let current = e.currentTarget.dataset.current
        this.setData({
            current: current
        })
        if (current == '0') {

            that.setData({
                fullVideoInfo: that.data.recommendVideoList[that.data.indexCurrent]
            })
            if (that.data.firstLoadRecommend) {
                that.getAreaList()
                that.setData({
                    firstLoadRecommend: false,
                    showPause: false
                })
            }
        }
        if (current == '1') {
            if (that.data.videoList.length > 0) {
                that.setData({
                    fullVideoInfo: that.data.videoList[that.data.indexCurrentZhaopin]
                })
            }
            if (that.data.videoList.length == 0) {
                that.queryKnightVideo()
            }
        }

    },
    // 视频暂停和播放
    videoPlay(e) {
        let that = this
        //获取video
        that.videoContext = wx.createVideoContext('myVideo' + that.data.indexCurrent)
        if (that.data.showPause) {
            //开始播放
            that.videoContext.play() //开始播放
            that.setData({
                showPause: false
            })
        } else {
            //当play==false 显示图片 暂停
            that.videoContext.pause() //暂停播放
            that.setData({
                showPause: true
            })
        }
    },

    getInfo: function () {
        let that = this
        wx.request({
            url: baseUrl + '/info',
            data: {
                "userid": app.globalData.userid
            },
            method: 'GET',
            success: function (res) {
                if (res.data.user.isagent == '0') {
                    that.isgetCoupon()
                } else {
                    that.setData({
                        showCoupon: false
                    })
                }
            }
        })
    },
    // 判断是否领取红包
    isgetCoupon() {
        let that = this
        wx.request({
            url: baseUrl + "/center/hasCoupon99?userid=" + app.globalData.userid,
            success: function (res) {
                if (res.data == '1') {
                    that.setData({
                        showCoupon: true
                    })
                } else {
                    that.setData({
                        showCoupon: false
                    })
                }

            }
        })
    },
    zhaopinPlay0(e) {
        let that = this
        let id = e.currentTarget.dataset.id
        let lastId = wx.getStorageSync('lastId2');
        that.data.timer = setTimeout(
            function () {
                if (lastId != id) {
                    login.getCache(that.zhaopinPlay)
                    wx.setStorageSync('lastId2', id);
                }
            }, 5000)


    },
    zhaopinPlay(adminToken) {

        var indexCurrentZhaopin = this.data.indexCurrentZhaopin
        var id = this.data.videoList[indexCurrentZhaopin].id
        wx.request({
            url: fwzs + "/api/sendViews/" + id,
            header: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + adminToken
            },
            success: function (res) {}
        })
    },
    recommendPlay0(e) {
        let that = this
        let id = e.currentTarget.dataset.id
        let lastId = wx.getStorageSync('lastId');
        that.data.timer = setTimeout(
            function () {
                if (lastId != id) {
                    login.getCache(that.recommendPlay)
                    wx.setStorageSync('lastId', id);
                }
            }, 5000)
    },
    recommendPlay(adminToken) {

        var indexCurrent = this.data.indexCurrent
        var id = this.data.recommendVideoList[indexCurrent].id
        wx.request({
            url: fwzs + "/api/sendViews/" + id,
            header: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + adminToken
            },
            success: function (res) {}
        })
    },
    // 视频暂停和播放
    videoPlayZhaopin(e) {
        let that = this
        /// let id = e.currentTarget.dataset.id;
        //获取video
        that.videoContext = wx.createVideoContext('myVideo' + that.data.indexCurrentZhaopin)
        if (that.data.showPause) {
            //开始播放
            that.videoContext.play() //开始播放
            that.setData({
                showPause: false
            })
        } else {
            //当play==false 显示图片 暂停
            that.videoContext.pause() //暂停播放
            that.setData({
                showPause: true
            })
        }
    },
    showLoginModal() {
        if (app.globalData.isAuth == 0 || !app.globalData.auth) {
            wx.showModal({
                cancelColor: 'cancelColor',
                title: '提示',
                content: '请先登录',
                success: (res) => {
                    if (res.confirm) {
                        console.log('用户点击确定')
                        this.setData({
                            showFlag: true
                        })
                    } else if (res.cancel) {
                        console.log('用户点击取消')
                    }

                }
            })
            return false
        } else {
            return true
        }
    },
    addVideo() {
        var that = this
        if (!that.showLoginModal()) {
            return
        }
        wx.getSetting({
            success: res => {
                // 已授权
                if (res.authSetting['scope.userLocation'] === false) {
                    wx.showToast({
                        title: '请先打开地理位置',
                        duration: 1000
                    })

                    setTimeout(() => {
                        return false
                    }, 1000);

                } else {

                        wx.navigateTo({
                            url: '../workShare/workShare'
                        })
                }
            }
        })


    },
    search() {
        wx.navigateTo({
            url: '../search/search'
        })
    },
    goVideoInfo(e) {
        let id = e.currentTarget.dataset.id;
        wx.navigateTo({
            url: '../videoInfo/videoInfo?id=' + id
        })
    },
    closeModal() {
        let that = this
        that.setData({
            showModalStatus: false,
            showContent: true
        })
    },
    toPersonalInfo: function (e) {
        wx.navigateTo({
            url: '../personalInfo/personalInfo?userid=' + e.currentTarget.dataset.userid + '&type=' + e.currentTarget.dataset.type
        })

    },
    //关注推荐视频
    toFollow(e) {
        let that = this
        if (!that.showLoginModal()) {
            return
        }
        //type 0 userid为sys_user 用户id type 1 userid为用户企业用户dept_id
        let userid = e.currentTarget.dataset.userid
        let isfollow = e.currentTarget.dataset.isfollow
        let id = e.currentTarget.dataset.id
        let type = e.currentTarget.dataset.type
        if (app.globalData.userid == userid) {
            wx.showToast({
                title: '不能关注自己',
                icon: 'none',
                duration: 2000
            })
            return false
        }
        let current = that.data.current
        if (current == 0) {
            let indexCurrent = that.data.indexCurrent
            var a = that.data.recommendVideoList
            a[indexCurrent].isFollow = a[indexCurrent].isFollow == 0 ? 1 : 0
            that.setData({
                recommendVideoList: a,
                fullVideoInfo: a[indexCurrent]
            })
        } else if (current == 1) {
            let indexCurrentZhaopin = that.data.indexCurrentZhaopin
            var a = that.data.videoList
            a[indexCurrentZhaopin].isFollow = a[indexCurrentZhaopin].isFollow == 0 ? 1 : 0
            that.setData({
                videoList: a,
                fullVideoInfo: a[indexCurrentZhaopin]
            })
        }
        if (isfollow == '0') {
            // type 0:用户视频，请求关注用户接口
            if (type == '0' || that.type == '4' || type == "3") {
                wx.request({
                    url: baseUrl + "/video/follow?userid=" + app.globalData.userid + "&followId=" + userid,
                    success: function (res) {
                        wx.showToast({
                            title: '关注成功',
                            icon: 'none',
                            duration: 2000
                        })
                    }
                })
            } else if (type == "1") {
                //type 1：企业
                wx.request({
                    url: baseUrl + '/video/followHr?userid=' + app.globalData.userid + "&deptId=" + userid,
                    success: function (res) {
                        wx.showToast({
                            title: '关注成功',
                            icon: 'none',
                            duration: 2000
                        })
                        that.setData({
                            'fullVideoInfo.isFollow': 1
                        })
                    }
                })
            } else {
                wx.request({
                    url: baseUrl + "/video/unFollow?userid=" + app.globalData.userid + "&followId=" + userid,
                    success: function (res) {
                        wx.showToast({
                            title: '取消关注成功',
                            icon: 'none',
                            duration: 2000
                        })
                    }
                })
            }
        }
    },
    thumbs_up(e) {
        let id = e.currentTarget.dataset.id
        let userid0 = e.currentTarget.dataset.userid
        let that = this
        if (!that.showLoginModal()) {
            return
        }
        var indexCurrent = that.data.indexCurrent
        wx.request({
            url: baseUrl + "/video/star",
            data: {
                vid: id,
                userid: app.globalData.userid,
                userid0: userid0,
                isStar: !that.data.recommendVideoList[indexCurrent].stars,
            },
            method: "Post",
            success: function (res) {
                if (res.data == 1) {
                    var a = that.data.recommendVideoList
                    a[indexCurrent].stars = !that.data.recommendVideoList[indexCurrent].stars
                    a[indexCurrent].starNum = that.data.recommendVideoList[indexCurrent].stars ? a[indexCurrent].starNum + 1 : a[indexCurrent].starNum - 1
                    that.setData({
                        ["recommendVideoList["+that.data.indexCurrent+"]"]: a[indexCurrent]
                    })
                }
            }
        })
    },
    thumbs_upZhaopin(e) {
        let id = e.currentTarget.dataset.id
        let userid0 = e.currentTarget.dataset.userid
        let that = this
        if (!that.showLoginModal()) {
            return
        }
        var indexCurrentZhaopin = that.data.indexCurrentZhaopin
        wx.request({
            url: baseUrl + "/video/star",
            data: {
                vid: id,
                userid: app.globalData.userid,
                userid0: userid0,
                isStar: !that.data.videoList[indexCurrentZhaopin].stars,
            },
            method: "Post",
            success: function (res) {
                if (res.data == 1) {

                    var a = that.data.videoList
                    a[indexCurrentZhaopin].stars = !that.data.videoList[indexCurrentZhaopin].stars
                    a[indexCurrentZhaopin].starNum = that.data.videoList[indexCurrentZhaopin].stars ? a[indexCurrentZhaopin].starNum + 1 : a[indexCurrentZhaopin].starNum - 1
                    that.setData({
                        ["videoList["+that.data.indexCurrentZhaopin+"]"]: a[indexCurrentZhaopin]
                    })
                }
            }
        })
    },
    //获取单个视频信息
    getSingleVideo(id) {
        let that = this
        wx.request({
            url: baseUrl + "/video/queryVideoById?id=" + id + "&userid=" + app.globalData.userid,
            success: function (res) {
                that.setData({
                    fullVideoInfo: res.data
                })
                if (that.data.current == 0 && that.data.recommendVideoList == 0) {
                    that.data.recommendVideoList[0] = that.data.fullVideoInfo
                } else if (that.data.current == 1 && that.data.videoList == 0) {
                    that.data.videoList[0] = that.data.fullVideoInfo
                }
            }
        })
    },
    checkAuth: function (e) {
        app.globalData.isAuth = e.detail.checkAuth
        app.globalData.userInfo = e.detail.userInfo
    },
    popComments(e) {
        let id = e.currentTarget.dataset.id
        let that = this
        that.setData({
            showContent: false,
            showModalStatus: true,
            // isComentShowd: true,
            vid: id
        })
        //if(that.data.commentContents.length==0){
        wx.request({
            url: baseUrl + "/video/queryCommentByVid?vid=" + (id ? id : that.data.vid) + "&userid=" + app.globalData.userid + "&pageNo=" + that.data.pageCommentNo + "&pageSize=5",
            success: function (res) {
                that.setData({
                    commentContents: res.data.pageData,
                    pageCommentNo: that.data.pageCommentNo + 1,
                    hasNext: res.data.hasNext
                    // isChildShow:res.data.isChildShow
                })
            }
        })
        //}

    },

    pop2endPageComments(e) {
        let that = this
        if (!that.data.hasNext) {
            return
        }
        wx.request({
            url: baseUrl + "/video/queryCommentByVid?vid=" + that.data.vid + "&userid=" + app.globalData.userid + "&pageNo=" + that.data.pageCommentNo + "&pageSize=5",
            success: function (res) {
                that.setData({
                    commentContents: that.data.commentContents.concat(res.data.pageData),
                    pageCommentNo: that.data.pageCommentNo + 1,
                    hasNext: res.data.hasNext
                    // isChildShow:res.data.isChildShow
                })
            }
        })
    },
    goCommentsLike(e) {
        let that = this
        let id = e.currentTarget.dataset.id
        let vid = e.currentTarget.dataset.vid
        let stars = e.currentTarget.dataset.stars
        wx.request({
            url: baseUrl + "/video/star",
            data: {
                commentId: id,
                vid: vid,
                userid: app.globalData.userid,
                isStar: !stars,
            },
            method: "Post",
            success: function (res) {
                wx.request({
                    url: baseUrl + "/video/queryCommentByVid?vid=" + vid + "&userid=" + app.globalData.userid,
                    success: function (res) {
                        that.setData({
                            commentContents: res.data.pageData
                        })
                    }
                })
            }
        })
    },
    getComments(e) {
        this.setData({
            commentContent: e.detail.value
        })
    },
    getMoreComments(e) {
        let that = this
        let id = e.currentTarget.dataset.id
        let vid = e.currentTarget.dataset.vid
        let idx = e.currentTarget.dataset.idx
        var num = Number(idx)
        var pageNo = that.data.commentContents[num].pageNo == null ? 1 : that.data.commentContents[num].pageNo + 1
        // that.data.pageCommentNo++
        let childHasNextPage1 = false
        wx.request({
            url: baseUrl + "/video/queryCommentByCommentId?vid=" + vid + "&commentId=" + id + "&userid=" + app.globalData.userid + "&pageNo=" + pageNo + "&pageSize=5",
            success: function (res) {
                if (that.data.commentContents[num].commentNum > pageNo * 5) {
                    childHasNextPage1 = true;
                }
                var c = that.data.commentContents[num].commentSet.concat(res.data);
                var b = that.data.commentContents[num].commentSet = c
                that.data.commentContents[num].isChildShow = true
                that.data.commentContents[num].childHasNextPage = childHasNextPage1
                that.data.commentContents[num].pageNo = pageNo
                var b = that.data.commentContents
                that.setData({
                    commentContents: b
                })
            }
        })
    },
    toComments(e) {
        let that = this
        if (!that.showLoginModal()) {
            return
        }
        let vid = e.currentTarget.dataset.vid
        let userid0 = e.currentTarget.dataset.userid
        if (that.data.commentContent == "") {
            wx.showModal({
                title: "提示",
                content: "请输入评论内容哦～"
            })
            return false
        }
        wx.request({
            url: baseUrl + "/video/addComment",
            data: {
                commentId: that.data.commentId,
                vid: vid,
                userid0,
                userid: app.globalData.userid,
                content: that.data.commentContent,
            },
            method: "Post",
            success: function (res) {
                if (res.data == '1') {
                    wx.showToast({
                        title: '评论成功',
                        icon: 'success',
                        duration: 2000
                    })
                    if (that.data.current == 0) {
                        var a = that.data.recommendVideoList
                        a[that.data.indexCurrent].comments = a[that.data.indexCurrent].comments + 1
                        that.setData({
                            recommendVideoList: a,
                            showModalStatus: false,
                            commentId: '',
                            showContent: true
                        })
                    }
                    if (that.data.current == 1) {
                        var a = that.data.videoList
                        a[that.data.indexCurrentZhaopin].comments = a[that.data.indexCurrentZhaopin].comments + 1
                        that.setData({
                            videoList: a,
                            showModalStatus: false,
                            commentId: '',
                            showContent: true
                        })
                    }
                    that.getSingleVideo(vid)
                }
            }
        })
    },
    onSlideChange(e) {
        let that = this
        let currentInfo = that.data.recommendVideoList[e.detail.current]
        var f = that.data.indexCurrent < e.detail.current ? true : false //判断上滑下滑
        that.setData({
            commentContents: [],
            pageCommentNo: 1,
            indexCurrent: e.detail.current,
            showPause: false,
            fullVideoInfo: currentInfo,
            // nextFullVideoInfo: that.data.recommendVideoList.length > e.detail.current + 1 ? that.data.recommendVideoList[e.detail.current + 1] : null,
            // preFullVideoInfo: e.detail.current > 0 ? that.data.recommendVideoList[e.detail.current - 1] : null
        })
        clearTimeout(that.data.timer);
        if (f) {
            if (that.data.recommendVideoList.length - 1 == that.data.indexCurrent) {
                that.data.pageRecommendNo++
                that.getAreaList()
            }
        }
    },
    onSlideChangeZhaopin(e) {
        let that = this
        let currentInfo = that.data.videoList[e.detail.current]
        var f = that.data.indexCurrentZhaopin < e.detail.current ? true : false //判断上滑下滑
        that.setData({
            commentContents: [],
            pageCommentNo: 1,
            indexCurrentZhaopin: e.detail.current,
            showPause: false,
            fullVideoInfo: currentInfo,
            // nextFullVideoInfoZhaopin: that.data.videoList.length > e.detail.current + 1 ? that.data.videoList[e.detail.current + 1] : null,
            // preFullVideoInfoZhaopin: e.detail.current > 0 ? that.data.videoList[e.detail.current - 1] : null
        })
        clearTimeout(that.data.timer);
        if (f) {
            for (let i = 1; i < 2000; i++) {
                if (e.detail.current == (5 * i - 1)) {
                    that.data.pageAreaNo++
                    that.queryKnightVideo()
                }
            }
        }
    },
    sliding: function (e) {
        let that = this
        let dy = e.detail.dy
        if (dy > 0) {
            that.setData({
                up: true
            })
        } else {
            that.setData({
                down: true
            })
        }
    },
    endChange: function () {
        this.setData({
            up: false,
            down: false
        })
    },
    // 展示评论回复的评论框
    showInput(e) {
        let id = e.currentTarget.dataset.id
        this.setData({
            isFocus: true,
            commentId: id
        })
    },
    //评论下拉刷新
    commentsDownRefresh: function () {
        this.pop2endPageComments()
    },
    //进入推荐企业
    toAgentRecruits: function (e) {
        let that = this
        let userid =  e.currentTarget.dataset.userid
        let id = e.currentTarget.dataset.id
        // wx.navigateTo({
        //     url: "../agentRecruits/agentRecruits?userid=" + userid
        // })
        if(that.data.current == '0'){
            if(that.data.recommendId){
                wx.navigateTo({
                    url: '../enterpriseInfo/enterpriseInfo?id=' + id + '&userid=' + that.data.recommendId
                })
            }else {
                wx.navigateTo({
                    url: '../enterpriseInfo/enterpriseInfo?id=' + id
                })
            }
        }else{
            wx.navigateTo({
                url: '../enterpriseInfo/enterpriseInfo?id=' + id + '&userid=' + userid
            })
        }

    },
    //点击分享按钮后发送给好友
    onShareAppMessage: function (res) {
        let that = this
        let userid = that.data.current == '0'?app.globalData.userid:that.data.fullVideoInfo.userid
        console.log(res,"fenx")
        if (res.from === 'button') {}
        return {
            title: '给你分享一个视频',
            path: 'pages/index/index?id=' + that.data.fullVideoInfo.id + '&current=' + that.data.current + '&userid=' + userid + "&userid0=" + that.data.fullVideoInfo.userid,
            success: function (res) {}
        }
    },
    /**
     * 企业详情分享朋友圈
     * @param {*} res
     */
    onShareTimeline: function (res) {
        let that = this
        let id = that.data.fullVideoInfo.id
        let current = that.data.current
        let title = that.data.fullVideoInfo.title
        let imgCover = that.data.fullVideoInfo.imgCover
        return {
            title: title,
            imageUrl: imgCover,
            query: 'id=' + id + '&current=' + current + "&userid0=" + that.data.fullVideoInfo.userid
        }
    },
    /**
     * 页面上拉触底事件的处理函数
     */

    onReachBottom() {

    },
    onReady: function (e) {

    },



})
