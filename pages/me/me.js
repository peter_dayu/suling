// pages/mine/mine.js
let app = getApp()
var baseUrl = app.globalData.baseUrl;
var fwzsUrl = app.globalData.fwzsUrl;
var userid = app.globalData.userid;
var login = require('../../utils/login.js');
Page({
    /**
     * 页面的初始数据
     */
    data: {
        personalInfo: {},
        current: 0,
        videoPage: 1,
        followPage: 1,
        starPage: 1,
        myFollowVideo: [],
        myStarVideo: [],
        myVideo: [],
        videoTotalCount: 0,
        followTotalCount: 0,
        starTotalCount: 0,
        avatarUrl: '',
        nickName: '',
        showTips:true,
        isknight:false,
        knightType:'',
        visible:app.globalData.visible,
        isunread:false
    },
    onLoad: function (options) {


        let that = this
        if(!app.globalData.userid){
            login.login()
        }
        that.setData({
            avatarUrl: app.globalData.userInfo.avatarUrl,
            nickName: app.globalData.userInfo.nickName,
            myVideo: [],
            videoPage: 1,
            visible:app.globalData.visible
        })
        setTimeout(function () {
            that.setData({
                showTips:false
            })
        },5000)

    },
    getInfo: function () {
        let that = this
        wx.request({
            url: baseUrl + '/info',
            data: {
                "userid": app.globalData.userid
            },
            method: 'GET',
            success: function (res) {
                that.setData({
                    personalInfo: res.data
                })
            }
        })
    },
    onShow: function () {
        var that =this
        that.setData({
            videoPage:1
        })
        that.getInfo()
        that.getMyVideo()
        that.queryBargain()
        that.idUnread()
    },

    viewResume: function (e) {

        wx.navigateTo({

            url: '../viewResume/viewResume',

        })
    },
    viewAgentEnterpiise:function(){
            wx.navigateTo({
                url: '../agentEnterprise/agentEnterprise',
            })

    },
    editAgentEnterprise:function(){

            wx.navigateTo({
                url: '../editAgentEnterprise/editAgentEnterprise',
            })

    },
    toMyTeam:function(){

            wx.navigateTo({
                url: '../myTeam/myTeam',
            })

    },
    goActivities(){
        wx.navigateTo({
            url: '../platformActivities/platformActivities',
        })
    },
    toMyMessage: function () {
        wx.navigateTo({
            url: '../myMessage/myMessage',
        })
    },
    goMyGroup: function () {
        wx.navigateTo({
            url: '../myGroup/myGroup',
        })
    },
    getMyVideo: function () {
        let that = this
        wx.request({
            url: baseUrl + '/video/myVideo?userid0=' + app.globalData.userid + '&pageNo=' + that.data.videoPage + '&pageSize=6',
            success: function (res) {
                that.setData({
                    myVideo:( that.data.videoPage==1? res.data.pageData:[...that.data.myVideo, ...res.data.pageData]),
                    videoTotalCount: res.data.totalCount
                })
            }
        })
    },
    getMyFollowVideo: function () {
        let that = this
        wx.request({
            url: baseUrl + '/video/myFollowVideo?userid0=' + app.globalData.userid + '&pageNo=' + that.data.followPage + '&pageSize=6',
            success: function (res) {
                that.setData({
                    myFollowVideo: [...that.data.myFollowVideo, ...res.data.pageData],
                    followTotalCount: res.data.totalCount
                })
            }
        })
    },
    getMyStarVideo: function () {
        let that = this
        wx.request({
            url: baseUrl + '/video/myStarVideo?userid0=' + app.globalData.userid + '&pageNo=' + that.data.starPage + '&pageSize=6',
            success: function (res) {
                that.setData({
                    myStarVideo: [...that.data.myStarVideo, ...res.data.pageData],
                    // myStarVideo:res.data.pageData,
                    starTotalCount: res.data.totalCount
                })
            }
        })
    },

    toFollow: function () {
        wx.navigateTo({
            url: '../follow/follow',
        })
    },
    queryFans: function () {
        wx.navigateTo({
            url: '../myFans/myFans',
        })
    },

    enlist:function(){
        wx.navigateTo({
          url: '../knightRule/knightRule'
        })
    },
    goMyAssets: function () {
        wx.navigateTo({
            url: '../myAssets/myAssets',
        })
    },
    goVideoInfo: function (e) {
        let id = e.currentTarget.dataset.id
        let type =e.currentTarget.dataset.type
        wx.navigateTo({
            url: '../videoInfo/videoInfo?fromPath=me&id=' + id+"&type="+type,
        })
    },
    goCut:function(){
        let that = this
        if(this.data.isknight){
            wx.navigateTo({
                url: '../invite/invite?current='+ that.data.knightType,
            })
        }else{
            wx.navigateTo({
                url: '../cutChoose/cutChoose',
            })
        }
    },
    goEnvelope:function(){
        wx.navigateTo({
            url: '../redEnvelope/redEnvelope',
        })
    },
    contact:function(){
        wx.navigateTo({
          url: '../contact/contact'
        })
    },
    chooseNav: function (e) {
        let that = this
        let current = e.currentTarget.dataset.current
        this.setData({
            current: current
        })
        if (current == '1') {
            that.setData({
                myStarVideo: [],
                starPage: 1,
            })
            that.getMyStarVideo()
        }
        else {
            that.setData({
                myVideo: [],
                videoPage: 1,
            })
            that.getMyVideo()
        }

        wx.pageScrollTo({
            scrollTop: 0
        })
    },
    //查询是否砍价
    queryBargain:function(){
        login.getCache(this.queryBargainList)
    },
    queryBargainList:function(adminToken){
        let that = this
        wx.request({
            url:fwzsUrl+"/api/bargainList",
            header: {
                'content-type': 'application/json', // 默认值
                'Authorization':'Bearer '+adminToken
            },
            method:'Post',
            data:{
                userid:app.globalData.userid,
            },
            success:function (res) {
                if(res.data.list.length == 0){
                    that.setData({
                        isknight:false
                    })
                }else{
                    if(res.data.list[0].type == 'knight1'){
                        that.setData({
                            knightType:0
                        })
                    }else if(res.data.list[0].type == 'knight2'){
                        that.setData({
                            knightType:1
                        })
                    }else{
                        that.setData({
                            knightType:2
                        })
                    }
                    that.setData({
                        isknight:true,
                    })
                }
            }
        })
    },
    //是否有未读消息
    idUnread:function(){
        let that = this
        wx.request({
            url:baseUrl+"/unReadMessage?userid="+app.globalData.userid,
            success:function (res) {
                if(res.data == '1'){
                    that.setData({
                        isunread:true
                    })
                }else{
                    that.setData({
                        isunread:false
                    })
                }
            }
        })
    },
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },
    /**
     * 生命周期函数--监听页面显示
     */

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },
    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },
    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },
    tobeKnight:function () {
        wx.navigateTo({
            url: '../knightRule/knightRule'
        })
    },
    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {
        let that = this
        if (that.data.current == '0') {
            if (that.data.videoPage * 6 < that.data.videoTotalCount) {
                that.data.videoPage++
                that.getMyVideo()
            }
        } else if (that.data.current == '2') {
            if (that.data.followPage * 6 < that.data.followTotalCount) {
                that.data.followPage++
                that.getMyFollowVideo()
            }
        } else if (that.data.current == '1') {
            if (that.data.starPage * 6 < that.data.starTotalCount) {
                that.data.starPage++
                that.getMyStarVideo()
            }
        }
    },
    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})
