// pages/redEnvelope/redEnvelope.js
const app = getApp();
var fwzsUrl = app.globalData.fwzsUrl;
var baseUrl = app.globalData.baseUrl;
var login = require('../../utils/login.js');
Page({

    /**
     * 页面的初始数据
     */
    data: {
        showReceived:false,
        recruitList:[],
        barHeight:0
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        let menuButton=wx.getMenuButtonBoundingClientRect()
        this.setData({
            barHeight:menuButton.top
        })
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {
        this.getRecruitList()
    },
    open:function(){
        login.getCache(this.openRedEnvelope)
    },
    openRedEnvelope:function(adminToken){
        let that = this
        var current=that.data.current
        wx.request({
            url: fwzsUrl + "/api/getFirstDeal10?userid="+app.globalData.userid,
            header: {
                'content-type': 'application/json', // 默认值
                'Authorization':'Bearer '+adminToken
            },
            success: function (res) {
                if(res.data.code == '1'){
                    that.setData({
                        showReceived:true
                    })
                }else{
                    wx.showToast({
                      title: res.data.message
                    })
                }
            }
        })
    },
    back:function(){
        wx.navigateBack()
    },
    close:function(){
        let that = this
        that.setData({
            showReceived:false
        })
    },
    getRecruitList:function(){
        let that = this
        wx.request({
            url: baseUrl + '/video/queryAll?type=1&pageNo=1&isFullTime=1&pageSize=4&userid='+app.globalData.userid,
            method: 'GET',
            success: function (res) {
                if(res.data){
                    that.setData({
                        recruitList:res.data
                    })
                }


            }
        })
    },
    goEnterpriseInfo(e){
        wx.navigateTo({
            url: '../enterpriseInfo/enterpriseInfo?id='+e.currentTarget.dataset.id
        })
    },
    //使用10元红包
    use:function(){
        let that = this
        that.setData({
            showReceived:false
        })
    },
    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})
