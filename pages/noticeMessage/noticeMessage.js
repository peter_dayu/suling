// pages/noticeMessage/noticeMessage.js
const app = getApp();
var baseUrl = app.globalData.baseUrl;
Page({

  /**
   * 页面的初始数据
   */
  data: {
      sysMessage:[]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
      this.getNoticeMessage()
      if(options.unread&&options.unread>0){
        this.clearUnreadSysCache()
      }
  },
  getNoticeMessage:function(){
    let that = this
      wx.request({
          url: baseUrl + "/sysMessage?userid=" + app.globalData.userid,
          success: function (res) {
              that.setData({
                  sysMessage:res.data.sysMessage
              })
          }
      })
  },
  clearUnreadSysCache:function(){
    wx.request({
      url: baseUrl+'/cache/clearUnreadSysCache?userid='+app.globalData.userid,
      success:function(res){

      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})
