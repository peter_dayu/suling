// pages/withdraw/withdraw.js
let app = getApp()
var baseUrl = app.globalData.baseUrl;
var login = require('../../utils/login.js');
Page({

    /**
     * 页面的初始数据
     */
    data: {
        amount:0,
        accountInfo:{}
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {

    },
    getAssets:function(){
        login.getCache(this.getAssets1)
      },
      getAssets1:function(adminToken){
          let that = this
          wx.request({
              url: app.globalData.fwzsUrl + '/api/account',
              data: { "userid": app.globalData.userid },
              method: 'GET',
              header:{
                  "Content-Type":"application/json",
                  "Authorization":"Bearer "+adminToken
              },
              success: function (res) {
                  that.setData({
                      accountInfo:res.data
                  })
              }
          })
      },
    withdraw:function(){
     login.getCache(this.withdraw1)
    },
    withdraw1:function(adminToken){
      let that = this
      if(that.data.amount <= 0){
          wx.showToast({
              title: '请输入正确金额',
              icon: 'none',
              duration: 2000
          })
          return false
      }
      if(that.data.amount < 0.3||that.data.amount>5000){
        wx.showToast({
            title: '金额必须在0.3元到5000元之间',
            icon: 'none',
            duration: 2000
        })
        return false
      }
     if(that.data.accountInfo.realBalance<that.data.amount){
        wx.showToast({
            title: '账户余额不足',
            icon: 'none',
            duration: 2000
        })
        return false
     }
      wx.request({
            //body, outTradeNo, totalFee, spbillCreateIp, notifyUrl, tradeType
            url: app.globalData.fwzsUrl + '/api/cashingOut?userid='+app.globalData.userid,
            data: {
              //  mchBillNo: (new Date()).valueOf(),  // (new Date()).valueOf() 随机一个时间戳
                description: "佣金",
                partnerTradeNo: (new Date()).valueOf(),
                checkName:'NO_CHECK',
                sendName: "和谐马",
                amount: that.data.amount * 100, //体现金额 单位 分
                spbillCreateIp: "192.168.0.123",
                notifyWay: "MINI_PROGRAM_JSAPI"
            },
            header: {
                'content-type': 'application/json', // 默认值
                "Authorization":"Bearer "+adminToken
            },
            method: "POST",
            success: function (res) {
                wx.showToast({
                    title: res.data.message,
                    icon:'none',
                    duration: 2000
                })
                setTimeout(() => {
                   wx.navigateBack()
                }, 2000)
            },
        });
    },
    getCount:function(e){
        let num = e.detail.value
        this.setData({
            amount:num
        })

    },
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {
        this.getAssets()
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})
