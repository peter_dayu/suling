// page/component/details/details.js
import tip from '../../utils/tip'
var app = getApp();
var baseUrl = app.globalData.baseUrl;
Page({
  data: {
    userid: "",
    openid: "",
    id: '',
    goods: {
    },
    name: '',
    phone: '',
    address: '',
    numa: '',
    num: 1,
    indicatorDots: true, //是否显示面板指示点
    autoplay: true, //是否自动切换
    interval: 3000, //自动切换时间间隔,3s
    duration: 1000, //  滑动动画时长1s
    minusStatus: 'disabled',
    siteList:[],
    index:0,
  },
  onLoad: function (options) {
    var that = this;
    this.setData({
      id: options.id,
      numa: options.num
    })
    wx.getSystemInfo({
      success: function (res) {
        that.setData({
          winWidth: res.windowWidth,
          winHeight: res.windowHeight
        });
      }

    });
    let postId = this.data.postId;
    try {
      var value = wx.getStorageSync('third_Session');
      if (value) {
        that.setData({
          openid: value.openid
        })
      }
    } catch (e) {
    }; wx.request({
      url: baseUrl + '/queryuser?openid=' + that.data.openid,
      success: res => {
        that.setData({
          userid: res.data.userid
        })
      }
    })
    var id = that.data.id
    wx.request({
      url: baseUrl + '/center/getCommodityDetail?commodityId=' + id,//请求地址
      method: 'GET',
      success: function (res) {
        var goods = that.data.goods
        goods = res.data
        that.setData({
          goods: goods
        })
      },
    })
    //that.getSiteDetail();
  },
  // 获取站点信息
  getSiteDetail:function(){
    let that = this;
    wx.request({
      url:baseUrl + "/site/listsite",
      method: 'GET',
      success(res) {
        var siteList = res.data.siteList;
        that.setData({
          siteList:siteList
        })
      }
    })
  },
  bindPickerChange(e){
    this.setData({
      index:e.detail.value
    })
  },
  bindMinus: function () {
    var num = this.data.num;
    // 如果大于1时，才可以减
    if (num > 1) {
      num--;
    }
    // 只有大于一件的时候，才能normal状态，否则disable状态
    var minusStatus = num <= 1 ? 'disabled' : 'normal';
    // 将数值与状态写回
    this.setData({
      num: num,
      minusStatus: minusStatus
    });
  },
  /* 点击加号 */
  bindPlus: function () {
    var num = this.data.num;
    // 不作过多考虑自增1
    num++;
    // 只有大于一件的时候，才能normal状态，否则disable状态
    var minusStatus = num < 1 ? 'disabled' : 'normal';
    // 将数值与状态写回
    this.setData({
      num: num,
      minusStatus: minusStatus
    });
  },
  /* 输入框事件 */
  bindManual: function (e) {
    var num = e.detail.value;
    // 将数值与状态写回
    this.setData({
      num: num
    });
  },

  addToCart() {
    var that = this;
    var price = that.data.goods.price;
    var num = that.data.num;
    var numa = that.data.numa;
    if (numa < price * num) {
      wx.showModal({
        title: '提示',
        content: '您的积分不够',
        showCancel: false
      })
    } else if(that.data.siteList.length < 0){
      wx.showModal({
        title: '提示',
        content: '暂无兑换点',
        showCancel: false
      })
    }else{
      tip.confirm("确认兑换地点是"+ that.data.siteList[that.data.index].siteName+'?').then(function(){
          that.onConfirm();
      }).catch(function(){
          // 取消
      });
    }
  },

  onConfirm: function () {
    var that = this;
      wx.request({
        url: baseUrl + "/center/getPresent",
        method: 'POST',
        data: {
          userId: that.data.userid,
          commodityId: that.data.id,
          siteId: that.data.siteList[that.data.index].siteId,
          num: that.data.num,
        },
        success: function (res) {
          wx.showToast({
            title: '兑换成功',
            icon: 'success',
            duration: 2000
          })
        }
      })
  },
})
