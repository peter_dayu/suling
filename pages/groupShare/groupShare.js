// pages/groupShare/groupShare.js
const app = getApp();
var baseUrl = app.globalData.baseUrl;
var promise = require('../../utils/promiseMethod');
Page({

    /**
     * 页面的初始数据
     */
    data: {
        reId:'',
        group:[],
        groupInfo:[],
        userNum:0,
        report:0,
        groupid:'',
        creatorid:'',
        nowTime:'',
        endTime:'',
        timer:'',
        num:'',
        vid:'',
        finish:0
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {

      this.setData({
          reId:options.reId
      })
        this.getRecruitGroupInfo()
    },
     getRecruitGroupInfo(){
        let that = this
        wx.request({
            url: baseUrl + "/recruitGroupInfo?reid="+that.data.reId+"&userid="+app.globalData.userid,
            success: function (res) {
                var creatorid=''
                var finish=''
                res.data.GroupInfo.forEach(function(element) {
                    if(element.grouped=='1'){
                        creatorid=element.creatorId
                        finish=element.finish
                    }
                  });
                that.setData({
                    groupInfo:res.data.GroupInfo,
                    userNum:res.data.AllUserNum,
                    report:res.data.report,
                    groupid:res.data.groupid,
                    vid:res.data.vid,
                    creatorid,
                    finish
                })
            }

        })

    },
    handleTime(){
        let that = this
        for(let i = 0;i<that.data.groupInfo.length;i++){
            if(that.data.groupInfo[i].createTime != '-1'){
                let time = that.showEndTime(that.data.groupInfo[i].createTime)
                that.data.groupInfo[i].endTime = time
            }
        }
        that.setData({
            groupInfo:that.data.groupInfo
        })
    },
    showEndTime(t){
        let nowTime = new Date().getTime();
        let endTime = t - nowTime;
        let days = parseInt(endTime / (1000 * 60 * 60 * 24));
        let hours = parseInt((endTime % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)) < 10? "0"+parseInt((endTime % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)):parseInt((endTime % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        let minutes = parseInt((endTime % (1000 * 60 * 60)) / (1000 * 60)) <10 ? "0"+parseInt((endTime % (1000 * 60 * 60)) / (1000 * 60)):parseInt((endTime % (1000 * 60 * 60)) / (1000 * 60));
        let seconds = parseInt((endTime % (1000 * 60)) / 1000) <10 ? "0"+parseInt((endTime % (1000 * 60)) / 1000):parseInt((endTime % (1000 * 60)) / 1000);
        endTime=days+'天'+hours+':'+minutes+':'+seconds
        return endTime
    },
    joinGroup(e){
        let that = this
        if(that.data.report=='1'){
            wx.showModal({
                title: '提示',
                content: '已经报名，不能加入组团',
                success (res) {
                }
            })
            return
        }
        let id = e.currentTarget.dataset.id;
        let reid = e.currentTarget.dataset.reid;
        let creatorid = e.currentTarget.dataset.creatorid;
        wx.request({
            url: baseUrl + '/groupInvite',
            data: {
                id: id,
                reid: reid,
                creatorId: creatorid,
                userid: app.globalData.userid,
            },
            method: "Post",
            success: function (res) {
                if(res.data.code!='1'){
                    wx.showModal({
                        title: '提示',
                        content: res.data.message,
                        success (res) {
                            if (res.confirm) {
                                console.log('用户点击确定')
                            } else if (res.cancel) {
                                console.log('用户点击取消')
                            }
                        }
                    })
                }
                if(res.data.code == '1'){
                    wx.showToast({
                        title: '加入组团成功',
                        icon: 'success',
                        duration: 2000
                    })
                    that.getRecruitGroupInfo()
                }
            }
        })
    },
    toGroup(e){
        let that = this
        if(that.data.report=='1'){
            wx.showModal({
                title: '提示',
                content: '已经报名，不能加入组团',
                success (res) {
                }
            })
            return
        }
        let reid = e.currentTarget.dataset.reid;
        wx.request({
            url: baseUrl + "/group",
            method: "Post",
            data: {
                reid: that.data.reId,
                creatorId: app.globalData.userid,
            },
            success: function (res) {
                if(res.data.groupid){
                    wx.showToast({
                        title: '组团成功',
                        icon: 'success',
                        duration: 2000
                    })
                    that.getRecruitGroupInfo()
                }else{
                    wx.showModal({
                        title: '提示',
                        content: res.data.message,
                        success (res) {
                        }
                    })
                }
            }
        })

    },
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },
    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {
          let that = this
        that.getResume()
        that.getRecruitGroupInfo()
        that.data.timer = setInterval(function () {
            that.handleTime()
        },1000)

    },
      // 判断是否已授权简历
     getResume(){
        let that = this
        wx.request({
            url: baseUrl + '/getcvbyid?userid=' + app.globalData.userid,
            success: function (e) {
                var num = that.data.num
                if (e.data.cv == null) {
                    num = 0
                } else {
                    num = 1
                }
                that.setData({
                    num: num
                })
            }
        });
    },
    goEnroll(){
        let that = this
        if(that.data.report == '0'){
            if (that.data.num == 0) {
                wx.showModal({
                    title: '请先填写简历',
                    content: '暂无简历',
                    success: function (res) {
                        if (res.confirm) {
                            wx.navigateTo({
                                url: '../deliver/deliver',
                            })
                        }
                    }
                })
            } else {
                var recommendType = '5'
                wx.request({
                    url: baseUrl + "/sendingApplications?reId=" + that.data.reId + "&userid=" + app.globalData.userid +'&recommendType='+recommendType,
                    success: function (res) {
                        if(res.data.result.code == '1'){
                            wx.showToast({
                                title: '报名成功',
                            })
                            that.getRecruitGroupInfo()
                        } else {
                            wx.showModal({
                                content: res.data.result.message,
                            })
                        }
                    }
                })
            }
        }
    },
    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {
        let that = this
        clearInterval(that.data.timer);

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {
        let that = this
        clearInterval(that.data.timer);
    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function (res) {
        let that = this
        if (res.from === 'button') {
        }
        return {
            title: '邀请组团求职',
            path: 'pages/enterpriseInfo/enterpriseInfo?groupid='+that.data.groupid +'&current=1'+'&createid='+that.data.creatorid+'&id='+that.data.vid,
            success: function (res) {

            }
        }
    }
})
