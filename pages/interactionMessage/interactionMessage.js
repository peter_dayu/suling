// pages/interactionMessage/interactionMessage.js
let app = getApp()
var baseUrl = app.globalData.baseUrl;
Page({

  /**
   * 页面的初始数据
   */
  data: {
      messageList:[]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
      this.getInteractionMessage()

      if(options.unread&&options.unread>0){
        this.clearUnreadHudongCache()
      }

  },
  clearUnreadHudongCache:function(){
    wx.request({
      url: baseUrl+'/cache/clearUnreadHudongCache?userid='+app.globalData.userid,
      success:function(res){

      }
    })
  },
  getInteractionMessage:function(){
      let that = this
      wx.request({
          url:baseUrl + "/hudongMessage?userid=" + app.globalData.userid,
          method: 'GET',
          success(res) {
              that.setData({
                  messageList:res.data
              })

          }
      })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})
