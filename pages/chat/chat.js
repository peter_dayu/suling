const app = getApp()

var websocket = require('../../utils/webSocket.js');

var utils = require('../../utils/util.js');

var baseUrl = app.globalData.baseUrl;

Page({
  data: {
    add: false,
    add1: true,
    add2: true,
    time: new Date(),
    //企业列表
    list: [],
    flv: "",
    chatList: [],
    chatMsg: '',
    winWidth: 0,
    winHeight: 0,
    color: "",
    siteId: "",
    // tab切换
    currentTab: 0,
    newslist: [],
    userInfo: {},
    scrollTop: 0,
    message: "",
    isAdmin: "",
    roomid: "",
    roomname: "",
    onlines: '',
    previewImgList: [],
    //弹幕
    inputHeight: '15rpx',
    doommData: [],
    arr: [],
    to:'',
    first:true,
    hasNew:true,
    timer:""
  },
  onLoad(options) {
    var that = this;
    //  that.data.locationId = options.locationId;
    //  that.data.flv='https://' + options.flv;

    if (app.globalData.userInfo) {
      that.setData({
        userInfo: app.globalData.userInfo
      })
    if(options.to){
      that.setData({
        to: options.to
      })
    }
    }
    websocket.connect('/chatSocket/','?from='+app.globalData.userid+'&to='+that.data.to, function (res) {
      var newd = JSON.parse(res.data)
      var date = newd.date;
      var list = []
      list = that.data.newslist
      list.push(JSON.parse(res.data))
      for (var i = 0; i < list.length; i++) {
        list[list.length - 1].date = utils.parseTime(parseInt(date));
      }
      setTimeout(function () {
        if(that.data.first){
          that.setData({
            newslist: list,
            first:false
          })
          that.getTop();
        }
      }, 500)
      if(!that.data.first){
        var from = newd.fromUser
          that.setData({
            newslist: list,
            hasNew:(from!=app.globalData.userid?true:false)
          })
          that.getTop();
      }
    })
  },

  onHide() {
    clearInterval(this.data.timer);
  },
  onUnload() {

    wx.closeSocket();
    clearInterval(this.data.timer);
    this.clearCache()
  },
  textfocus: function (e) {
    var that = this;

    that.setData({
      inputHeight: '600rpx'
    })
  },
  textblur: function (e) {
    var that = this;

    that.setData({
      inputHeight: '15rpx'
    })
  },
  binddan() {
    var that = this;
    that.setData({
      add2: (!that.data.add2)
    })
  },
  bindmu() {
    var that = this;
    that.setData({
      add2: (!that.data.add2)
    })
  },
  onReady(res) {
    this.ctx = wx.createLivePlayerContext('player')
  },
  bindPlay() {
    var that = this;
    that.setData({
      add: (!that.data.add)
    })
    this.ctx.play({
      success: res => {
        console.log('play success')
      },
      fail: res => {
        console.log('play fail')
      }
    })
  },
  //获取scrollTop值
  getTop: function(e){
    var that=this
    var list = that.data.newslist.length + 3;
    if (list * 90 > 1336) {
      that.setData({
        //scrollTop: this.data.scrollTop + 48
        scrollTop: list * 90
      })
    }
  },
  clearCache:function(){
    wx.request({
      url: baseUrl+'/cache/clearUnreadChatCache?userid='+app.globalData.userid+'&id='+this.data.newslist[0].id,
      success:function(res){

      }
    })
  },
  //事件处理函数
  send: function () {

    var flag = this

    if (this.data.message.trim() == "") {

      wx.showToast({

        title: '消息不能为空哦~',

        icon: "none",

        duration: 2000

      })

    } else {
      setTimeout(function () {
        flag.setData({
          increase: false
        })
      }, 500)
      websocket.send('{ "content": "' + this.data.message + '", "date": "' + new Date().getTime() + '","type":"text", "nickName": "' + this.data.userInfo.nickName + '", "avatarUrl": "' + this.data.userInfo.avatarUrl + '","isAdmin":"' + this.data.isAdmin + '"}')
      this.getTop(1);
    }
  },

  //监听input值的改变

  bindChange(res) {

    this.setData({

      message: res.detail.value

    })

  },

  cleanInput() {

    //button会自动清空，所以不能再次清空而是应该给他设置目前的input值

    this.setData({

      message: this.data.message

    })

  },

  increase() {

    this.setData({

      increase: true,

      aniStyle: true

    })

  },

  //点击空白隐藏message下选框

  outbtn() {

    this.setData({

      increase: false,

      aniStyle: true

    })

  },
  onShow(){
    var that=this
    that.data.timer =  setInterval(() => {
      if(this.data.hasNew){
        this.clearCache()
        this.setData({
          hasNew:false
        })
      }
    }, 6000);
  }
})
