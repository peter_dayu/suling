// pages/upgradeAgent/upgradeAgent.js
const app = getApp();
var baseUrl = app.globalData.baseUrl;
var fwzsUrl = app.globalData.fwzsUrl;
var login = require('../../utils/login.js');
Page({

    /**
     * 页面的初始数据
     */
    data: {

        current:0,
        priceList:[],
        totalPrice:0,   //要付款的钱
        number:1,
        level:1,
        recommendId:'',
        isTipTrue:false,
        buttonTitle:'我同意',
        coupons:[],
        bargain:'',
        couponValue:'',  //优惠金额
        couponLevel:'',
        coupon:'',
        disableCouponValue:false,
       checked:true
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        //login.login()
        this.queryPrice()
        var that=this
        if(options.recommendId){
            that.setData({
                recommendId:options.recommendId
            })
        }

         if(options.level){
            that.setData({
                current:options.level-1
            })
         }
         if(options.bargain){
            that.setData({
                bargain:options.bargain
            })
            that.getBargainAmount()
         }
         if(options.coupon){
            that.setData({
                coupon:options.coupon
            })
            that.getCounponList()
        }
    },
    queryPrice:function(){
        login.getCache(this.queryPrice1)
     },
     queryPrice1:function (adminToken){
       let that = this
       var current=that.data.current
       wx.request({
        url: fwzsUrl + "/api/vip/queryPrice?type=agent&level=1",
        header: {
            'content-type': 'application/json', // 默认值
            'Authorization':'Bearer '+adminToken
        },
        success: function (res) {
            that.setData({
                priceList:res.data,
                totalPrice:  res.data[current].totalPrice,
                number:res.data[current].number,
                level:res.data[current].level
            })
        }
      })
     },
    tipAgree:function(){
        this.setData({
            isTipTrue:false,
            buttonTitle:'我同意'
        })
    },
    tipAgree1:function(){
        this.setData({
            isTipTrue:true,
            buttonTitle:'确定'
        })
    },
    chooseNav:function(e){
        let that = this
        let current = parseInt(e.currentTarget.dataset.current)
       let canUesCoupon=  that.data.couponValue &&(that.data.couponLevel==current+1)
        this.setData({
            current:current,
            totalPrice:( canUesCoupon? that.data.priceList[current].totalPrice-that.data.couponValue: that.data.priceList[current].totalPrice),
            level:that.data.priceList[current].level,
            checked:(canUesCoupon?true:false),
            disableCouponValue:canUesCoupon?false:true,
            number:that.data.priceList[current].number
        })
    },
    getCounponList:function(){
        login.getCache(this.getCounponList1)
    },
    getCounponList1(adminToken){
        let that = this
        var current=that.data.current
        wx.request({
            url: fwzsUrl + '/api/myCoupons',
            data: { "type": 'knight','userid':app.globalData.userid},
            method: 'GET',
            header:{
                "Content-Type":"application/json",
                "Authorization":"Bearer "+adminToken
            },
            success: function (res) {
                if(res.data.length>0&&res.data[0].used==0){
                    that.setData({
                        coupons:res.data,
                        couponValue:res.data[0].amount,
                        couponLevel:res.data[0].level,
                        level:that.data.priceList[current].level,
                        number:that.data.priceList[current].number,
                        totalPrice: that.data.checked&&res.data[0].level==current+1?that.data.priceList[current].totalPrice-res.data[0].amount:that.data.priceList[current].totalPrice
                    })
                }else{
                    that.setData({
                        checked:false
                    })
                }


            }
        })
    },
    getBargainAmount:function () {
        login.getCache(this.getBargainAmount1)
    },
    getBargainAmount1:function (adminToken) {
        let that = this
        var current=that.data.current
        wx.request({
            url: fwzsUrl + '/api/bargainList',
            data: {'userid':app.globalData.userid},
            method: 'Post',
            header:{
                "Content-Type":"application/json",
                "Authorization":"Bearer "+adminToken
            },
            success: function (res) {
                if(res.data.list.length>0){
                    let   couponLevel= (res.data.list[0].type=='knight1'?1:res.data.list[0].type=='knight2'?2:res.data.list[0].type=='knight3'?3:null)
                    that.setData({
                        couponValue:res.data.totalAmount,
                        current:couponLevel-1,
                        couponLevel,
                        level:couponLevel,
                        number:that.data.priceList[couponLevel-1].number,
                        totalPrice:that.data.priceList[couponLevel-1].totalPrice-res.data.totalAmount
                    })
                }else{
                    that.setData({
                        checked:false
                    })
                }
            }
        })
    },
    buy:function(){
        wx.navigateTo({
          url: '../payfor/payfor?totalPrice='+this.data.totalPrice+'&number='+this.data.number+'&level='+this.data.level+'&recommendId='+(this.data.recommendId?this.data.recommendId:'')+'&couponType='+((this.data.checked&&this.data.coupon)?this.data.coupons[0].type:'')+'&bargain='+(this.data.bargain&&this.data.checked?this.data.bargain:'')
        })
    },
    checkboxChange:function(e){
        var that= this
        var current= that.data.current
        if(e.detail.value[0]=='cb'){
            that.setData({
                checked:true,
                totalPrice: that.data.priceList[current].totalPrice-that.data.couponValue,
            })
        }else{
            that.setData({
                checked:false,
                totalPrice: that.data.priceList[current].totalPrice
            })
        }
    },
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})
