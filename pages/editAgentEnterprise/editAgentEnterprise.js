// pages/recommendInfo/recommendInfo.js
let app = getApp()
var baseUrl = app.globalData.baseUrl;
var login = require('../../utils/login.js');
import util from '../../utils/util'
Page({

    /**
     * 页面的初始数据
     */
    data: {
        recruitList:[],
        current:0,
        date:'',
        index:'',
        interviewList:['合格','不合格'],
        statusIndex:'',
        companyList:[],
        createTime:null,
        reportStatus:4,
        reid:'',
        status:'',
        candidateCurrent:0,
        agentCurrent:0,
        teamCurrent:0,
        agentRights:{},
        agentRecruitsList:[],
        groupIndex:0,
        rightsNum:0,
        fromSet:0,
        enrollIndex:'',
        showFlag:false,
        recommendId:'',
        enrollCurrent:0,
        recruits:[],
        visible:app.globalData.visible,
        enrollStatus:[
            {
                name:'全部',
                id:''
            },
            {
                name:'已报名',
                id:'0'
            },
            {
                name:'已入职',
                id:'8'
            }],
        statusId:'',
        personalInfo:{},
        isShow:false,
        authList:[],
        yesterdayAgentRecruitsList:[]
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {

        if(options.recommendId){
            login.login()
            this.setData({
                recommendId:options.recommendId
            })

        }
        this.setData({
            visible:app.globalData.visible
        })
        if(options && options.current){
            this.setData({
                current:options.current
            })
        }

        this.getYesterdayAgentRecruits()
        this.getMyRecommendRecruitList()
        this.getMyRecommend()
        this.getInfo()
        this.isShowGuide()
    },
    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {
        let that = this
        this.getAgentRecruits()
        this.getMyAgentRights()
        login.userIdCallback = userid => {
            if(!app.globalData.user.isagent){
                that.updateRecommedUser(that.data.recommendId)
            }

            if(!app.globalData.isAuth){
                that.setData({
                    showFlag:true
                })
            }
        }
    },
    //判断是否为骑士
    getInfo: function () {
        let that = this
        wx.request({
            url: baseUrl + '/info',
            data: {
                "userid": app.globalData.userid
            },
            method: 'GET',
            success: function (res) {
                that.setData({
                    personalInfo: res.data
                })
            }
        })
    },
    // 判断指引是否显示
    isShowGuide:function(){
        let that = this
        wx.request({
            url:baseUrl+"/center/isFirstEnterKnightRecruit?userid="+app.globalData.userid,
            success:function (res) {
                if(res.data == '1'){
                    that.setData({
                        isShow:true
                    })
                }else{
                    that.setData({
                        isShow:false
                    })
                }
            }
        })
    },
    //成为骑士
    toKnight:function(){
        wx.navigateTo({
            url: '../knightRule/knightRule'
        })
    },
    //关闭指引
    closePop:function(){
        this.setData({
            isShow:false
        })
    },
    updateRecommedUser:function(recommendId){
        if(recommendId){
            wx.request({
                url: baseUrl + '/updRecommendId?userid='+app.globalData.userid +'&recommendId='+recommendId,
                success: function (res) {
                    console.log(res,"更新推荐人信息")
                }
            })
        }

    },
    //去企业详情页
    goEnterpriseInfo:function(e){
        let id = e.currentTarget.dataset.id
        if(id ){
            wx.navigateTo({
                url: '../enterpriseInfo/enterpriseInfo?id=' + id
            })
        }
    },
    getMyRecommend:function(){
        let that = this
        wx.request({
            url: that.data.date? baseUrl + '/myRecommend?userid='+app.globalData.userid +'&reportStatus='+that.data.reportStatus+'&reid='+that.data.reid+'&status='+that.data.status+'&createTime='+that.data.date+' 00:00:00':baseUrl + '/myRecommend?userid='+app.globalData.userid +'&reportStatus='+that.data.reportStatus+'&reid='+that.data.reid+'&status='+that.data.status,
            success: function (res) {
                that.setData({
                    recruitList:res.data
                })
                if(that.data.reportStatus == '4'){
                    let list1 = []
                    let list2 = []
                    let list3 = []
                    if(res.data.length > 0){
                        for(let i = 0; i<res.data.length; i++){
                            if (res.data[i].status == 0 || res.data[i].status == 1 || res.data[i].status == 2) {
                                list1.push(res.data[i])
                            } else if (res.data[i].status == 3 || res.data[i].status == 4 || res.data[i].status == 7) {
                                list2.push(res.data[i])
                            } else if (res.data[i].status == 8 || res.data[i].status == 9 || res.data[i].status == 10|| res.data[i].status == 11) {
                                list3.push(res.data[i])
                            }
                        }
                        if(list1.length != '0'){
                            that.setData({
                                candidateCurrent:0,
                                reportStatus:0
                            })
                            that.getMyRecommend()
                        }else if(list2.length != '0'){
                            that.setData({
                                candidateCurrent:1,
                                reportStatus:1
                            })
                            that.getMyRecommend()
                        }else if(list3.length != '0'){
                            that.setData({
                                candidateCurrent:2,
                                reportStatus:2
                            })
                            that.getMyRecommend()
                        }else{
                            that.setData({
                                candidateCurrent:0,
                                reportStatus:0
                            })
                            that.getMyRecommend()
                        }
                    }
                }
            }
        })
    },
    getMyRecommendRecruitList:function(){
        let that = this
        wx.request({
            url: baseUrl + '/entprz/myRecommendRecruitList?userid='+app.globalData.userid,
            success: function (res) {
                that.setData({
                    companyList:res.data
                })
            }
        })
    },
    checkAuth: function(e) {
        app.globalData.isAuth = e.detail.checkAuth
        app.globalData.userInfo = e.detail.userInfo
    },

    getAgentRecruits:function(){
        let that = this
        wx.request({
            url: baseUrl + '/agentRecruits?userid='+app.globalData.userid,
            success: function (res) {
                that.setData({
                    agentRecruitsList:res.data.recruits
                })
            }
        })
    },
    // 查看昨日代理权限
    getYesterdayAgentRecruits:function(){
        let that = this
        wx.request({
            url: baseUrl + '/agentRecruitsYesterday?userid='+app.globalData.userid,
            success: function (res) {
                that.setData({
                    yesterdayAgentRecruitsList:res.data
                })
            }
        })
    },
    getMyAgentRights:function(){
        let that = this
        wx.request({
            url: baseUrl + '/myAgentRights?userid='+app.globalData.userid,
            success: function (res) {
                let authList = parseInt(res.data.total - res.data.used)
                that.data.authList.length = authList

                that.setData({
                    agentRights:res.data,
                    rightsNum:res.data.total-res.data.used,
                    fromSet:res.data.fromSet,
                    authList:that.data.authList
                })

            }
        })
    },

    chooseAgentNav:function(e){
        let current = e.currentTarget.dataset.current
        this.setData({
            agentCurrent: current,
            reportStatus:4
        })
        if(current==1){
          this.getMyRecommend()
        }
        if(current==0){
            this.getAgentRecruits()
          }
    },

    chooseCandidateNav:function(e){
        let that = this
        let current = e.currentTarget.dataset.current
        this.setData({
            candidateCurrent: current
        })
        if(current == '1'){
            that.setData({
                reportStatus:1
            })
        }else if(current == '2'){
            that.setData({
                reportStatus:2
            })
        }else{
            that.setData({
                reportStatus:0
            })
        }
        that.getMyRecommend()
    },
    bindDateChange:function(e){
        this.setData({
            date: e.detail.value
        })
        this.getMyRecommend()
    },
    bindCompanyChange:function(e){
        let value = e.detail.value
        let reid = this.data.companyList[value].reId
        this.setData({
            index:value,
            reid: reid
        })
        this.getMyRecommend()
    },
    bindStatusChange:function(e){
        let index = e.detail.value
        this.setData({
            enrollIndex:'',
            statusIndex: index
        })
        if(index =='0'){
            this.setData({
                status: 3
            })
        }else{
            this.setData({
                status: 4
            })
        }
        this.getMyRecommend()
    },


    toBuy:function(){
        if(this.data.fromSet == '0'){
            wx.navigateTo({
                url: '../upgradeAgent/upgradeAgent?coupon=1'
            })
        }else{
            wx.navigateTo({
                url: '../upgradeAgentAgain/upgradeAgentAgain'
            })
        }
    },

    applyCancel:function(e){
        var that =this
        wx.showModal({
            title: '提示',
            content: '确认取消推荐',
            success (res) {
                if (res.confirm) {
                    wx.request({
                        url: baseUrl+'/cancelAgentRecruit?userid='+app.globalData.userid+'&reid='+e.currentTarget.dataset.reid,
                        success:function(res){
                            if(res.data==1){
                                wx.showToast({
                                    title: '取消成功',
                                })
                            }
                            that.setData({
                                agentRecruitsList:[]
                            })

                            that.getAgentRecruits()
                            that.getMyAgentRights()
                            that.getYesterdayAgentRecruits()
                        }
                    })
                } else if (res.cancel) {
                    console.log('用户点击取消')
                }
            }
        })


    },
    applyActive:function(e){
        var that =this
        wx.showModal({
            title: '提示',
            content: '确认开启推荐',
            success (res) {
                if (res.confirm) {
                    if(!e.currentTarget.dataset.vid){
                        wx.showToast({
                            icon:'error',
                            title: '代理的视频已删除，不能继续招聘!'
                        })
                        return false
                    }
                    wx.request({
                        url: baseUrl+'/continueYesterdayAgentRecruits?userid='+app.globalData.userid+'&reid='+e.currentTarget.dataset.reid+'&vid='+e.currentTarget.dataset.vid,
                        success:function(res){

                            if(res.data==1){
                                wx.showToast({
                                    title: '操作成功，预计三分钟内通过审核',
                                    icon:'none'
                                })
                                that.setData({
                                    agentRecruitsList:[]
                                })
                                that.getAgentRecruits()
                                that.getMyAgentRights()
                            }
                            if(res.data== -1){
                                wx.showToast({
                                    title: '权限余额不足',
                                })
                            }
                            if(res.data == -2){
                                wx.showToast({
                                    title: '您已招聘该企业',
                                })
                            }
                            if(res.data == -3){
                                wx.showToast({
                                    title: '正在审核中',
                                })
                            }
                        }
                    })
                } else if (res.cancel) {
                    console.log('用户点击取消')
                }
            }
        })

    },
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },


    // 企业优选列表
    goEnterpriseList:function(){
        wx.navigateTo({
          url: '../agentEnterprise/agentEnterprise'
        })
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function (res) {
        let imageUrl = res.target.dataset.image
        let vid = res.target.dataset.vid
        if (res.from === 'button') {

        }
        return {
            title: '给您分享一个职位',
            path: 'pages/enterpriseInfo/enterpriseInfo?userid='+app.globalData.userid+'&id='+vid,
            imageUrl:imageUrl,
            success: function (res) {

            }
        }
    }
})
