// pages/recruit/recruit.js
import {AreaPicker} from "../../secondArea/areaSelector/selector";

var QQMapWX = require('../../utils/qqmap-wx-jssdk.min.js');
var login = require('../../utils/login.js');
var qqmapsdk = new QQMapWX({
    key: 'IFCBZ-FF53U-ILBVP-BOAOD-SA4E7-GWFPK'//申请的开发者秘钥key
});
const app = getApp();
var baseUrl = app.globalData.baseUrl;
Page(Object.assign({}, AreaPicker,{

    /**
     * 页面的初始数据
     */
    data: {
        city:'',
        district:'',
        locationId:'',
        pageNo:1,
        //当前选中数组的下标值
        customIndex: [0, 0],
        aIndex:[0,0],
        //当前选中数组
        fonlyArray: [
            [],
            []
        ],
        zonlyArray: [
            [],
            []
        ],
        onlyArray: [
            [],
            []
        ],
        industryArray:['制造业'],
        isFullTimeArray:['长期工','小时工'],
        skilledArray:['普工','技工'],
        pickerdisable:0,
        customArray:[],
        zhizao:[
            [],
            []
         ],
        zhizaoValue:'普工 长期工',
        aValue:'操作工',
        industryValue:'制造业',
        isFullTimeValue:'长期工',
        skilledValue:'普工',
        fuwu:[
            [],
            []
         ],
        //制造业或服务业
        tagIndex:0,
        industryIndex:0,
        isFullTimeIndex:0,
        skilledIndex:0,
        workId:'49',
        date:'',
        locationName: '',
        areaId:'',
        inputName:'',
        videoList:[]
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        let that =this
        this.getOccupationList()
        wx.request({
            url: baseUrl + "/queryDayTime",
            success: function (res) {
                that.setData({
                    date:res.data.result
                })
            }
        })
        that.getEnterpriseList()
    },
    getLocation() {
        let that = this
        wx.getLocation({
            success: function (res) {
                // 调用sdk接口
                qqmapsdk.reverseGeocoder({
                    location: {
                        latitude: res.latitude,
                        longitude: res.longitude
                    },
                    success: function (res) {
                        //获取当前地址成功
                        var city = res.result.address_component.city;
                        var district = res.result.address_component.district
                        that.setData({
                            city:city,
                            district: district,
                            locationName:district
                        })
                        wx.request({
                            url: baseUrl + "/area/queryLocationIdByName?city=" + city+"&district="+district,
                            success: function (res) {
                                that.setData({
                                    locationId:res.data
                                })
                                that.getEnterpriseList();

                            }
                        })
                    },
                    fail: function (res) {
                        console.log('获取当前地址失败');
                    }
                });
            },
            fail(res) {

                console.log(res, '获取地址失败')
            }
        })
    },
    getOccupationList(){
        let that =this
        wx.request({
            url: baseUrl + "/dict/queryOccupationByCondition?industry=" + that.data.industryIndex +"&isFullTime="+(that.data.isFullTimeIndex==0?1:0)+"&skilled="+that.data.skilledIndex,
            success: function (res) {

                that.setData({
                    onlyArray:res.data
                })
            }
        })
    },
    getEnterpriseList(){
      let that =this
      var page=that.data.pageNo
        wx.request({
            url: baseUrl + "/video/queryAll?userid=" + app.globalData.userid +"&locationId="+(that.data.locationId?that.data.locationId:'')+"&type=1"+"&pageNo="+that.data.pageNo +"&pageSize=20&tag="+(that.data.workId),
            success: function (res) {
                if(page>1){
                    that.setData({
                        videoList:[...that.data.videoList,...res.data]
                    })
                }else if(page==1){
                    that.setData({
                        videoList:res.data
                    })
                }

            }
        })
    },
    goEnterpriseInfo(e){
        wx.navigateTo({
          url: '../enterpriseInfo/enterpriseInfo?id='+e.currentTarget.dataset.id+'&workId='+this.data.workId
        })
    },
    onAreaCommit(locationList) { //当用户更换地区
        let _that = this;
        this.setData({
            locationName: locationList[0].name || '',
            areaId: locationList[0].id || '',
            pageNo:1
        });
        _that.search()
    },
    bindPickerChange(e){
        let index = e.detail.value
        let workId = this.data.onlyArray[index].id
        this.setData({
            workId,
            aValue: this.data.onlyArray[index].value
        })
        this.search()
    },
    bindColumnChange(e){
        let that = this
        if(that.data.industryIndex==0){
            if(e.detail.column == '0'){

                that.data.onlyArray[1] = []
                if(e.detail.value == '0'){
                    for (var k = 0; k < that.data.customArray[0].children[0].children.length; k++){
                        that.data.onlyArray[1].push(that.data.customArray[0].children[e.detail.value].children[k]);
                    }
                }else{
                    for (var k = 0; k < that.data.customArray[0].children[1].children.length; k++){
                        that.data.onlyArray[1].push(that.data.customArray[0].children[1].children[k]);
                    }
                }
            }
            that.setData({
                onlyArray:that.data.onlyArray
            })
        }else if(that.data.industryIndex==1){
            if(e.detail.column == '0'){

                that.data.onlyArray[1] = []
                if(e.detail.value == '0'){
                    for (var k = 0; k < that.data.customArray[1].children[0].children.length; k++){
                        that.data.onlyArray[1].push(that.data.customArray[1].children[e.detail.value].children[k]);
                    }
                }else{
                    for (var k = 0; k < that.data.customArray[1].children[1].children.length; k++){
                        that.data.onlyArray[1].push(that.data.customArray[1].children[1].children[k]);
                    }
                }
            }
            that.setData({
                onlyArray:that.data.onlyArray
            })
        }

    },


    bindMultiPickerChange(e){
        let that = this
        let id = ''
        let value= ''
        let currentIndex = e.detail.value
        id =  that.data.onlyArray[0][currentIndex[0]].children[currentIndex[1]].id
        value =  that.data.onlyArray[0][currentIndex[0]].value+' '+ that.data.onlyArray[0][currentIndex[0]].children[currentIndex[1]].value
        that.setData({
            aIndex: currentIndex,
            aValue :value,
            workId: id
        })
        that.search()
    },
    bindIndustryChange(e){
      var that=this
      that.setData({
          workId:'49',
          aValue:'操作工',
        industryIndex: e.detail.value,
        industryValue:this.data.industryArray[e.detail.value],
      })
      that.getOccupationList();
      that.search()

    },
    bindIsFullTimeChange(e){
        var that=this
        let workid =49
        let workname ='操作工'
        if(e.detail.value==1&&this.data.skilledIndex==1){
          workid=39
           workname ='电工'
        }
        if(e.detail.value==1&&this.data.skilledIndex==0){
            workid=52
             workname ='操作工'
        }
        if(e.detail.value==0&&this.data.skilledIndex==0){
            workid=49
             workname ='操作工'
        }
        if(e.detail.value==0&&this.data.skilledIndex==1){
            workid=29
             workname ='电工'
        }
        that.setData({
          workId:workid,
          aValue:workname,
          isFullTimeIndex: e.detail.value,
          isFullTimeValue:this.data.isFullTimeArray[e.detail.value],
        })
        that.getOccupationList();
        that.search()

      },
      bindSkilledChange(e){
        var that=this
        let workid =49
        let workname ='操作工'
        if(this.data.isFullTimeIndex==1&&e.detail.value==1){
          workid=39
           workname ='电工'
        }
        if(this.data.isFullTimeIndex==0&&e.detail.value==1){
            workid=52
             workname ='操作工'
        }
        if(this.data.isFullTimeIndex==0&&e.detail.value==0){
            workid=49
             workname ='操作工'
        }
        if(this.data.isFullTimeIndex==0&&e.detail.value==1){
            workid=29
             workname ='电工'
        }
        that.setData({
          workId:workid,
          aValue:workname,
          skilledIndex: e.detail.value,
          skilledValue:this.data.skilledArray[e.detail.value],
        })
        that.getOccupationList();
        that.search()

      },
    getValue(e){
        this.setData({
            inputName:e.detail.value
        })
        this.search()
    },
    search(){
        let that = this
        wx.request({
            url: baseUrl + '/video/search?type=1&value='+that.data.inputName+"&locationId="+that.data.areaId+"&tag="+that.data.workId,//这一部的含义是访问我们的数据,并把同类名字的数据全部拿出来
            success: function (res) {
                that.setData({
                    videoList:res.data
                })
            }
        })
    },
    clear(){
        let that = this
        that.setData({
            locationName:'',
            areaId:'',
            industryIndex:'0',
            isFullTimeIndex:'0',
            isFullTimeValue:'长期工',
            industryValue:'制造业',
            skilledIndex:'0',
            skilledValue:'普工',
            workId:'49',
            aValue:'操作工',
            aIndex:'',

        })
        that.search()
    },
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {



    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {
        let that = this
        if(that.data.videoList.length == that.data.pageNo*20){
            that.data.pageNo++
            that.getEnterpriseList()
        }
    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})
)
