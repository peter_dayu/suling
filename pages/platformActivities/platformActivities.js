// pages/bonusPoints/bonusPoints.js
const app = getApp();
var baseUrl = app.globalData.baseUrl;
var fwzsUrl = app.globalData.fwzsUrl;
var login = require('../../utils/login.js');
Page({

    /**
     * 页面的初始数据
     */
    data: {
        coupons:[],
        visible:app.globalData.visible,
        currentTime:''
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        // this.getCounponList()
        let time = new Date().getTime()
        this.setData({
            currentTime:time
        })
        this.queryMyCoupons()
        this.setData({
            visible:app.globalData.visible
        })
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },
    queryMyCoupons:function(){
        login.getCache(this.queryMyCoupons1)
    },
    queryMyCoupons1(adminToken){
        let that = this
        var token = app.globalData.token;
        wx.request({
            url: fwzsUrl + '/api/myCoupons',
            data: { "userid":app.globalData.userid},
            method: 'GET',
            header:{
                "Content-Type":"application/json",
                "Authorization":"Bearer "+adminToken
            },
            success: function (res) {
                if(res.data){
                    for(let i=0;i<res.data.length;i++){
                        let endtime = res.data[i].endTime.replace(new RegExp("-","gm"),"/")
                        endtime = (new Date(endtime)).getTime()
                        res.data[i].milliseconds =(new Date(endtime)).getTime()
                    }
                    that.setData({
                        coupons:res.data
                    })
                }

            }
        })
    },
    getCounponList:function(){
        login.getCache(this.getCounponList1)
    },
    getCounponList1(adminToken){
        let that = this
        var token = app.globalData.token;
        wx.request({
            url: fwzsUrl + '/api/couponList',
            data: { "type": 'knight',"userid":app.globalData.userid},
            method: 'GET',
            header:{
                "Content-Type":"application/json",
                "Authorization":"Bearer "+adminToken
            },
            success: function (res) {
                that.setData({
                    coupons:res.data
                })

            }
        })
    },
    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },
    toUpdateAgent:function(e){
        var index =e.currentTarget.dataset.index
        var level=this.data.coupons[index].level
        wx.navigateTo({
            url: '../upgradeAgent/upgradeAgent?coupon=1&level='+level
        })
    },
    goApplyInterview:function(){
        wx.showModal({
            title: '提示',
            content: '快去参加面试吧',
            success: function (res) {
            }
        })
    },
    toJobList:function(){
        wx.showModal({
            title: '提示',
            content: '快去首页报名吧',
            success: function (res) {
            }
        })
    },
    toInvite:function(){
        wx.switchTab({
            url:"../me/me"
        })
    },
    toEntryList:function(){
        wx.navigateTo({
            url: '../friendEntry/friendEntry'
        })
    },
    //去推荐
    recommend:function(){
        wx.switchTab({
            url: '../recruit/recruit'
        })
    },
    //去我的收益
    toMyassets:function(){
        wx.navigateTo({
            url: '../myAssets/myAssets'
        })
    },
    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})
