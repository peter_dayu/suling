// pages/recommendInfo/recommendInfo.js
let app = getApp()
var baseUrl = app.globalData.baseUrl;
var login = require('../../utils/login.js');
import util from '../../utils/util'
Page({

    /**
     * 页面的初始数据
     */
    data: {
        current:0,
        index:'',
        interviewList:['合格','不合格'],
        statusIndex:'',
        status:'',
        teamList:[],
        showFlag:false,
        statusId:''
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {

        this.getMyTeam(1,null,1)

    },

    checkAuth: function(e) {
        app.globalData.isAuth = e.detail.checkAuth
        app.globalData.userInfo = e.detail.userInfo
    },
    getMyTeam:function(level,status,isagent){
        let that = this
        wx.request({
            url: baseUrl + '/myTeam/'+app.globalData.userid+'?status='+(status?status:'') +'&level='+(level?level:'')+'&isagent='+(isagent?isagent:''),
            success: function (res) {
                that.setData({
                    teamList:res.data,
                    level:level,
                    statusIndex:status
                })
            }
        })
    },
    queryLevel2Team:function(e){
        var that=this

        if(e.currentTarget.dataset.level!=1){
            return false
        }
        wx.request({
            url: baseUrl + '/myTeam/'+app.globalData.userid+'?status='+that.data.statusId +'&level=2&pid='+e.currentTarget.dataset.userid+'&isagent=1',
            success: function (res) {
                that.setData({
                    teamList:res.data
                })
            }
        })
    },



    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    //招募骑士
    enlist:function(){
        wx.navigateTo({
          url: '../recruitingShare/recruitingShare'
        })
    },
    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },
    /**
     * 用户点击右上角分享
     */

})
