
let app = getApp()
var baseUrl = app.globalData.baseUrl;
var login = require('../../utils/login.js');
import util from '../../utils/util'
Page({

    /**
     * 页面的初始数据
     */
    data: {
        recruitList:[],
        current:0,
        date:'',
        index:'',
        interviewList:['合格','不合格'],
        statusIndex:'',
        companyList:[],
        createTime:null,
        reportStatus:0,
        reid:'',
        status:'',
        candidateCurrent:0,
        agentCurrent:0,
        teamCurrent:0,
        agentRights:{},
        agentRecruitsList:[],
        groupList:['团队成员','一级团队','二级团队'],
        groupIndex:0,
        teamList:[],
        rightsNum:0,
        fromSet:0,
        enrollIndex:'',
        showFlag:false,
        recommendId:'',
        enrollCurrent:0,
        level:1,
        src:"https://www.juyuange.net/zhaomujihua.png",

        enrollStatus:[
            {
                name:'全部',
                id:''
            },
            {
                name:'已报名',
                id:'0'
            },
            // {
            //     name:'已到场',
            //     id:'1'
            // },
            // {
            //     name:'面试通过',
            //     id:'3'
            // },
            {
                name:'已入职',
                id:'8'
            }],
        statusId:'',
        personalInfo:{}
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        if(options.recommendId){
            login.login()
            this.setData({
                recommendId:options.recommendId
            })

        }


    },
    //判断是否为骑士
    getInfo: function () {
        let that = this
        wx.request({
            url: baseUrl + '/info',
            data: {
                "userid": app.globalData.userid
            },
            method: 'GET',
            success: function (res) {
                that.setData({
                    personalInfo: res.data
                })
            }
        })
    },
    updateRecommedUser:function(recommendId){
        if(recommendId){
            wx.request({
                url: baseUrl + '/updRecommendId?userid='+app.globalData.userid +'&recommendId='+recommendId,
                success: function (res) {
                   console.log(res,"更新推荐人信息")
                }
            })
        }

    },

    getMyRecommend:function(){
        let that = this
        wx.request({
            // url: baseUrl + '/myRecommend?userid='+app.globalData.userid +'&reportStatus='+that.data.reportStatus+'&reid='+that.data.reid+'&status='+that.data.status +'&createTime='+that.data.createTime,

            url: that.data.date? baseUrl + '/myRecommend?userid='+app.globalData.userid +'&reportStatus='+that.data.reportStatus+'&reid='+that.data.reid+'&status='+that.data.status+'&createTime='+that.data.date+' 00:00:00':baseUrl + '/myRecommend?userid='+app.globalData.userid +'&reportStatus='+that.data.reportStatus+'&reid='+that.data.reid+'&status='+that.data.status,
            success: function (res) {
                that.setData({
                    recruitList:res.data
                })
            }
        })
    },
    getMyRecommendRecruitList:function(){
        let that = this
        wx.request({
            url: baseUrl + '/entprz/myRecommendRecruitList?userid='+app.globalData.userid,
            success: function (res) {
                that.setData({
                    companyList:res.data
                })
            }
        })
    },
    checkAuth: function(e) {
        app.globalData.isAuth = e.detail.checkAuth
        app.globalData.userInfo = e.detail.userInfo
    },
    getMyTeam:function(level,status,isagent){
        let that = this
        wx.request({
            url: baseUrl + '/myTeam/'+app.globalData.userid+'?status='+(status?status:'') +'&level='+(level?level:'')+'&isagent='+(isagent?isagent:''),
            success: function (res) {
                that.setData({
                    teamList:res.data,
                    level:level,
                    statusIndex:status
                })
            }
        })
    },

    getMyAgentRights:function(){
        let that = this
        wx.request({
            url: baseUrl + '/myAgentRights?userid='+app.globalData.userid,
            success: function (res) {
                that.setData({
                    agentRights:res.data,
                    rightsNum:res.data.total-res.data.used,
                    fromSet:res.data.fromSet
                })
            }
        })
    },
    chooseNav:function(e){
        let current = e.currentTarget.dataset.current
        this.setData({
            current: current
        })
        if(current==1){
            this.getMyTeam(1,null,1)
        }
    },

    apply:function(){
        let that = this
        if(this.data.rightsNum > 0){
            if(app.globalData.isAuth==0){
                wx.showModal({
                    cancelColor: 'cancelColor',
                    title: '提示',
                    content: '请先授权',
                    success (res) {
                        if (res.confirm) {
                            console.log('用户点击确定')
                            that.setData({
                                showFlag:true
                            })
                        } else if (res.cancel) {
                            console.log('用户点击取消')
                        }
                    }
                  })
                  return false
            }
            wx.getSetting({
                success: res => {
                  // 已授权
                 if (res.authSetting['scope.userLocation'] === false) {
                     wx.showToast({
                       title: '请先打开地理位置',
                       duration :1000
                     })
                     setTimeout(() => {
                       return false
                     }, 1000);

                 }else {
                    wx.navigateTo({
                        url: '../recommendShare/recommendShare'
                    })
                 }
                }
            })

        }else{
            if(that.data.fromSet == '0'){
                wx.navigateTo({
                    url: '../upgradeAgent/upgradeAgent'
                })
            }else{
                wx.navigateTo({
                    url: '../upgradeAgentAgain/upgradeAgentAgain'
                })
            }
         }
        //  if(that.data.fromSet == '0'){
        //     wx.navigateTo({
        //         url: '../upgradeAgent/upgradeAgent?recommendId='+that.data.recommendId
        //     })
        // }
    },
    apply1:function(){
        let that = this
        if(that.data.fromSet == '0'){
                    wx.navigateTo({
                        url: '../upgradeAgent/upgradeAgent?recommendId='+that.data.recommendId+'&coupon=1'
                    })
                }
    },
    applyCancel:function(e){
        console.log(e,"cancelAgentRecruit")
        var that =this
        wx.request({
          url: baseUrl+'/cancelAgentRecruit?userid='+app.globalData.userid+'&reid='+e.currentTarget.dataset.reid,
          success:function(res){
              if(res.data==1){
                  wx.showToast({
                    title: '取消成功',
                  })
              }
              that.getAgentRecruits()
              that.getMyAgentRights()
          }
        })
    },
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {
        let that = this
        login.userIdCallback = userid => {
            // if(!app.globalData.user.isagent){
            //     that.updateRecommedUser(that.data.recommendId)
            // }

            if(!app.globalData.isAuth){
                that.setData({
                    showFlag:true
                })
            }
        }
        that.getInfo()
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload () {
        // wx.reLaunch({
        //   url: '/pages/index/index'//原始首页路径
        // })
    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },
    confirm:function(){
        if(app.globalData.userInfo.isagent=='1'){
            wx.showToast({
              title:'不能重复加入'
            })
        }else{
            this.apply1()
        }
    },
    share:function(){
            wx.showModal({
                title: '提示',
                content: '不是骑士不能分享',
                success (res) {
                    if (res.confirm) {
                        console.log('用户点击确定')
                    } else if (res.cancel) {
                        console.log('用户点击取消')
                    }
                }
            })
    },
    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function (res) {

        if (res.from === 'button') {

        }
        return {
            title: '欢迎加入',
            path: 'pages/recruitingShare/recruitingShare?recommendId='+app.globalData.userid,
            success: function (res) {
                wx.showModal({
                  cancelColor: 'cancelColor',
                  content:res
                })
            }
        }
    }
})
