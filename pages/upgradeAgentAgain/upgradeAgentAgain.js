// pages/upgradeAgentAgain/upgradeAgentAgain.js
const app = getApp();
var fwzsUrl = app.globalData.fwzsUrl;
var baseUrl= app.globalData.baseUrl;
var login = require('../../utils/login.js');

Page({

  /**
   * 页面的初始数据
   */
  data: {
    number:0,
    price:{},
    totalPrice:0
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
      this.queryPrice()
  },
  queryPrice:function(){

   login.getCache(this.queryPrice1)

},
queryPrice1:function (adminToken){
  let that = this
  console.log(adminToken, "执行回调了啊")
    wx.request({
      url: fwzsUrl+"/api/vip/queryAgentPrice?type=agent&level=0",
      header: {
          'content-type': 'application/json', // 默认值
          'Authorization':'Bearer '+ adminToken
      },
      success: function (res) {
          that.setData({
              price:res.data
          })
      }
  })
},
  coutAgent:function(e){
    var that=this
    let num = e.detail.value
    var that=this
    .setData({
        number:num,
        totalPrice:(this.data.price.price*num).toFixed(2)
    })
  },
  add:function name() {
    this.setData({
      number:this.data.number+1,
      totalPrice:(this.data.price.price*(this.data.number+1)).toFixed(2)
  })
  },
  subtraction:function name() {
    if(this.data.number>0){
      this.setData({
        number:this.data.number-1,
        totalPrice:(this.data.price.price*(this.data.number-1)).toFixed(2)
    })
    }

  },
  buy:function(){
    if(this.data.number>0){
      wx.navigateTo({
        url: '../payfor/payfor?totalPrice='+this.data.totalPrice+'&number='+this.data.number+'&level=0'
      })
    }else{
      wx.showToast({
        title: '数量有误',
        icon:'error',
        duration:2000
      })
    }

},
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})
