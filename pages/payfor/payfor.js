// pages/payfor/payfor.js
const app = getApp();
var fwzsUrl = app.globalData.fwzsUrl;
var baseUrl= app.globalData.baseUrl;
var login = require('../../utils/login.js');
Page({

    /**
     * 页面的初始数据
     */
    data: {
        realBalance:0,
        couponType:'',
        bargain:'',
        isBlanceSelected:true,
        ispaySelected:false,
        current:0,
        totalPrice:null
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        var that=this
        if(options.recommendId){
            that.setData({
                recommendId:options.recommendId
            })
        }
        if(options.couponType){
            that.setData({
                couponType:options.couponType
            })
        }
        if(options.bargain){
            that.setData({
                bargain:options.bargain
            })
        }
        that.setData({
            totalPrice:options.totalPrice,
            number:parseInt(options.number),
            level:parseInt(options.level)
        })
        that.queryAccount()

    },
    queryAccount:function(){
               login.getCache(this.queryAccount1)
    },
    queryAccount1:function(adminToken){
        let that = this
        app.globalData.adminToken=adminToken
        wx.request({
            url: fwzsUrl + "/api/account?userid="+app.globalData.userid,
            header: {
                'content-type': 'application/json', // 默认值
                'Authorization':'Bearer '+adminToken
            },
            success: function (res) {
                that.setData({
                    realBalance:res.data.realBalance
                })
            }
        })
    },
    chooseNav:function(e){
        let that = this
        let current = e.currentTarget.dataset.current
        that.setData({
            current:current
        })
        if(current == '0'){
            that.setData({
                isBlanceSelected:true,
                ispaySelected:false
            })
        }else{
            that.setData({
                isBlanceSelected:false,
                ispaySelected:true
            })
        }

    },
    confirm:function(){
        var that =this
        if(that.data.isBlanceSelected){
            if(that.data.realBalance<that.data.totalPrice){
                wx.showModal({
                    icon:'error',
                    content:'余额不足'
                  })
                  return false
            }else{
               wx.request({
                url: fwzsUrl + "/api/purchaseAgentRightsBalance",
                method:"POST",
                header: {
                    'content-type': 'application/json', // 默认值
                    'Authorization':'Bearer '+app.globalData.adminToken
                },
                data:{
                    userid: app.globalData.userid,
                    number: that.data.number,
                    type:"agent",
                    totalFee:that.data.totalPrice*100,
                    level:that.data.level,
                    couponType:that.data.couponType,
                    bargain:that.data.bargain,
                    recommendId:(that.data.recommendId?that.data.recommendId:'')
                },
                success: function (res) {
                    if(res.data&&res.data==1){
                        wx.showToast({
                          title: '支付成功',
                          duration:1000
                        })
                        setTimeout(() => {
                            wx.redirectTo({
                                url: '/pages/paySuccess/paySuccess',
                              })
                        }, 1000)
                    }else{
                        wx.showToast({
                          icon:"none",
                          title: res.data.message,
                          duration:2000
                        })
                    }
                }
               })
            }
        }if(that.data.ispaySelected){
            wx.request({
                url: baseUrl + "/api/pay/purchaseAgentRightsWx?payType=1&number="+that.data.number+"&type=agent&level="+
                that.data.level+"&userid="+app.globalData.userid+"&recommendId="+(that.data.recommendId?that.data.recommendId:'')+"&couponType="+that.data.couponType+'&bargain='+that.data.bargain,
                data: {
                    openid: app.globalData.openid,
                    body: "代理权限",
                    outTradeNo: (new Date()).valueOf(),
                    totalFee:100*that.data.totalPrice,
                    spbillCreateIp:"192.168.0.123",
                    notifyUrl:app.globalData.notifyUrl+"/api/pay/notify/agentOrder",
                    tradeType:"JSAPI"
                },
                method:"Post",
                success: function (res) {
                    login.doWxPay(res.data)

                }
            })
        }
    },
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})
