// pages/agentRecruits/agentRecruits.js
const app = getApp();
var baseUrl = app.globalData.baseUrl;
Page({

    /**
     * 页面的初始数据
     */
    data: {
        userInfo: {},
        recruits: [],
        userid: '',
        showBtn: false,
        rightsNum: 0,
        showFlag: false
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        let that = this
        if (options && options.userid) {
            this.setData({
                userid: options.userid
            })
            if (options.userid == app.globalData.userid) {
                that.setData({
                    showBtn: true
                })
            }
        }
        this.getRecruits()
        this.getMyAgentRights()
    },
    getRecruits: function () {
        let that = this
        wx.request({
            url: baseUrl + "/agentRecruits?userid=" + that.data.userid,
            success: function (res) {
                that.setData({
                    userInfo: res.data.user,
                    recruits: res.data.recruits
                })
            }
        })
    },
    getMyAgentRights: function () {
        let that = this
        wx.request({
            url: baseUrl + "/myAgentRights?userid=" + app.globalData.userid,
            success: function (res) {
                let rightsNum = res.data.total - res.data.used
                that.setData({
                    rightsNum: rightsNum,
                    fromSet: res.data.fromSet
                })
            }
        })
    },
    goEnterpriseInfo(e) {
        let id = e.currentTarget.dataset.id
        let userid = e.currentTarget.dataset.userid
        wx.navigateTo({
            url: '../enterpriseInfo/enterpriseInfo?id=' + id + '&userid=' + userid
        })
    },
    apply: function () {
        let that = this
        if (this.data.rightsNum > 0) {
            if (this.data.rightsNum > 0) {
                if (app.globalData.isAuth == 0) {
                    wx.showModal({
                        cancelColor: 'cancelColor',
                        title: '提示',
                        content: '请先授权',
                        success(res) {
                            if (res.confirm) {
                                console.log('用户点击确定')
                                that.setData({
                                    showFlag: true
                                })
                            } else if (res.cancel) {
                                console.log('用户点击取消')
                            }
                        }
                    })
                    return false
                }
                wx.getSetting({
                    success: res => {
                        // 已授权
                        if (res.authSetting['scope.userLocation'] === false) {
                            wx.showToast({
                                title: '请先打开地理位置',
                                duration: 1000
                            })
                            setTimeout(() => {
                                return false
                            }, 1000);

                        } else {
                            wx.navigateTo({
                                url: '../recommendShare/recommendShare'
                            })
                        }
                    }
                })
            }
        }else {
            if (that.data.fromSet == '0') {
                wx.navigateTo({
                    url: '../upgradeAgent/upgradeAgent'
                })
            } else {
                wx.navigateTo({
                    url: '../upgradeAgentAgain/upgradeAgentAgain'
                })
            }
        }
    },
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})
